package com.dhanush.infotech.project.GOIL1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dhanush.infotech.project.GOIL1.model.Delivery;

public interface DeliveryRepoistory extends JpaRepository<Delivery, Long> {
	
	
	@Query(value = "SELECT a FROM Delivery a WHERE admission_id = :caseId")
	List<Delivery> findByCaseId(@Param("caseId") Long uniqueNo) ;

}
