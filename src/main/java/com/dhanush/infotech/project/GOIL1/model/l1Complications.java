package com.dhanush.infotech.project.GOIL1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "l1_complications")
public class l1Complications {
	@Id
	private Long id;
	private static final long serialVersionUID = 1L;
	@Column(name="state")
	public String state;
	@Column(name="facilityname")
	public String facilityname;
	@Column(name="districtname")
	public String districtname;
	@Column(name="district")
	public String district;
	@Column(name="block")
	public String block;
	@Column(name="facility")
	public String facility;
	@Column(name="createdat")
	public String createdat;
	@Column(name="facility_type")
	public String facilitytype;
	@Column(name="deliverycomplications")
	public String deliverycomplications;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getFacilityname() {
		return facilityname;
	}
	public void setFacilityname(String facilityname) {
		this.facilityname = facilityname;
	}
	public String getDistrictname() {
		return districtname;
	}
	public void setDistrictname(String districtname) {
		this.districtname = districtname;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}
	public String getFacility() {
		return facility;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	public String getCreatedat() {
		return createdat;
	}
	public void setCreatedat(String createdat) {
		this.createdat = createdat;
	}
	public String getFacilitytype() {
		return facilitytype;
	}
	public void setFacilitytype(String facilitytype) {
		this.facilitytype = facilitytype;
	}
	
	public String getDeliverycomplications() {
		return deliverycomplications;
	}
	public void setDeliverycomplications(String deliverycomplications) {
		this.deliverycomplications = deliverycomplications;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
