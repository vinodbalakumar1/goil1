package com.dhanush.infotech.project.GOIL1.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.dhanush.infotech.project.GOIL1.model.AssessmentOfPostPartumCondition;
import com.dhanush.infotech.project.GOIL1.model.BabyNotes;
import com.dhanush.infotech.project.GOIL1.model.Delivery;
import com.dhanush.infotech.project.GOIL1.model.DischargeDetails;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "l1_admission")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class Admission {

	public AssessmentOfPostPartumCondition getAssessmentOfPostPartumCondition() {
		return assessmentOfPostPartumCondition;
	}

	public void setAssessmentOfPostPartumCondition(AssessmentOfPostPartumCondition assessmentOfPostPartumCondition) {
		this.assessmentOfPostPartumCondition = assessmentOfPostPartumCondition;
	}

	/**
	 * Created by by vinod on 25/07/18.
	 */

	@Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt = new Date();

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt = new Date();

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "admission")
	private Delivery delivery;
	
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "admission")
	private DischargeDetails dischargeDetails;
	
	public DischargeDetails getDischargeDetails() {
		return dischargeDetails;
	}

	public void setDischargeDetails(DischargeDetails dischargeDetails) {
		this.dischargeDetails = dischargeDetails;
	}

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "admission")
	private AssessmentOfPostPartumCondition assessmentOfPostPartumCondition;
	
	
	
	
	public List<BabyNotes> getBabynotes() {
		return babynotes;
	}

	public void setBabynotes(List<BabyNotes> babynotes) {
		this.babynotes = babynotes;
	}

	@OneToMany(cascade=CascadeType.ALL,fetch =FetchType.LAZY)
    @JoinColumn(name="babynotesid")
    private List<BabyNotes> babynotes;
	
	

	public Delivery getDelivery() {
		return delivery;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Size(max = 60)
	public String uniqueNo;



	
	private String  address;
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	private String state;
	
	private String statename;
	
	private String districtname;
	
	private String district;

	private String blockname;
	
	private String block;


	private String facilitytypename;

	public String facilityType;
	
	private String facility;

	private String facilityname;

	private String patientName;
	
	private String age;

	private String woDo;
	
	private String mctsNo;

	private String booked;
	
	private String bplJsyRegister;
	

	private String nameOfAsha;

	private String isActive;
	
	private String admissionDate;

	private String time;
	
	private String lmp;

	private String edd;
	
	private String ga;
	
	private String antenatalSteroidsGiven;
	
	
	private String gravida;

	private String parity;
	
	
	private String abortion;
	
	
	private String livingChildren;
	
	private String pastObstetricsHistory;
	
	private String contraceptionHistory;
	
	

	private String others1;

	private String bloodGroup;
	

	private String hb;
	
	private String urineProtien;
	
	private String hiv;

	private String VDRLRPR;
	
	private String malaria;

	private String antiGivenHb;
	
	private String bloodSugar;
	
	private String urineSugar;
	
	private String HBsAg;
	
	private String syphilis;
	
	private String GDM;
	
	private String OthersSpecify2;
	
	private String medicalHOifany;



	private int status;

	

	


	public String vitalsTemparature;

	public String vitalsPulse;
	
	public String vitalsWeight;

	public String vitalsRespiratoryRate;

	public String vitalsFhr;

	public String vitalsBpSystolic;

	public String vitalsBpDiastolic;
	
	
	

	public String paPresentingPart;

	public String paMultiplePregnancy;

	public String paFundalHeight;


	
	

	public String pvExamCervicalDilation;

	public String pvExamCervicalEffacement;

	public String pvExamMembranes;

	public String pvExamAntibioticGiven;

	
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	
	
	
	
	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public String getField4() {
		return field4;
	}

	public void setField4(String field4) {
		this.field4 = field4;
	}

	public String getField5() {
		return field5;
	}

	public void setField5(String field5) {
		this.field5 = field5;
	}

	public String getField6() {
		return field6;
	}

	public void setField6(String field6) {
		this.field6 = field6;
	}

	public String getField7() {
		return field7;
	}

	public void setField7(String field7) {
		this.field7 = field7;
	}

	public String getField8() {
		return field8;
	}

	public void setField8(String field8) {
		this.field8 = field8;
	}

	public String getField9() {
		return field9;
	}

	public void setField9(String field9) {
		this.field9 = field9;
	}

	public String getField10() {
		return field10;
	}

	public void setField10(String field10) {
		this.field10 = field10;
	}

	public String Completeness;

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUniqueNo() {
		return uniqueNo;
	}

	public void setUniqueNo(String uniqueNo) {
		this.uniqueNo = uniqueNo;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStatename() {
		return statename;
	}

	public void setStatename(String statename) {
		this.statename = statename;
	}

	public String getDistrictname() {
		return districtname;
	}

	public void setDistrictname(String districtname) {
		this.districtname = districtname;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getBlockname() {
		return blockname;
	}

	public void setBlockname(String blockname) {
		this.blockname = blockname;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getFacilitytypename() {
		return facilitytypename;
	}

	public void setFacilitytypename(String facilitytypename) {
		this.facilitytypename = facilitytypename;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	public String getFacilityname() {
		return facilityname;
	}

	public void setFacilityname(String facilityname) {
		this.facilityname = facilityname;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getWoDo() {
		return woDo;
	}

	public void setWoDo(String woDo) {
		this.woDo = woDo;
	}

	public String getMctsNo() {
		return mctsNo;
	}

	public void setMctsNo(String mctsNo) {
		this.mctsNo = mctsNo;
	}

	public String getBooked() {
		return booked;
	}

	public void setBooked(String booked) {
		this.booked = booked;
	}

	public String getBplJsyRegister() {
		return bplJsyRegister;
	}

	public void setBplJsyRegister(String bplJsyRegister) {
		this.bplJsyRegister = bplJsyRegister;
	}

	public String getNameOfAsha() {
		return nameOfAsha;
	}

	public void setNameOfAsha(String nameOfAsha) {
		this.nameOfAsha = nameOfAsha;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getAdmissionDate() {
		return admissionDate;
	}

	public void setAdmissionDate(String admissionDate) {
		this.admissionDate = admissionDate;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getLmp() {
		return lmp;
	}

	public void setLmp(String lmp) {
		this.lmp = lmp;
	}

	public String getEdd() {
		return edd;
	}

	public void setEdd(String edd) {
		this.edd = edd;
	}

	public String getGa() {
		return ga;
	}

	public void setGa(String ga) {
		this.ga = ga;
	}

	public String getAntenatalSteroidsGiven() {
		return antenatalSteroidsGiven;
	}

	public void setAntenatalSteroidsGiven(String antenatalSteroidsGiven) {
		this.antenatalSteroidsGiven = antenatalSteroidsGiven;
	}

	public String getGravida() {
		return gravida;
	}

	public void setGravida(String gravida) {
		this.gravida = gravida;
	}

	public String getParity() {
		return parity;
	}

	public void setParity(String parity) {
		this.parity = parity;
	}

	public String getAbortion() {
		return abortion;
	}

	public void setAbortion(String abortion) {
		this.abortion = abortion;
	}

	public String getLivingChildren() {
		return livingChildren;
	}

	public void setLivingChildren(String livingChildren) {
		this.livingChildren = livingChildren;
	}

	public String getPastObstetricsHistory() {
		return pastObstetricsHistory;
	}

	public void setPastObstetricsHistory(String pastObstetricsHistory) {
		this.pastObstetricsHistory = pastObstetricsHistory;
	}

	public String getContraceptionHistory() {
		return contraceptionHistory;
	}

	public void setContraceptionHistory(String contraceptionHistory) {
		this.contraceptionHistory = contraceptionHistory;
	}

	public String getOthers1() {
		return others1;
	}

	public void setOthers1(String others1) {
		this.others1 = others1;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getHb() {
		return hb;
	}

	public void setHb(String hb) {
		this.hb = hb;
	}

	public String getUrineProtien() {
		return urineProtien;
	}

	public void setUrineProtien(String urineProtien) {
		this.urineProtien = urineProtien;
	}

	public String getHiv() {
		return hiv;
	}

	public void setHiv(String hiv) {
		this.hiv = hiv;
	}

	public String getVDRLRPR() {
		return VDRLRPR;
	}

	public void setVDRLRPR(String vDRLRPR) {
		VDRLRPR = vDRLRPR;
	}

	public String getMalaria() {
		return malaria;
	}

	public void setMalaria(String malaria) {
		this.malaria = malaria;
	}

	public String getAntiGivenHb() {
		return antiGivenHb;
	}

	public void setAntiGivenHb(String antiGivenHb) {
		this.antiGivenHb = antiGivenHb;
	}

	public String getBloodSugar() {
		return bloodSugar;
	}

	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	public String getUrineSugar() {
		return urineSugar;
	}

	public void setUrineSugar(String urineSugar) {
		this.urineSugar = urineSugar;
	}

	public String getHBsAg() {
		return HBsAg;
	}

	public void setHBsAg(String hBsAg) {
		HBsAg = hBsAg;
	}

	public String getSyphilis() {
		return syphilis;
	}

	public void setSyphilis(String syphilis) {
		this.syphilis = syphilis;
	}

	public String getGDM() {
		return GDM;
	}

	public void setGDM(String gDM) {
		GDM = gDM;
	}

	public String getOthersSpecify2() {
		return OthersSpecify2;
	}

	public void setOthersSpecify2(String othersSpecify2) {
		OthersSpecify2 = othersSpecify2;
	}

	public String getMedicalHOifany() {
		return medicalHOifany;
	}

	public void setMedicalHOifany(String medicalHOifany) {
		this.medicalHOifany = medicalHOifany;
	}



	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getVitalsTemparature() {
		return vitalsTemparature;
	}

	public void setVitalsTemparature(String vitalsTemparature) {
		this.vitalsTemparature = vitalsTemparature;
	}

	public String getVitalsPulse() {
		return vitalsPulse;
	}

	public void setVitalsPulse(String vitalsPulse) {
		this.vitalsPulse = vitalsPulse;
	}

	public String getVitalsWeight() {
		return vitalsWeight;
	}

	public void setVitalsWeight(String vitalsWeight) {
		this.vitalsWeight = vitalsWeight;
	}

	public String getVitalsRespiratoryRate() {
		return vitalsRespiratoryRate;
	}

	public void setVitalsRespiratoryRate(String vitalsRespiratoryRate) {
		this.vitalsRespiratoryRate = vitalsRespiratoryRate;
	}

	public String getVitalsFhr() {
		return vitalsFhr;
	}

	public void setVitalsFhr(String vitalsFhr) {
		this.vitalsFhr = vitalsFhr;
	}

	public String getVitalsBpSystolic() {
		return vitalsBpSystolic;
	}

	public void setVitalsBpSystolic(String vitalsBpSystolic) {
		this.vitalsBpSystolic = vitalsBpSystolic;
	}

	public String getVitalsBpDiastolic() {
		return vitalsBpDiastolic;
	}

	public void setVitalsBpDiastolic(String vitalsBpDiastolic) {
		this.vitalsBpDiastolic = vitalsBpDiastolic;
	}

	public String getPaPresentingPart() {
		return paPresentingPart;
	}

	public void setPaPresentingPart(String paPresentingPart) {
		this.paPresentingPart = paPresentingPart;
	}

	public String getPaMultiplePregnancy() {
		return paMultiplePregnancy;
	}

	public void setPaMultiplePregnancy(String paMultiplePregnancy) {
		this.paMultiplePregnancy = paMultiplePregnancy;
	}

	public String getPaFundalHeight() {
		return paFundalHeight;
	}

	public void setPaFundalHeight(String paFundalHeight) {
		this.paFundalHeight = paFundalHeight;
	}

	public String getPvExamCervicalDilation() {
		return pvExamCervicalDilation;
	}

	public void setPvExamCervicalDilation(String pvExamCervicalDilation) {
		this.pvExamCervicalDilation = pvExamCervicalDilation;
	}

	public String getPvExamCervicalEffacement() {
		return pvExamCervicalEffacement;
	}

	public void setPvExamCervicalEffacement(String pvExamCervicalEffacement) {
		this.pvExamCervicalEffacement = pvExamCervicalEffacement;
	}

	public String getPvExamMembranes() {
		return pvExamMembranes;
	}

	public void setPvExamMembranes(String pvExamMembranes) {
		this.pvExamMembranes = pvExamMembranes;
	}

	public String getPvExamAntibioticGiven() {
		return pvExamAntibioticGiven;
	}

	public void setPvExamAntibioticGiven(String pvExamAntibioticGiven) {
		this.pvExamAntibioticGiven = pvExamAntibioticGiven;
	}

	public String getCompleteness() {
		return Completeness;
	}

	public void setCompleteness(String completeness) {
		Completeness = completeness;
	}
	
	
	  
	  
	
		
	
}
