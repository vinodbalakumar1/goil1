package com.dhanush.infotech.project.GOIL1.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.dhanush.infotech.project.GOIL1.model.Admission;
import com.dhanush.infotech.project.GOIL1.model.L1CSVModel;
import com.dhanush.infotech.project.GOIL1.repository.AdmissionRepository;
import com.dhanush.infotech.project.GOIL1.repository.L1CSVRepository;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class L1CSVController 
{
	@Autowired 
	L1CSVRepository l1csvRepository;
	
	@Autowired
	AdmissionRepository admissionRepository;
	
	 
	 @CrossOrigin
	 @RequestMapping(value = "l1CSVDownload", method = RequestMethod.GET )
	 private void getCSVDownload(@RequestParam("state_code") String state_code,@RequestParam ("district_code") String district_code
			 ,@RequestParam ("block_code") String block_code,@RequestParam ("facility_code") String facility_code,@RequestParam ("facility_type_id") String facility_type_id,@RequestParam("input_year") String input_year,@RequestParam("input_month") String input_month,HttpServletResponse response) throws Exception
	{
		 
       
			                                                                                                                                 	
			 String state= state_code ;                                                                  	
			String district=district_code.equalsIgnoreCase("ALL") ?"" :district_code;           	
			String block=block_code.equalsIgnoreCase("ALL") ?"" :block_code ;                   	
			String facility=facility_code.equalsIgnoreCase("ALL") ?"" :facility_code ;          	
			String facilityType=facility_type_id.equalsIgnoreCase("ALL") ?"" :facility_type_id ;	 
		    String year=input_year.equalsIgnoreCase("ALL") ?"%" :input_year+"-" ;                          
		    String  month=input_month.equalsIgnoreCase("ALL")?"":input_month.length()==0 ?"0"+input_month+"-" : input_month+"-" ;                       
		    String date=year+""+month;                                                                                                       
		    List<L1CSVModel> li=null;
		    
		    
		     if(state.length()>0 && district.length()==0 && block.length()==0 && facility.length()==0 && facilityType.length()==0)             
		     {		 		                                                                                                                  
		      li=l1csvRepository.findStateDetails(state,date);                                                                              
		     }                                                                                                                                 
			if(state.length()>0 && district.length()>0 && block.length()==0 && facility.length()==0 && facilityType.length()==0)                                                                                                                   
		     {		 		                                                                                                                  
	         	 li=l1csvRepository.findDistrictDetails(state,district,date);		                                                      
	         }                                                                                                                                 
	         if(state.length()>0 && district.length()>0 && block.length()>0 && facilityType.length()==0 && facility.length()==0)               
	         {		 		                                                                                                                  
           	 li=l1csvRepository.findBlockDetails(state,district,block,date);                                                           
           }                                                                                                                                 
           if(state.length()>0 && district.length()>0 && block.length()>0 && facilityType.length()>0 && facility.length()==0)                
	         {                                                                                                                                 
		     	                                                                                                                              
	         li=l1csvRepository.findFacilityTypeDetails(state,district,block,facilityType,date);         
	         System.out.println("li--"+li.size());
	                                                                                                                                           
	         }                                                                                                                                 
	         if(state.length()>0 && district.length()>0 && block.length()>0 && facilityType.length()>0 && facility.length()>0)                 
	         {                                                                                                                                 
	         	                                                                                                                              
	          li=l1csvRepository.findFacilityDetails(state,district,block,facilityType,facility,date);                                      
		                                                                                                                                       
	         }  
	         
	            String csvFileName = "csvReport.csv";                                                                           
	                                                                                                                            
	               response.setContentType("text/csv");                                                                         
	                                                                                                                            
	               String headerKey = "Content-Disposition";                                                                    
	               String headerValue = String.format("attachment; filename=\"%s\"",csvFileName);                               
	               response.setHeader(headerKey, headerValue);                                                                  
	                                                                                                                            
	                                                                                                                            
	                ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),CsvPreference.STANDARD_PREFERENCE);    
	                
	                       String[] header = {"statename","districtname","facilityname","facilitytypename","birthweight","breast_feeding_initiated","congenital_anomaly",
	                    		   "cried_immediately_after_birth","delivery_notes_time","delivery_outcome1","delivery_outcome2","if_yes_time_of_initiation",
	                    		   "preterm","resuscitated","sex_of_baby","specify","any_other_drug_specify","baby_referred_ward",
	                    		   "cct_ward","complications_during_delivery_ward","condition_of_baby","date_of_transfer_to_pnc_ward","details_of3rd4th_stage_of_labor",
	                    		   "episiotomy_given_ward","if_yes_dose","if_yesgive_reason_ward","inj_vitaminkadministered","placenta_complete_ward","ppiucd_inserted_ward","time_of_transfer_to_pnc_ward",
	                    		   "ut_massage_ward","unique_id","Baby1","Baby2","Baby3","vaccination_done","completeness",
	                    		   "gdm","hbs_ag","others_specify2","vdrlrpr","abortion","admission_date","age","antenatal_steroids_given","anti_given_hb",
	                    		   "blood_group","blood_sugar","booked","bpl_jsy_register","contraception_history","edd","ga",
	                    		   "gravida","hb","hiv","is_active","living_children","lmp","malaria","mcts_no",
	                    		   "medicalhoifany","name_of_asha","others1","pa_fundal_height","pa_multiple_pregnancy","pa_presenting_part",
	                    		   "parity","past_obstetrics_history","patient_name","pv_exam_antibiotic_given","pv_exam_cervical_dilation","pv_exam_cervical_effacement",
	                    		   "pv_exam_membranes","status","syphilis","time","unique_no","urine_protien","urine_sugar","vitals_bp_diastolic",
	                    		   "vitals_bp_systolic","vitals_fhr","vitals_pulse","vitals_respiratory_rate",
	                    		   "vitals_temparature","vitals_weight","wo_do","advice_of_baby","advice_of_mother","condition_of_mother","others"  
	                       
	                       };
	                   csvWriter.writeHeader(header);                          
	                                                                         
	                 for (L1CSVModel data : li) {                             
	                 	try {                                               
	                 	                                                    
	                 	      System.out.println("data--"+data);                                              
	                     csvWriter.write(data,header);                       
	                 	}catch(Exception e)                                 
	                 	{                                                   
	                 		e.printStackTrace();                            
	                 		System.out.println("exceptionnnnnnnnn----"+e);  
	                 		                                                
	                 	}                                                   
	                 }                                                       
	                                                                         
	                 csvWriter.close();
			
	         
	}
	
		
		@CrossOrigin
		@RequestMapping(value = "/getl1beneficiaryreport", method = RequestMethod.POST, produces = { "application/json" })
		public Map<String, Object> getChildIdByUniqueID(@RequestBody String userJsonReq) {
			org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
			Map<String, Object> error = new HashMap<>(); 
			Map<String, Object> data = new HashMap<>();
			List<Map<String,Object>> arr = new ArrayList<>();
			try {
				int i = 0;
				i+=  Integer.parseInt(openJson.getString("state_code").length() > 0 ? "1" : "0");
				i+=   Integer.parseInt(!openJson.getString("district_code").toString().equalsIgnoreCase("ALL") ? "1" :"0");
				i+=  Integer.parseInt(!openJson.getString("block_code").toString().equalsIgnoreCase("ALL") ? "1" :"0");
				i+=   Integer.parseInt(!openJson.getString("facility_type_id").toString().equalsIgnoreCase("ALL") ? "1" :"0");
				i+=   Integer.parseInt(!openJson.getString("facility_code").toString().equalsIgnoreCase("ALL") ? "1" :"0");
				
		 	String year=openJson.getString("input_year").equalsIgnoreCase("ALL") ?"%" :openJson.getString("input_year")+"-" ;
		 	String month=openJson.getString("input_month").equalsIgnoreCase("ALL")?"%":openJson.getString("input_month").length()==1 ? "0"+openJson.getString("input_month")+"%" :openJson.getString("input_month") +"%";
		 	String date=year+""+month;
		 	System.out.println(date);
			List<Admission> admission = null;
		 	switch(i) {
		 	case 1 :
			 	admission = admissionRepository.findByStateAndAdmissionDateLike(openJson.getString("state_code"),date);
			 	break;
		 	case 2 :
		 		admission=admissionRepository.findByStateAndDistrictAndAdmissionDateLike(openJson.getString("state_code"),openJson.getString("district_code"),date);
		 	break;
		 	case 3 :
		 		admission=admissionRepository.findByStateAndDistrictAndBlockAndAdmissionDateLike(openJson.getString("state_code"),openJson.getString("district_code"),openJson.getString("block_code"),date);
		 	break;
		 	case 4 :
		 		admission=admissionRepository.findByStateAndDistrictAndBlockAndFacilityTypeAndAdmissionDateLike(openJson.getString("state_code"),openJson.getString("district_code"),openJson.getString("block_code"),openJson.getString("facility_type_id"),date);
		 	break;
		 	case 5 :
		 		admission=admissionRepository.findByStateAndDistrictAndBlockAndFacilityTypeAndFacilityAndAdmissionDateLike(openJson.getString("state_code"),openJson.getString("district_code"),openJson.getString("block_code"),openJson.getString("facility_type_id"),openJson.getString("facility_code"),date);
		 	break;
		 	default:
		 		break;
		 	}
			for(Admission admission1 : admission)
			{
				Map<String , Object> map  = new HashMap<>();
				map.put("case_no", admission1.getUniqueNo());
				map.put("admDate", admission1.getAdmissionDate());
				map.put("beneficiary_name", admission1.getPatientName());
				map.put("mobile_no","");
				map.put("pcts_no","");
				map.put("status", admission1.getCompleteness());
				arr.add(map);
			}
			data.put("message", arr.size()+" Records Found");
			data.put("result", arr);
			data.put("total", arr.size());
			data.put("success",true);
			data.put("error", error);
			}
			catch (Exception e) {
				error.put("error", e.getMessage());
				data.put("message", arr.size()+" Records Found");
				data.put("result", arr);
				data.put("total", arr.size());
				data.put("success",true);
				data.put("error", error);
			}
			return data;
		}
	 
	 
	
	
	

}
