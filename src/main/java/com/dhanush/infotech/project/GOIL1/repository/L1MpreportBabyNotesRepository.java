package com.dhanush.infotech.project.GOIL1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dhanush.infotech.project.GOIL1.model.L1MpreportBabyNotesView;

@Repository
public interface L1MpreportBabyNotesRepository extends JpaRepository<L1MpreportBabyNotesView, Long>
{

	
	//for obstructed labour
	
	

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.admissiondate like :start%  and a.complications like '%99%' ")
	 Long findStateObstructedLabor(@Param("state")String state, @Param("start") String start);

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.admissiondate like :start% and a.complications like '%99%' ")
	 Long findDistrictObstructedLabor(@Param("state")String state,@Param("district")String district, @Param("start")String start);

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start%  and a.complications like '%99%' ")
	 Long findBlockObstructedLabor(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and a.complications like '%99%' ")
	 Long findFacilitytypeObstructedLabor(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	 @Query(value = "select  count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.complications like '%99%' ")
	 Long findFacilityCodeObstructedLabor(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
	   @Param("start")String start);

	 
	 
	 // for maternal death
	 

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.admissiondate like :start%  and a.complications like '%473%' ")
	 Long findStateMaternalDeaths(@Param("state")String state, @Param("start") String start);

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.admissiondate like :start% and a.complications like '%473%' ")
	 Long findDistrictMaternalDeaths(@Param("state")String state,@Param("district")String district, @Param("start")String start);

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start%  and a.complications like '%473%' ")
	 Long findBlockMaternalDeaths(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and a.complications like '%473%' ")
	 Long findFacilitytypeMaternalDeaths(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	 @Query(value = "select  count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.complications like '%473%' ")
	 Long findFacilityCodeMaternalDeaths(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
	   @Param("start")String start);
	 
	 
	 // for elampsia/preeclampsia
	 

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.admissiondate like :start% and (complications like '%96%' or complications like '%97%') ")
	 Long findByStateEclampsia(@Param("state")String state, @Param("start") String start);

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.admissiondate like :start% and (complications like '%96%' or complications like '%97%') ")
	 Long findByDistrictEclampsia(@Param("state")String state,@Param("district")String district, @Param("start")String start);

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start%  and (complications like '%96%' or complications like '%97%') ")
	 Long findByBlockEclampsia(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and (complications like '%96%' or complications like '%97%')")
	 Long findByFacilitytypeEclampsia(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	 @Query(value = "select  count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and (complications like '%96%' or complications like '%97%')")
	 Long findByFacilityCodeEclampsia(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
	   @Param("start")String start);
	 
	 
	 
	 //for sepsis
	 
	 

	 @Query(value = "select count(1) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.admissiondate like :start%  and a.complications like '%480%'")
	 Long findStateSepsis(@Param("state")String state, @Param("start") String start);

	 @Query(value = "select count(1) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.admissiondate like :start%  and a.complications like '%480%'")
	 Long findDistrictSepsis(@Param("state")String state,@Param("district")String district, @Param("start")String start);

	 @Query(value = "select count(1) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and a.complications like '%480%'")
	 Long findBlockSepsis(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

	 @Query(value = "select count(1) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start%  and a.complications like '%480%'")
	 Long findFacilitytypeSepsis(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	 @Query(value = "select  count(1) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start%  and a.complications like '%480%' ")
	 Long findFacilityCodeSepsis(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
	   @Param("start")String start);
	 
	 
	 //for pph
	 

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.admissiondate like :start% and a.complications like '%100%' ")
	 Long findStatePph(@Param("state")String state, @Param("start") String start);

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.admissiondate like :start% and a.complications like '%100%' ")
	 Long findDistrictPph(@Param("state")String state,@Param("district")String district, @Param("start")String start);

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and a.complications like '%100%' ")
	 Long findBlockPph(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

	 @Query(value = "select count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and a.complications like '%100%' ")
	 Long findFacilitytypePph(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	 @Query(value = "select  count(a.complications) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start%  and a.complications like '%100%' ")
	 Long findFacilityCodePph(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
	   @Param("start")String start);

	 // for ppiucd
	 
	

	@Query(value = "select count(a.ppiucd) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.admissiondate like :start%  and a.ppiucd in ('1','Yes')")
	Long findByStatePpiucd(@Param("state")String state, @Param("start") String start);

	@Query(value = "select count(a.ppiucd) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.admissiondate like :start%  and a.ppiucd in ('1','Yes')")
	Long findByDistrictPpiucd(@Param("state")String state,@Param("district")String district, @Param("start")String start);

	@Query(value = "select count(a.ppiucd) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and a.ppiucd in ('1','Yes')")
	Long findByBlockPpiucd(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

	@Query(value = "select count(a.ppiucd) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start%  and a.ppiucd in ('1','Yes')")
	Long findByFacilitytypePpiucd(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	@Query(value = "select  count(a.ppiucd) as count FROM L1MpreportBabyNotesView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start%  and a.ppiucd in ('1','Yes')")
	Long findByFacilityCodePpiucd(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
			@Param("start")String start);


	
}
