package com.dhanush.infotech.project.GOIL1.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dhanush.infotech.project.GOIL1.model.L1MPReportModel;


@Repository
public interface L1MPReportRepository extends JpaRepository<L1MPReportModel, Long>
{

	@Query(value = "select a FROM L1MPReportModel a where state = :state and district = :district and block = :block and facilitytype = :facilityType and facility = :facility and year = :year and month = :month")
	List<L1MPReportModel> findByFacility(@Param("state") int state,@Param("district") int district,@Param("block") int block ,@Param("facilityType") int facilityType,@Param("facility") int facility,@Param("year") int year,@Param("month") int month);

	@Query(value = "select a FROM L1MPReportModel a where a.state = :state and a.year = :year and a.month= :month ")
	List<L1MPReportModel> findByState(@Param("state")int state,@Param("year") int year,@Param("month") int month);
	
	@Query(value = "select a FROM L1MPReportModel a where a.state = :state and a.district = :district and year = :year and month = :month")
	List<L1MPReportModel> findByDistrict(@Param("state")int state,@Param("district")int district,@Param("year") int year,@Param("month") int month);

	@Query(value = "select a FROM L1MPReportModel a where a.state = :state and a.district = :district and a.block = :block  and year = :year and month = :month")
	List<L1MPReportModel> findByBlock(@Param("state")int state, @Param("district")int district,@Param("block") int block,@Param("year") int year,@Param("month") int month);

	@Query(value = "select a FROM L1MPReportModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and year = :year and month = :month")
	List<L1MPReportModel> findByFacilitytype(@Param("state")int state, @Param("district")int district,@Param("block") int block,@Param("facilitytype") int facilitytype,@Param("year") int year,@Param("month") int month);

	@Query(value = "select a FROM L1MPReportModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and year = :year and month = :month")
	List<L1MPReportModel> findByFacilityCode(@Param("state")int state,@Param("district") int district,@Param("block")  int block,@Param("facilitytype") int facilitytype,@Param("facility") int facility,
			@Param("year") int year,@Param("month") int month);

	//for getting max timestamp records month wise

	@Query(value = "select max(createdAt) FROM L1MPReportModel group by month")
	List<L1MPReportModel> findMaxTimerecords();
	
	
	
	@Query(value = "select a FROM L1MPReportModel a where a.state = :state and createdAt=:createdAt")
	List<L1MPReportModel> findByStateALL(@Param("state")int state,@Param("createdAt") Date createdAt);
	
	
	
	@Query(value = "select a FROM L1MPReportModel a where a.state = :state and a.district = :district and createdAt=:createdAt")
	List<L1MPReportModel> findByDistrictALL(@Param("state")int state,@Param("district")int district,@Param("createdAt")Date mpReport);

	@Query(value = "select a FROM L1MPReportModel a where a.state = :state and a.district = :district and a.block = :block and createdAt=:createdAt")
	List<L1MPReportModel> findByBlockALL(@Param("state")int state, @Param("district")int district,@Param("block") int block,@Param("createdAt")Date mpReport);

	@Query(value = "select a FROM L1MPReportModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and createdAt=:createdAt")
	List<L1MPReportModel> findByFacilitytypeALL(@Param("state")int state, @Param("district")int district,@Param("block") int block,@Param("facilitytype") int facilitytype,@Param("createdAt")Date mpReport);

	@Query(value = "select a FROM L1MPReportModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and createdAt=:createdAt")
	List<L1MPReportModel> findByFacilityCodeALL(@Param("state")int state,@Param("district") int district,@Param("block")  int block,@Param("facilitytype") int facilitytype,@Param("facility") int facility,@Param("createdAt")Date mpReport);

	
	
	
	@Query(value = "select max(createdAt) FROM L1MPReportModel where year=:year group by month")
	List<L1MPReportModel> findMaxTimerecordsByYear(@Param("year")int year);

	
	


	

}
