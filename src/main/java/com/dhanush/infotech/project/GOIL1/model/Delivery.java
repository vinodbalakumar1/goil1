package com.dhanush.infotech.project.GOIL1.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.dhanush.infotech.project.GOIL1.model.Admission;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "l1_delivery")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)

public class Delivery {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createdAt = new Date();

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date updatedAt = new Date();;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "admissionId", nullable = false)
	private Admission admission;

	private 	String deliveryOutcome1	;			
	private 	String deliveryOutcome2 ;			
	private 	String deliveryNotesTime ;			
	private 	String sexOfBaby         ;		
	private 	String preterm          	;		
	private 	String birthweight      	;		
	private 	String criedImmediatelyAfterBirth; 	
	private 	String resuscitated 		;		
	private 	String breastFeedingInitiated; 		
	private 	String ifYesTimeOfInitiation ;		
	private 	String congenitalAnomaly 	;		
	private 	String specify 				;
	
	
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	
	
	
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Admission getAdmission() {
		return admission;
	}
	public void setAdmission(Admission admission) {
		this.admission = admission;
	}
	public String getDeliveryOutcome1() {
		return deliveryOutcome1;
	}
	public void setDeliveryOutcome1(String deliveryOutcome1) {
		this.deliveryOutcome1 = deliveryOutcome1;
	}
	public String getDeliveryOutcome2() {
		return deliveryOutcome2;
	}
	public void setDeliveryOutcome2(String deliveryOutcome2) {
		this.deliveryOutcome2 = deliveryOutcome2;
	}
	public String getDeliveryNotesTime() {
		return deliveryNotesTime;
	}
	public void setDeliveryNotesTime(String deliveryNotesTime) {
		this.deliveryNotesTime = deliveryNotesTime;
	}
	public String getSexOfBaby() {
		return sexOfBaby;
	}
	public void setSexOfBaby(String sexOfBaby) {
		this.sexOfBaby = sexOfBaby;
	}
	public String getPreterm() {
		return preterm;
	}
	public void setPreterm(String preterm) {
		this.preterm = preterm;
	}
	public String getBirthweight() {
		return birthweight;
	}
	public void setBirthweight(String birthweight) {
		this.birthweight = birthweight;
	}
	public String getCriedImmediatelyAfterBirth() {
		return criedImmediatelyAfterBirth;
	}
	public void setCriedImmediatelyAfterBirth(String criedImmediatelyAfterBirth) {
		this.criedImmediatelyAfterBirth = criedImmediatelyAfterBirth;
	}
	public String getResuscitated() {
		return resuscitated;
	}
	public void setResuscitated(String resuscitated) {
		this.resuscitated = resuscitated;
	}
	public String getBreastFeedingInitiated() {
		return breastFeedingInitiated;
	}
	public void setBreastFeedingInitiated(String breastFeedingInitiated) {
		this.breastFeedingInitiated = breastFeedingInitiated;
	}
	public String getIfYesTimeOfInitiation() {
		return ifYesTimeOfInitiation;
	}
	public void setIfYesTimeOfInitiation(String ifYesTimeOfInitiation) {
		this.ifYesTimeOfInitiation = ifYesTimeOfInitiation;
	}
	public String getCongenitalAnomaly() {
		return congenitalAnomaly;
	}
	public void setCongenitalAnomaly(String congenitalAnomaly) {
		this.congenitalAnomaly = congenitalAnomaly;
	}
	public String getSpecify() {
		return specify;
	}
	public void setSpecify(String specify) {
		this.specify = specify;
	}		
	

}
