package com.dhanush.infotech.project.GOIL1.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL1.repository.L1ComplicationsRepository;
import com.dhanush.infotech.project.GOIL1.repository.LiveCaesareanRepository;
import com.dhanush.infotech.project.GOIL1.repository.RecordingMotherBPRepository;
import com.dhanush.infotech.project.GOIL1.repository.TotalDeliveryRepository;

@RestController
@RequestMapping("/api")
@CrossOrigin

public class FacilityDashBoard {
	
	public String TOTALADMISSIONS ="facility_cases_count";
	public String TOTALDELIVERIES ="facility_delivery_count";
	public String MOTHERBPSTATE ="state_mother_bp";
	public String MOTHERBPFACILITY ="facility_mother_bp";
	public String STATEECLAMPSIA="state_eclampsia";
	public String FACILITYECLAMPSIA ="facility_eclampsia";
	public String LABLES = "labels";
	@Autowired
	TotalDeliveryRepository totalDeliveryRepository ;
	
	@Autowired
	RecordingMotherBPRepository recordingMotherBPRepository;
	
	
	@Autowired
	LiveCaesareanRepository liveCaesareanRepository;
	
	@Autowired
	L1ComplicationsRepository l1ComplicationsRepository;
	
	
	public String months[] = {"Jan", "Feb", "Mar", "Apr",
            "May", "June", "July", "Aug", "Sep",
            "Oct", "Nov", "Dec"};
	
	
	public   String month[] = {"01","02","03","04","05","06","07","08","09","10","11","12"};

	@CrossOrigin
	@RequestMapping(value = "facilitydashboard/l1", method = RequestMethod.POST, produces = { "application/json" })
	public HashMap<String, Object> createUser(@RequestBody String userJsonReq) throws  Exception {
		HashMap<String, Object> report = new HashMap<>();
		HashMap<String, Object> data = new HashMap<>();
		HashMap<String, Object> error = new HashMap<String, Object>();
		org.json.JSONObject obj = new org.json.JSONObject(userJsonReq);
		String  block = obj.getString("block_code").toString();
		String district = obj.getString("district_code").toString();
		String state = obj.getString("state_code").toString();
		String facility = obj.getString("facility_code").toString();
		String facilitytype = obj.getString("facility_type_id").toString();
		int year = Integer.parseInt(obj.getString("input_year").toString());
		int Imonth =  Integer.parseInt(obj.getString("input_month").toString());
		HashMap<String, Object> report1 = new HashMap<>();
		int[] deliveries = new int[6];
		int[] admissions = new int[6];
		int[] mother_state = new int[6];
		int[] motherBp_facility = new int[6];
		int[] eclampsia_state = new int[6];
		int[] eclampsia_facility = new int[6];
		String[] lablels = new String[6];
		  for(int i = 0;i < 6;i++){
			     String date = year+"-"+month[Imonth-1]+"-";
			     lablels[i]=	year+"-"+months[Imonth-1];
			     deliveries[i] = Integer.parseInt(totalDelivery(state, district, block, facilitytype, facility, date));
			     admissions[i] = Integer.parseInt(totalAdmissions(state, district, block, facilitytype, facility, date)); 
			     mother_state[i] = Integer.parseInt(MotherBPState(state,date));
			     motherBp_facility[i] = Integer.parseInt(motherBpFacility( state, district, block, facilitytype, facility, date));
			     eclampsia_state[i] = Integer.parseInt(stateEclampsia( state, date));
			     eclampsia_facility[i] = Integer.parseInt(facilityEclampsia( state, district, block, facilitytype, facility, date));
			     Imonth = Imonth-1;
			     if(Imonth == 0) {
			    	 Imonth = 12; year = year-1;
			     }
		  }
		  
		  int len= lablels.length-1;
		  String[] rev=new String[len+1];
		  int[] deliveries1 = new int[len+1],admissions1 = new int[len+1],mother_state1 = new int[len+1],motherBp_facility1 = new int[len+1],eclampsia_state1 = new int[len+1],eclampsia_facility1 = new int[len+1];
		  for(int i = len ; i >= 0; i--)
		  {
			  rev[len-i] = lablels[i];
			  deliveries1[len-i] = deliveries[i];
			  admissions1[len-i]=admissions[i];
			  mother_state1[len-i]=mother_state[i];
			  motherBp_facility1[len-i]=motherBp_facility[i];
			  eclampsia_state1[len-i]=eclampsia_state[i];
			  eclampsia_facility1[len-i]=eclampsia_facility[i];
		  }
		  
		  report.put(TOTALADMISSIONS, admissions1);
		  report.put(TOTALDELIVERIES,deliveries1);
		  report.put(MOTHERBPSTATE,mother_state1);
		  report.put(MOTHERBPFACILITY,motherBp_facility1);
		  report.put(STATEECLAMPSIA,eclampsia_state1);
		  report.put(FACILITYECLAMPSIA,eclampsia_facility1);
		  report.put(LABLES,rev);
		report1.put("report", report);
		report1.put("total",7);
		report1.put("message", "7 Records Found");
		report1.put("error", error);
		report1.put("success", true);
		data.put("data", report1);
				return data;
	}
	
	  public String totalDelivery(String state,String district,String block,String facilitytype,String facility,String date)
	   {
		 Long total ;
		 total = totalDeliveryRepository.FacilityDashBoard(state,district,block,facilitytype,facility,date);
		 if (total==null)
		 {
			 total = (long) 0;
		 }
		 return total.toString();
	   }
	  
	  
	  public String totalAdmissions(String state,String district,String block,String facilitytype,String facility,String date)
	   {
		 Long total;
		 total = recordingMotherBPRepository.FacilityDashBoard(state,district,block,facilitytype,facility,date);
		 if (total==null)
		 {
			 total = (long) 0;
		 }
		 return total.toString();
	   }
	  
	  
	   @SuppressWarnings("unused")
	public String MotherBPState (String state,String date)
	   {
		   Long total = 0L;
		   Long admission = 0L;
				 total = recordingMotherBPRepository.MotherBpState(state,date)*100;
				 admission = recordingMotherBPRepository.FacilityDashBoardstate(state,date);
		   Long result;
		   if( total == 0 || total == null && admission == 0 || admission ==0) {
			 result = (long) 0;
			 
		   }
		   else {
		    result = total/admission;	
		   }
		   return result.toString();
	   }
	   
	   
	   
	   public String motherBpFacility(String state,String district,String block,String facilitytype,String facility,String date)
	   {
		   Long total = 0L;
		   Long admission = 0L;
				 total = recordingMotherBPRepository.MotherBpFacility(state,district,block,facilitytype,facility,date)*100;
				 admission = recordingMotherBPRepository.FacilityDashBoardFacility(state,district,block,facilitytype,facility,date);
		  
		   Long result;
		   if( total == 0 || total == null && admission == 0 || admission ==0) {
			 result = (long) 0;
			 
		   }
		   else {
		    result = total/admission;	
		   }
		   return result.toString();
	   }
	
	   
	   
		public String stateEclampsia(String state,String date)
		   {
			   Long total = 0L;
			   Long deliveries =0L;
			  
			   total = l1ComplicationsRepository.FacilityDashBoardState(state,date)*100;
			   deliveries = totalDeliveryRepository.FacilityDashBoardState(state,date);
			   Long result;
			   if(total == null || total == 0 && deliveries == null || deliveries == 0) {
				   total = 0L;
				   deliveries = 0L;
				   result = (long) 0;
			   }
			   else {
			   result = total/deliveries;
			   }
			   System.out.println(result);
			   return result.toString();
		   } 
	   
	   
		public String facilityEclampsia(String state,String district,String block,String facilitytype,String facility,String date)
		   {
			   Long total = 0L;
			   Long deliveries =0L;
			  

					  total = l1ComplicationsRepository.FacilityDashBoardFility(state,district,block,facilitytype,facility,date)*100;
					 deliveries = totalDeliveryRepository.FacilityDashBoard(state,district,block,facilitytype,facility,date);
			
			   Long result;
			   if(total == null || total == 0 && deliveries == null || deliveries == 0) {
				   total = 0L;
				   deliveries = 0L;
				   result = (long) 0;
			   }
			   else {
			   result = total/deliveries;
			   }
			   return result.toString();
		   } 
	   
	   
	   
}
	
	
