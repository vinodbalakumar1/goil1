package com.dhanush.infotech.project.GOIL1.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="l1mpreportbabynotes")
public class L1MpreportBabyNotesView implements Serializable
{

	private static final long serialVersionUID = 1L;
	@Id
	private Long id;
	@Column(name="state")
	public String state;
	@Column(name="district")
	public String district;
	@Column(name="block")
	public String block;
	@Column(name="facility")
	public String facility;
	@Column(name="facilitytype")
	public String facilitytype;
	@Column(name="admissiondate")
	public String admissiondate;
	@Column(name="complications_during_delivery_ward")
	public String complications;
	@Column(name="ppiucd_inserted_ward")
	public String ppiucd;
	
	
	
	
	
	public String getPpiucd() {
		return ppiucd;
	}
	public void setPpiucd(String ppiucd) {
		this.ppiucd = ppiucd;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}
	public String getFacility() {
		return facility;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	public String getFacilitytype() {
		return facilitytype;
	}
	public void setFacilitytype(String facilitytype) {
		this.facilitytype = facilitytype;
	}
	public String getAdmissiondate() {
		return admissiondate;
	}
	public void setAdmissiondate(String admissiondate) {
		this.admissiondate = admissiondate;
	}
	public String getComplications() {
		return complications;
	}
	public void setComplications(String complications) {
		this.complications = complications;
	}

	
	
}
