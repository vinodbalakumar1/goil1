package com.dhanush.infotech.project.GOIL1.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL1.model.Admission;
import com.dhanush.infotech.project.GOIL1.repository.AdmissionRepository;
import com.dhanush.infotech.project.GOIL1.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api")
public class GetAdmissionDetailsController {

	@Autowired
	AdmissionRepository admissionRepository;


	@CrossOrigin
	@RequestMapping(value = "/getadmissiondetailsbyid/l1/{id}", method = RequestMethod.GET, produces = {
			"application/json" })
	public String getUserById(@PathVariable(value = "id") Long UserId) {
		Admission dischargeDetails = admissionRepository.findById(UserId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", UserId));
		JSONObject normal = new JSONObject();
		JSONObject obj = new JSONObject();
		try {
		obj.put("admissionId", ""+dischargeDetails.getId());
		obj.put("unique_id", dischargeDetails.getUniqueNo());
		obj.put("state", dischargeDetails.getState());
		obj.put("district", dischargeDetails.getDistrict());
		obj.put("block", dischargeDetails.getBlock());
		obj.put("facility", dischargeDetails.getFacility());
		obj.put("facilityType", dischargeDetails.getFacilityType());

		JSONObject obj1 = new JSONObject();
		obj1.put("patientName", dischargeDetails.getPatientName());
		obj1.put("age", dischargeDetails.getAge());
		obj1.put("woDo", dischargeDetails.getWoDo());
		obj1.put("mctsNo", dischargeDetails.getMctsNo());
		obj1.put("booked", dischargeDetails.getBooked());
		obj1.put("bplJsyRegister", dischargeDetails.getBplJsyRegister());
		
		obj1.put("nameOfFacility",dischargeDetails.getFacilityname());
		obj1.put("nameOfAsha", dischargeDetails.getNameOfAsha());
		
		
		obj1.put("admissionDate", dischargeDetails.getAdmissionDate());
		obj1.put("time", dischargeDetails.getTime());
		obj1.put("lmp", dischargeDetails.getLmp());
		obj1.put("edd", dischargeDetails.getEdd());
		obj1.put("ga", dischargeDetails.getGa());
		obj1.put("antenatalSteroidsGiven", dischargeDetails.getAntenatalSteroidsGiven());
		obj1.put("gravida", dischargeDetails.getGravida());
		obj1.put("parity", dischargeDetails.getParity());
		obj1.put("abortion", dischargeDetails.getAbortion());
		obj1.put("livingChildren", dischargeDetails.getLivingChildren());
		obj1.put("pastObstetricsHistory", dischargeDetails.getPastObstetricsHistory());
		obj1.put("contraceptionHistory", dischargeDetails.getContraceptionHistory());
		obj1.put("othersSpecify", dischargeDetails.getOthers1());
		obj1.put("bloodGroup", dischargeDetails.getBloodGroup());
		obj1.put("hb", dischargeDetails.getHb());
		obj1.put("urineProtein", dischargeDetails.getUrineProtien());
		obj1.put("hiv", dischargeDetails.getHiv());
		obj1.put("VDRLRPR", dischargeDetails.getVDRLRPR());
		obj1.put("malaria", dischargeDetails.getMalaria());
		obj1.put("antiGivenHb", dischargeDetails.getAntiGivenHb());
		obj1.put("bloodSugar", dischargeDetails.getBloodSugar());
		obj1.put("urineSugar", dischargeDetails.getUrineSugar());
		obj1.put("HBsAg", dischargeDetails.getHBsAg());
		obj1.put("syphilis", dischargeDetails.getSyphilis());
		obj1.put("GDM", dischargeDetails.getGDM());
		obj1.put("OthersSpecify1", dischargeDetails.getOthersSpecify2());
		obj1.put("presentho",dischargeDetails.getAddress());
		obj1.put("medicalHOifany", dischargeDetails.getMedicalHOifany());
		
		
		
		JSONObject vitals = new JSONObject();
		vitals.put("vitalsBpDiastolic", dischargeDetails.getVitalsBpDiastolic());
		vitals.put("vitalsBpSystolic",dischargeDetails.getVitalsBpSystolic());
		vitals.put("vitalsTemparature", dischargeDetails.getVitalsTemparature());
		vitals.put("vitalsPulse", dischargeDetails.getVitalsPulse());
		vitals.put("vitalsRespiratoryRate", dischargeDetails.getVitalsRespiratoryRate());
		vitals.put("vitalsFhr", dischargeDetails.getVitalsFhr());
		vitals.put("vitalsWeight",dischargeDetails.getVitalsWeight());

		JSONObject pAExamination = new JSONObject();
		pAExamination.put("presentingpart",
				dischargeDetails.getPaPresentingPart());
		pAExamination.put("multiplepregnancy", dischargeDetails.getPaMultiplePregnancy());
		pAExamination.put("fundalheight", dischargeDetails.getPaFundalHeight());

	

		JSONObject pVExamination = new JSONObject();

	
		pVExamination.put("cervicalDilation", dischargeDetails.getPvExamCervicalDilation());
		pVExamination.put("effacement", dischargeDetails.getPvExamCervicalEffacement());
		pVExamination.put("membranes", dischargeDetails.getPvExamMembranes());
		pVExamination.put("antibioticgiven", dischargeDetails.getPvExamAntibioticGiven());
		
		JSONObject others = new JSONObject();
		others.put("field1", dischargeDetails.getField1());
		others.put("field2", dischargeDetails.getField2());
		others.put("field3", dischargeDetails.getField3());
		others.put("field4", dischargeDetails.getField4());
		others.put("field5", dischargeDetails.getField5());
		others.put("field6", dischargeDetails.getField6());
		others.put("field7", dischargeDetails.getField7());
		others.put("field8", dischargeDetails.getField8());
		others.put("field9", dischargeDetails.getField9());
		others.put("field10", dischargeDetails.getField10());
		

	
	
		obj.put("admission", obj1);
		obj.put("vitals", vitals);
		obj.put("others", others);
		obj.put("pAExamination", pAExamination);
		obj.put("pVExamination", pVExamination);
		normal.put("status", "ok");
		normal.put("data", obj);
	}
		catch(Exception e)
		{
			normal.put("status", "No Record Found");
			normal.put("data", obj);
		}
		return normal.toString();
	}
}
