package com.dhanush.infotech.project.GOIL1.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "l1_totaldelivery")
public class TotalDelivery implements Serializable  {
	@Id
	private Long id;
	private static final long serialVersionUID = 1L;
	@Column(name="delivery")
	public String delivery;
	@Column(name="state")
	public String state;
	@Column(name="district")
	public String district;
	@Column(name="block")
	public String block;
	@Column(name="facility")
	public String facility;
	@Column(name="createdat")
	public String createdat;
	@Column(name="facilitytype")
	public String facilitytype;
	
	@Column(name="deliverytype")
	public String deliverytype;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	public String getDelivery() {
		return delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getBlock() {
		return block;
	}


	public void setBlock(String block) {
		this.block = block;
	}



}
