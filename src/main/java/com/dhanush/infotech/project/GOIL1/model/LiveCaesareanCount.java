package com.dhanush.infotech.project.GOIL1.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "l1_deliveryoutcome")
public class LiveCaesareanCount implements Serializable  {
	@Id
	private Long id;
	private static final long serialVersionUID = 1L;
	@Column(name="state")
	public String state;
	@Column(name="district")
	public String district;
	@Column(name="block")
	public String block;
	@Column(name="facility")
	public String facility;
	@Column(name="createdat")
	public String createdat;
	@Column(name="facilitytype")
	public String facilitytype;
	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}





	public String getFacility() {
		return facility;
	}



	public void setFacility(String facility) {
		this.facility = facility;
	}



	public String getCreatedat() {
		return createdat;
	}



	public void setCreatedat(String createdat) {
		this.createdat = createdat;
	}



	public String getFacilitytype() {
		return facilitytype;
	}



	public void setFacilitytype(String facilitytype) {
		this.facilitytype = facilitytype;
	}






	public String getDeliveryoutcome1() {
		return deliveryoutcome1;
	}



	public void setDeliveryoutcome1(String deliveryoutcome1) {
		this.deliveryoutcome1 = deliveryoutcome1;
	}


	@Column(name="deliveryoutcome1")
	public String deliveryoutcome1;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getBlock() {
		return block;
	}


	public void setBlock(String block) {
		this.block = block;
	}



}
