package com.dhanush.infotech.project.GOIL1.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL1.repository.L1ComplicationsRepository;
import com.dhanush.infotech.project.GOIL1.repository.RecordingMotherBPRepository;
import com.dhanush.infotech.project.GOIL1.repository.SDtotalDeliveryViewRepository;
import com.dhanush.infotech.project.GOIL1.repository.StateDashBoardDeliveryView;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class StateDashBoard {
	
	@Autowired 
	StateDashBoardDeliveryView stateDashBoardDeliveryView;
	
	
	@Autowired
	
	SDtotalDeliveryViewRepository sDtotalDeliveryViewRepository;
	
	@Autowired
	RecordingMotherBPRepository recordingMotherBPRepository;
	
	@Autowired
	
	L1ComplicationsRepository l1ComplicationsRepository;
	
	
	 ArrayList<String> list = new ArrayList<>();
	
	@SuppressWarnings("unchecked")
	@CrossOrigin
	@RequestMapping(value = "statedashboard/l1", method = RequestMethod.POST, produces = { "application/json" })
	public String stateDashBoard(@RequestBody String userJsonReq) throws  Exception {
		org.json.JSONObject obj = new org.json.JSONObject(userJsonReq);
		Map<String,Object> kpiResult = new HashMap<>();
		JSONObject test = new JSONObject();
		JSONObject test1 = new JSONObject();
		String district = obj.getString("district_code").toString();
		String state = obj.getString("state_code").toString();
		String year =obj.getString("year").toString().equalsIgnoreCase("ALL") ? "%" :obj.getString("year").toString()+"-";
		  String month =obj.getString("month").toString().equalsIgnoreCase("ALL") ? "":obj.getString("month").toString().length()==1 ? "0"+obj.getString("month").toString()+"-%" : obj.getString("month").toString()+"-%";
		String date = year+""+month;
		JSONArray arr =totalDelivery(state,district,date);
		test.put("total", arr.length());
		test.put("result",arr);
		test.put("message", arr.length()+" Records Found");
		test1.put("error", kpiResult);
		test1.put("success", true);
		test1.put("data", test);
		return test1.toString();
		
	}
	
	
	public JSONArray totalDelivery(String state,String district,String date)
	   {
		JSONArray method = null;
		 JSONArray xcv = new JSONArray();
		 JSONArray xcu = new JSONArray();;
		 JSONArray facil = new JSONArray();;
		 JSONArray livebirth = new JSONArray();;
		 JSONArray motherbparr = new JSONArray();;
		 JSONArray astmlarr = new JSONArray();;
		 JSONArray mothertemparr = new JSONArray();;
		 JSONArray partographarr = new JSONArray();;
		 JSONArray Eclampsiaarr = new JSONArray();;
		 JSONArray ppharr = new JSONArray();;
		 JSONArray prolongarr = new JSONArray();;
		 JSONArray caesareanarr = new JSONArray();;
		 JSONArray neontalarr = new JSONArray();;
		 JSONArray stillarr = new JSONArray();;
		 JSONArray babyarr = new JSONArray();;
		
		 
		
		   if (state.length() > 0 && district.equalsIgnoreCase("ALL")) {
				 List<Object[]> totalde = stateDashBoardDeliveryView.objByState(state,date);
				 Map<Object,Object> objde = new HashMap<>();
				 for (int i = 0; i < totalde.size(); i++) {
					  Object[] arr = totalde.get(i);
					  Long l = new Long( (long) arr[0]);
						String district1 = arr[1].toString();
						objde.put(district1, l);
						 
					  }
				
				 
				 List<Object[]>  livebirths1 = stateDashBoardDeliveryView.objByState0(state,date);
				 Map<Object,Object> objbir = new HashMap<>();
				 for (int i = 0; i < livebirths1.size(); i++) {
					  Object[] arr = livebirths1.get(i);
						Long l = (long)arr[0];
						String district1 = arr[1].toString();
						objbir.put(district1, l);
						  
					  }
				 List<Object[]>	 motherbp1=recordingMotherBPRepository.objState(state,date);
				 Map<Object,Object> objbp = new HashMap<>();
				 for (int i = 0; i < motherbp1.size(); i++) {
					  Object[] arr = motherbp1.get(i);
						Long l = (long)arr[0];
						
						String district1 = arr[1].toString();
						objbp.put(district1, l);
						
					  }
				 List<Object[]>	ad =recordingMotherBPRepository.objByState(state,date);
				 Map<Object,Object> obj19 = new HashMap<>();
				 for (int i = 0; i < ad.size(); i++) {
					  Object[] arr = ad.get(i);
						Long l = (long)arr[0];
						String district1 = arr[1].toString();
						obj19.put(district1, l);
						
					  }
				 
/*				
				 List<Object[]>	amts =stateDashBoardDeliveryView.objByState1(state,date);
					Map<Object,Object> objamt = new HashMap<>();
				 for (int i = 0; i < amts.size(); i++) {
					  Object[] arr = amts.get(i);
						Long l = (long)arr[0];
						String district1 = arr[1].toString();
						objamt.put(district1, l);
						  
					  }*/
				 
				 
	/*			 List<Object[]>	moth =motherTemperatureatDischargeViewRepository.objByState(state,date);
				 Map<Object,Object> objmote = new HashMap<>();
				 for (int i = 0; i < moth.size(); i++) {
					  Object[] arr = moth.get(i);
						Long l = (long)arr[0];
						
						String district1 = arr[2].toString();
						objmote.put(district1, l);
						 
					  }*/
				 
		/*		 List<Object[]>	 par = partographMonitoringViewRepository.objByState1(state, date);
				 Map<Object,Object> objpar = new HashMap<>();
				 for (int i = 0; i < par.size(); i++) {
					  Object[] arr = par.get(i);
						Long l = (long)arr[0];
						String district1 = arr[2].toString();
						objpar.put(district1, l);
						 
					  }*/
				 
				 List<Object[]> ecl  = l1ComplicationsRepository.objByState5(state, date);
				 Map<Object,Object> objecl = new HashMap<>();
				 for (int i = 0; i < ecl.size(); i++) {
					  Object[] arr = ecl.get(i);
						Long l = (long)arr[0];
						
						String district1 = arr[2].toString();
						objecl.put(district1, l);
						  
					  }
				 
				 // 
				 List<Object[]> pp = l1ComplicationsRepository.objByState6(state, date);
				 Map<Object,Object> objpp = new HashMap<>();
				 for (int i = 0; i < pp.size(); i++) {
					  Object[] arr = pp.get(i);
						Long l = (long)arr[0];
						
						String district1 = arr[2].toString();
						objpp.put(district1, l);
					  }
				
				 
				 
				 // prolong
				 
/*				 List<Object[]> pro =  stateDashBoardDeliveryView.objByState8(state, date);
				 Map<Object,Object> objpro = new HashMap<>();
				 for (int i = 0; i < pro.size(); i++) {
					  Object[] arr = pro.get(i);
						Long l = (long)arr[0];
						
						String district1 = arr[2].toString();
						objpro.put(district1, l);
					  }
				 
				 
				 
				 // caes
				 
				 List<Object[]> caes =  stateDashBoardDeliveryView.objByState9(state, date);
				 Map<Object,Object> objcaes = new HashMap<>();
				 for (int i = 0; i < caes.size(); i++) {
					  Object[] arr = caes.get(i);
						Long l = (long)arr[0];
						
						String district1 = arr[2].toString();
						objcaes.put(district1, l);
						 
					  }
				 
				List<Object[]> neon=neonatalAsphyxiaViewRepository.objState(state, date);
				Map<Object,Object> objneon = new HashMap<>();
				 for (int i = 0; i < neon.size(); i++) {
					  Object[] arr = neon.get(i);
						Long l = (long)arr[0];
						String district1 = arr[2].toString();
						objneon.put(district1, l);
						 
					  }
				 */
				 List<Object[]> sti = stateDashBoardDeliveryView.objByState3(state, date);
				 Map<Object,Object> objsti = new HashMap<>();
				 for (int i = 0; i < sti.size(); i++) {
					  Object[] arr = sti.get(i);
						Long l = (long)arr[0];
						
						
						String district1 = arr[2].toString();
						objsti.put(district1, l);
						
					  }
		/*		 List<Object[]> babyy = motherTemperatureatDischargeViewRepository.SDByState2(state, date);
				 Map<Object,Object> objbab = new HashMap<>();
				 for (int i = 0; i < babyy.size(); i++) {
					  Object[] arr = babyy.get(i);
						Long l = (long)arr[0];
						String district1 = arr[2].toString();
						objbab.put(district1, l);
						 
					  }
				 */
				 List<Object[]>	alldis1t =recordingMotherBPRepository.objtate(state,date);
					
				 for (int i = 0; i < alldis1t.size(); i++) {
					  Object[] arr = alldis1t.get(i);
						String district1 = arr[0].toString();
					
						 xcu.put(district1);
					  }
				 
				 
				 // calculation
				 int x = xcu.length();
				 for(int i =0 ; i < x;i++)
				 {
					String xyz = xcu.get(i).toString(); 
					Map<Object,Object> del = objde;
					Long delivery = (Long) del.get(xyz);
					delivery = delivery == null ? (long) 0 :delivery;
					String facily ="ALL";
					Map<Object,Object> liv = objbir;
					Long lvebr = (Long) liv.get(xyz);
					lvebr = lvebr == null ? (long) 0 : lvebr;
					Map<Object,Object> bp = objbp;
					Long bpm = (Long) bp.get(xyz);
					bpm = bpm == null ? (long) 0 : bpm*100; 
					
					Map<Object,Object> admm = obj19;
					Long admis = (Long) admm.get(xyz);
					admis = admis == null ? (long) 0 : admis;
					Long bpofmother;
					try {
					bpofmother = bpm == 0 ? 0 : bpm/admis;
					}
					catch(Exception e)
					{
						bpofmother = (long) 0;
					}
				/*	Map<Object,Object> objamt1 = objamt;
					Long amt = (Long) objamt1.get(xyz);
					
					amt = amt == null ? (long) 0 : amt;
					Long amtll ;
					try {
						amtll = amt*100/delivery;
					}
					catch (Exception e) {
						amtll = (long) 0;
					}*/
					/*Map<Object,Object> objmote1 = objmote;
					Long objmote11 = (Long) objmote1.get(xyz);
					objmote11 = objmote11 == null ? (long) 0 : objmote11;
					Long momtemp;
					try {
						momtemp = objmote11*100/admis;
						
					}
					catch(Exception e)
					{
						momtemp = (long) 0;
					}
					*/
		/*			Map<Object,Object> objpar1 = objpar;
					Long objpar11 =(Long) objpar1.get(xyz);
					objpar11 = objpar11 == null ? (long) 0 : objpar11;
					Long parato;
					try {
						parato = objpar11*100/admis; 
					}
					catch (Exception e) {
					parato = (long) 0;
					}*/
					Map<Object, Object> objecl1 = objecl;
					Long objecl11 =(Long) objecl1.get(xyz);
					objecl11 = objecl11 == null ? (long) 0 : objecl11;
					Long eclamp;
					try {
						eclamp = objecl11*100/delivery;
					}
					catch(Exception e)
					{
						eclamp = (long) 0;
					}
					Map<Object,Object> objpp1 = objpp;
					Long objpp11 =(Long) objpp1.get(xyz);
					objpp11 = objpp11 == null ? (long) 0 : objpp11;
					Long pphh;
					try {
						pphh = objpp11*100/delivery;
					}
					catch (Exception e) {
						pphh = (long) 0;
					}
				/*	Map<Object,Object> objpro1 = objpro;
					Long objpro11 = (Long) objpro1.get(xyz);
					objpro11 = objpro11 == null ? (long) 0 : objpro11;
					Long prolon;
					try {
						prolon = objpro11*100/delivery;
					}
					catch(Exception e)
					{
						prolon = (long) 0;
					}*/
				/*	Map<Object,Object>  objcaes1 = objcaes;
					Long objcaes11 = (Long) objcaes1.get(xyz);
					objcaes11 = objcaes11 == null ? (long) 0 : objcaes11;
					Long caesern;
					try
					{
						caesern = objcaes11*100/delivery;
					}
					catch (Exception e) {
						caesern = (long) 0; 
					}
					Map<Object,Object> objneon1 = objneon;
					Long objneon11 = (Long) objneon1.get(xyz);
					objneon11 = objneon11 == null ? (long) 0 : objneon11;
					Long neona;
					try {
						neona = objneon11*100/lvebr;
					}
					catch (Exception e) {
						neona = (long) 0;
					}*/
					Map<Object,Object> objsti1 = objsti;
					Long objsti11 = (Long) objsti1.get(xyz);
					objsti11 = objsti11 == null ? (long) 0 : objsti11;
					Long stillbrt;
					try
					{
						stillbrt=  objsti11*100/delivery; 
					}
					catch (Exception e) {
					stillbrt = (long) 0;
					}
			/*		Map<Object,Object> objbab1 = objbab;
					Long objbab11 =(Long) objbab1.get(xyz);
					objbab11 = objbab11 == null ? (long) 0 : objbab11;
					Long babyyy;
					try {
						babyyy=objbab11*100/lvebr;
					}
					catch (Exception e) {
						babyyy = (long) 0;
					}*/
					   xcv = xcv.put(delivery);
					   facil = facil.put(facily);
					   livebirth=livebirth.put(lvebr);
					   motherbparr = motherbparr.put(bpofmother);
					   astmlarr = astmlarr.put(0);
					   mothertemparr = mothertemparr.put(0);
					   partographarr = partographarr.put(0);
					   Eclampsiaarr = Eclampsiaarr.put(eclamp);
					   ppharr = ppharr.put(pphh);
					   prolongarr = prolongarr.put(0);
					   caesareanarr = caesareanarr.put(0);
					   neontalarr = neontalarr.put(0);
					   stillarr = stillarr.put(stillbrt);
				   babyarr = babyarr.put(0);
					
				 }
				 
				 
				 
				return method = method(xcv,xcu,facil,livebirth,motherbparr,astmlarr,mothertemparr,partographarr,Eclampsiaarr,ppharr,prolongarr,caesareanarr,neontalarr,stillarr,babyarr); 
				
					
			}
		   else if (state.length() > 0 && !district.equalsIgnoreCase("ALL")) {
			
			   List<Object[]> totalde = stateDashBoardDeliveryView.objByDistrict(state,district,date);
				 Map<Object,Object> objde = new HashMap<>();
				 for (int i = 0; i < totalde.size(); i++) {
					  Object[] arr = totalde.get(i);
					  Long l = new Long( (long) arr[0]);
						String district1 = arr[1].toString();
						objde.put(district1, l);
						 
					  }
				
				 
				 List<Object[]>  livebirths1 = stateDashBoardDeliveryView.objByDistrict0(state,district,date);
				 Map<Object,Object> objbir = new HashMap<>();
				 for (int i = 0; i < livebirths1.size(); i++) {
					  Object[] arr = livebirths1.get(i);
						Long l = (long)arr[0];
						
						String district1 = arr[1].toString();
						objbir.put(district1, l);
						  
					  }
				 List<Object[]>	 motherbp1=recordingMotherBPRepository.objDistrict(state,district,date);
				 Map<Object,Object> objbp = new HashMap<>();
				 for (int i = 0; i < motherbp1.size(); i++) {
					  Object[] arr = motherbp1.get(i);
						Long l = (long)arr[0];
						
						String district1 = arr[1].toString();
						objbp.put(district1, l);
						
					  }
				 List<Object[]>	ad =recordingMotherBPRepository.objByDistrict1(state,district,date);
				 Map<Object,Object> obj19 = new HashMap<>();
				 for (int i = 0; i < ad.size(); i++) {
					  Object[] arr = ad.get(i);
						Long l = (long)arr[0];
						String district1 = arr[1].toString();
						obj19.put(district1, l);
						
					  }
				 
				
	/*			 List<Object[]>	amts =stateDashBoardDeliveryView.objByDistrict1(state,district,date);
					Map<Object,Object> objamt = new HashMap<>();
				 for (int i = 0; i < amts.size(); i++) {
					  Object[] arr = amts.get(i);
						Long l = (long)arr[0];
						String district1 = arr[2].toString();
						objamt.put(district1, l);
						  
					  }
				 */
				 
	/*			 List<Object[]>	moth =motherTemperatureatDischargeViewRepository.objByDistrict(state,district,date);
				 Map<Object,Object> objmote = new HashMap<>();
				 for (int i = 0; i < moth.size(); i++) {
					  Object[] arr = moth.get(i);
						Long l = (long)arr[0];
						String district1 = arr[1].toString();
						objmote.put(district1, l);
						 
					  }*/
				 
			/*	 List<Object[]>	 par = partographMonitoringViewRepository.objByDistrict1(state,district,date);
				 Map<Object,Object> objpar = new HashMap<>();
				 for (int i = 0; i < par.size(); i++) {
					  Object[] arr = par.get(i);
						Long l = (long)arr[0];
						
						String district1 = arr[1].toString();
						objpar.put(district1, l);
						 
					  }*/
				 
				 List<Object[]> ecl  = l1ComplicationsRepository.objByDistrict5(state,district, date);
				 Map<Object,Object> objecl = new HashMap<>();
				 for (int i = 0; i < ecl.size(); i++) {
					  Object[] arr = ecl.get(i);
						Long l = (long)arr[0];
						
						String district1 = arr[4].toString();
						objecl.put(district1, l);
						  
					  }
			
				 List<Object[]> pp = l1ComplicationsRepository.objByDistrict6(state,district, date);
				 Map<Object,Object> objpp = new HashMap<>();
				 for (int i = 0; i < pp.size(); i++) {
					  Object[] arr = pp.get(i);
						Long l = (long)arr[0];
						
						String district1 = arr[4].toString();
						objpp.put(district1, l);
						 
					  }
				 
				
				 
				 
			/*	 
				 List<Object[]> pro =  stateDashBoardDeliveryView.objByDistrict8(state,district, date);
				 Map<Object,Object> objpro = new HashMap<>();
				 for (int i = 0; i < pro.size(); i++) {
					  Object[] arr = pro.get(i);
						Long l = (long)arr[0];
						
						String district1 = arr[4].toString();
						objpro.put(district1, l);
					  }
				 
				 
				 List<Object[]> caes =  stateDashBoardDeliveryView.objByDistrict9(state,district, date);
				 Map<Object,Object> objcaes = new HashMap<>();
				 for (int i = 0; i < caes.size(); i++) {
					  Object[] arr = caes.get(i);
						Long l = (long)arr[0];
						
						String district1 = arr[4].toString();
						objcaes.put(district1, l);
						 
					  }
				 
				List<Object[]> neon=neonatalAsphyxiaViewRepository.objDistrict(state,district, date);
				Map<Object,Object> objneon = new HashMap<>();
				 for (int i = 0; i < neon.size(); i++) {
					  Object[] arr = neon.get(i);
						Long l = (long)arr[0];
						String district1 = arr[4].toString();
						objneon.put(district1, l);
						 
					  }
				 */
				 List<Object[]> sti = stateDashBoardDeliveryView.objByDistrict3(state,district, date);
				 Map<Object,Object> objsti = new HashMap<>();
				 for (int i = 0; i < sti.size(); i++) {
					  Object[] arr = sti.get(i);
						Long l = (long)arr[0];
						
						
						String district1 = arr[4].toString();
						objsti.put(district1, l);
						
					  }
			/*	 List<Object[]> babyy = motherTemperatureatDischargeViewRepository.SDByDistrict2(state,district, date);
				 Map<Object,Object> objbab = new HashMap<>();
				 for (int i = 0; i < babyy.size(); i++) {
					  Object[] arr = babyy.get(i);
						Long l = (long)arr[0];
						String district1 = arr[4].toString();
						objbab.put(district1, l);
						 
					  }
			*/	 
				 List<Object[]>	alldis1t =recordingMotherBPRepository.objDistrict1(state,district,date);
					
				 for (int i = 0; i < alldis1t.size(); i++) {
					  Object[] arr = alldis1t.get(i);
						String district1 = arr[0].toString();
						String fac = arr[1].toString();
						 xcu.put(district1);
						 facil.put(fac);
					  }
				 
				 
				 
				 // calculation
				 int x = facil.length();
				 for(int i =0 ; i < x;i++)
				 {
					String xyz = facil.get(i).toString(); 
					Map<Object,Object> del = objde;
					Long delivery = (Long) del.get(xyz);
					delivery = delivery == null ? (long) 0 :delivery;
					Map<Object,Object> liv = objbir;
					Long lvebr = (Long) liv.get(xyz);
					lvebr = lvebr == null ? (long) 0 : lvebr;
					Map<Object,Object> bp = objbp;
					Long bpm = (Long) bp.get(xyz);
					bpm = bpm == null ? (long) 0 : bpm*100; 
					Map<Object,Object> admm = obj19;
					Long admis = (Long) admm.get(xyz);
					admis = admis == null ? (long) 0 : admis;
					Long bpofmother;
					try {
					bpofmother = bpm == 0 ? 0 : bpm/admis;
					}
					catch(Exception e)
					{
						bpofmother = (long) 0;
					}
				/*	Map<Object,Object> objamt1 = objamt;
					Long amt = (Long) objamt1.get(xyz);
					amt = amt == null ? (long) 0 : amt;
					Long amtll ;
					try {
						amtll = amt*100/delivery;
					}
					catch (Exception e) {
						amtll = (long) 0;
					}*/
			/*		Map<Object,Object> objmote1 = objmote;
					Long objmote11 = (Long) objmote1.get(xyz);
					objmote11 = objmote11 == null ? (long) 0 : objmote11;
					Long momtemp;
					try {
						momtemp = objmote11*100/admis;
					}
					catch(Exception e)
					{
						momtemp = (long) 0;
					}*/
				/*	Map<Object,Object> objpar1 = objpar;
					Long objpar11 =(Long) objpar1.get(xyz);
					objpar11 = objpar11 == null ? (long) 0 : objpar11;
					Long parato;
					try {
						parato = objpar11*100/admis; 
					}
					catch (Exception e) {
					parato = (long) 0;
					}*/
				Map<Object, Object> objecl1 = objecl;
					Long objecl11 =(Long) objecl1.get(xyz);
					objecl11 = objecl11 == null ? (long) 0 : objecl11;
					Long eclamp;
					try {
						eclamp = objecl11*100/delivery;
					}
					catch(Exception e)
					{
						eclamp = (long) 0;
					}
					Map<Object,Object> objpp1 = objpp;
					Long objpp11 =(Long) objpp1.get(xyz);
					objpp11 = objpp11 == null ? (long) 0 : objpp11;
					Long pphh;
					try {
						pphh = objpp11*100/delivery;
					}
					catch (Exception e) {
						pphh = (long) 0;
					}
				/*	Map<Object,Object> objpro1 = objpro;
					Long objpro11 = (Long) objpro1.get(xyz);
					objpro11 = objpro11 == null ? (long) 0 : objpro11;
					Long prolon;
					try {
						prolon = objpro11*100/delivery;
					}
					catch(Exception e)
					{
						prolon = (long) 0;
					}
					Map<Object,Object>  objcaes1 = objcaes;
					Long objcaes11 = (Long) objcaes1.get(xyz);
					objcaes11 = objcaes11 == null ? (long) 0 : objcaes11;
					Long caesern;
					try
					{
						caesern = objcaes11*100/delivery;
					}
					catch (Exception e) {
						caesern = (long) 0; 
					}
					Map<Object,Object> objneon1 = objneon;
					Long objneon11 = (Long) objneon1.get(xyz);
					objneon11 = objneon11 == null ? (long) 0 : objneon11;
					Long neona;
					try {
						neona = objneon11*100/lvebr;
					}
					catch (Exception e) {
						neona = (long) 0;
					}
				*/	Map<Object,Object> objsti1 = objsti;
					Long objsti11 = (Long) objsti1.get(xyz);
					objsti11 = objsti11 == null ? (long) 0 : objsti11;
					Long stillbrt;
					try
					{
						stillbrt=  objsti11*100/delivery; 
					}
					catch (Exception e) {
					stillbrt = (long) 0;
					}
				/*	Map<Object,Object> objbab1 = objbab;
					Long objbab11 =(Long) objbab1.get(xyz);
					objbab11 = objbab11 == null ? (long) 0 : objbab11;
					Long babyyy;
					try {
						babyyy=objbab11*100/lvebr;
					}
					catch (Exception e) {
						babyyy = (long) 0;
					}
				*/	   xcv = xcv.put(delivery);
					   livebirth=livebirth.put(lvebr);
					   motherbparr = motherbparr.put(bpofmother);
				//	   astmlarr = astmlarr.put(amtll);
					//   mothertemparr = mothertemparr.put(momtemp);
				//	   partographarr = partographarr.put(parato);
					   Eclampsiaarr = Eclampsiaarr.put(eclamp);
					   ppharr = ppharr.put(pphh);
					//   prolongarr = prolongarr.put(prolon);
					//   caesareanarr = caesareanarr.put(caesern);
					//   neontalarr = neontalarr.put(neona);
					   stillarr = stillarr.put(stillbrt);
					//   babyarr = babyarr.put(babyyy);
					
				 }
				 
				 
				 
				return method = method(xcv,xcu,facil,livebirth,motherbparr,astmlarr,mothertemparr,partographarr,Eclampsiaarr,ppharr,prolongarr,caesareanarr,neontalarr,stillarr,babyarr); 
			   
			   
		   }
		   return method;
		  
	   }
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	private JSONArray method( JSONArray xcv,JSONArray xcu,JSONArray facil,JSONArray livebirth,JSONArray motherbparr,
			JSONArray astmlarr,JSONArray mothertemparr,JSONArray partographarr,JSONArray Eclampsiaarr,
			JSONArray ppharr,JSONArray prolongarr,JSONArray caesareanarr,JSONArray neontalarr,JSONArray stillarr,JSONArray babyarr) {
		JSONArray ar = new JSONArray();
		for(int x =0 ; x < xcu.length();x++)
		{
			JSONObject obj = new JSONObject();
			obj.put("preeclampsia_eclampsia", Eclampsiaarr.get(x));
			obj.put("mother_bp_admission", motherbparr.get(x));
			obj.put("district", xcu.get(x));
			obj.put("facility", facil.get(x));
			obj.put("partograph", "0");
			obj.put("total_live_births", livebirth.get(x));
			obj.put("mother_temperature_discharge", "0");
			obj.put("total_deliveries", xcv.get(x));
			obj.put("preterm_births", "0");
			obj.put("prolonged_labor", "0");
			obj.put("amtsl", "0");
			obj.put("live_birth_neo_natal_asphyxia", "0");
			obj.put("Babys_temperature_recorded_at_discharge", "0");
			obj.put("still_birth", stillarr.get(x));
			obj.put("pph", "0");
			obj.put("caesarean_section", "0");
			ar.put(obj);
		}
		return ar;
	}
	   }


