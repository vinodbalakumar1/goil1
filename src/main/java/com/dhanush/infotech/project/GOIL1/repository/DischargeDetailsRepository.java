package com.dhanush.infotech.project.GOIL1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dhanush.infotech.project.GOIL1.model.DischargeDetails;

/**
 * Created by vinod on 12/06/18.
 */

@Repository
public interface DischargeDetailsRepository extends JpaRepository<DischargeDetails, Long> {

	@Query(value = "SELECT a FROM DischargeDetails a WHERE admission_id = :admissionId")
	List<DischargeDetails> findByCaseId(@Param("admissionId") Long admissionId);
}
