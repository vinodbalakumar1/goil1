package com.dhanush.infotech.project.GOIL1.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL1.model.L1MPReportModel;
import com.dhanush.infotech.project.GOIL1.repository.AdmissionRepository;
import com.dhanush.infotech.project.GOIL1.repository.L1MPReportRepository;
import com.dhanush.infotech.project.GOIL1.repository.L1MpreportBabyNotesRepository;
import com.dhanush.infotech.project.GOIL1.repository.MPReportViewRepository;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class L1MpreportController
{
	
	@Autowired
	L1MPReportRepository l1mpreportRepository;
	
	@Autowired
	MPReportViewRepository mpreportviewRepository;
	
	@Autowired
	L1MpreportBabyNotesRepository babynotesRepository;
	
	@Autowired
	AdmissionRepository admissionRepository;
	
	
	@CrossOrigin
	@RequestMapping(value = "l1insertdeliveryreport", method = RequestMethod.POST, produces = {
			"application/json" }, consumes = "application/json")

	public HashMap<String, String> reports(@RequestBody String data) throws Exception {
	
		org.json.JSONObject map = new org.json.JSONObject(data);
		String state = map.get("state").toString();
		String district = map.get("district").toString();
		String block = map.get("block").toString();
		String facility_type_id = map.get("facility_type_id").toString();
		String facility = map.get("facility").toString();
		String month = map.get("month").toString();
		String year = map.get("year").toString();
		String normalDelivery = map.get("normal_deliveries").toString();
		String assistedDelivery = map.get("assisted_deliveries").toString();
		String caesareanDelivery = map.get("caesarean_deliveries").toString();
		String liveBirths = map.get("live_birth").toString();
		String stillBirths = map.get("still_birth").toString();
		String maternalDeaths = map.get("maternal_deaths").toString();

		L1MPReportModel mpreport = new L1MPReportModel();
		mpreport.setNormal_deliveries(Integer.parseInt(normalDelivery));
		mpreport.setAssisted_deliveries(Integer.parseInt(assistedDelivery));
		mpreport.setCaesarean_deliveries(Integer.parseInt(caesareanDelivery));
		mpreport.setLive_births(Integer.parseInt(liveBirths));
		mpreport.setMaternal_deaths(Integer.parseInt(maternalDeaths));
		mpreport.setStillbirths(Integer.parseInt(stillBirths));
		mpreport.setState(Integer.parseInt(state));
		mpreport.setDistrict(Integer.parseInt(district));
		mpreport.setBlock(Integer.parseInt(block));
		mpreport.setFacilitytype(Integer.parseInt(facility_type_id));
		mpreport.setFacility(Integer.parseInt(facility));
		mpreport.setYear(Integer.parseInt(year));
		mpreport.setMonth(Integer.parseInt(month));
		l1mpreportRepository.save(mpreport);
		HashMap<String, String> reports1 = new HashMap<>();
		reports1.put("id", String.valueOf(mpreport.getId()));
		reports1.put("message", "successfully inserted");
		return reports1;

	}
	
	@CrossOrigin
	@RequestMapping(value = "l1selectdeliveryreport", method = RequestMethod.POST, produces = {
			"application/json" }, consumes = "application/json")
	private String actualdeliveryreport_(@RequestBody String userdetail) throws Exception {
		org.json.JSONObject map = new org.json.JSONObject(userdetail);
		String state = map.get("state").toString();
		String district = map.get("district").toString();
		String block = map.get("block").toString();
		String facility_type_id = map.get("facility_type_id").toString();
		String facility = map.get("facility").toString();
		String month = map.get("month").toString();
		String year = map.get("year").toString();
		JSONArray arr = new JSONArray();
		List<L1MPReportModel>	listUsers = l1mpreportRepository.findByFacility(Integer.parseInt(state), Integer.parseInt(district),Integer.parseInt(block),Integer.parseInt(facility_type_id),Integer.parseInt(facility),Integer.parseInt(year),Integer.parseInt(month));
		Iterator<L1MPReportModel> itr = listUsers.iterator();
		while (itr.hasNext()) {
			JSONObject obj = new JSONObject();
			L1MPReportModel user = itr.next();
			obj.put("normal_deliveries", ""+user.getNormal_deliveries());
			obj.put("assisted_deliveries",""+ user.getAssisted_deliveries());
			obj.put("caesarean_deliveries", ""+user.getCaesarean_deliveries());
			obj.put("live_births", ""+user.getLive_births());
			obj.put("still_births", ""+user.getStillbirths());
			obj.put("maternal_deaths", ""+user.getMaternal_deaths());
			arr.put(obj);
		}
		if(arr.length() > 0) {
			
			return arr.get(arr.length()-1).toString();
			
		}
		else if(arr.length() == 0)
		{
			JSONObject obj1 = new JSONObject();
			arr.put(obj1);
			return arr.get(0).toString();
		}
		else
		{
		return arr.get(0).toString();
	}
	}
	
	@CrossOrigin
	@RequestMapping(value = "l1mpreport", method = RequestMethod.POST, produces = { "application/json" })
	public @ResponseBody Map<String, Object> mpReport(@RequestBody String userJsonReq) throws NumberFormatException, Exception {
		try {
		org.json.JSONObject obj = new org.json.JSONObject(userJsonReq);
		String  block = obj.getString("block_code").toString().equalsIgnoreCase("ALL")?"":obj.getString("block_code").toString();
		String district = obj.getString("district_code").toString().equalsIgnoreCase("ALL")?"":obj.getString("district_code").toString();
		String state = obj.getString("state_code").toString();
		String facility = obj.getString("facility_code").toString().equalsIgnoreCase("ALL")?"":obj.getString("facility_code").toString();
		String facilitytype = obj.getString("facility_type_id").toString().equalsIgnoreCase("ALL") ?"":obj.getString("facility_type_id").toString();
		String year =obj.getString("input_year").toString().equalsIgnoreCase("ALL") ? "" :obj.getString("input_year").toString()+"-";
		//String month =obj.getString("input_month").toString().equalsIgnoreCase("ALL") ? "":"0"+obj.getString("input_month").toString()+"-";
		String month =obj.getString("input_month").toString().equalsIgnoreCase("ALL") ? "":obj.getString("input_month").length()==1?"0"+obj.getString("input_month")+"-":obj.getString("input_month")+"-";

		String start = year+""+month;
		System.out.println("date="+start);
		System.out.println("year & month="+year+","+month);

		String end="";
		
		 Map<String,Object> map = new HashMap<>();
		 
		// map.put("Normal_Deliveries", normalDeliveries(state,district,block,facilitytype,facility,start,end));
		// map.put("Assisted_Deliveries", assistedDeliveries(state,district,block,facilitytype,facility,start,end));
		//map.put("Caesarean_Deliveries", caesareanDeliveries(state,district,block,facilitytype,facility,start,end));
		 map.put("Normal_Deliveries", "NA");
		 map.put("Assisted_Deliveries", "NA");
		 map.put("Caesarean_Deliveries", "NA");
			 
		map.put("Live_Births", liveBirths(state,district,block,facilitytype,facility,start,end));
		map.put("still_births", stillBirths(state,district,block,facilitytype,facility,start,end));
		map.put("maternal_deaths",  maternalDeaths(state,district,block,facilitytype,facility,start,end));
		map.put("obstructed_labor",obstructedLabor(state,district,block,facilitytype,facility,start,end));
		map.put("Eclampsia",Eclampsia(state,district,block,facilitytype,facility,start,end));
		map.put("Newborn_with_asphyxia",newbornwithAsphyxia(state,district,block,facilitytype,facility,start,end));
		map.put("Pre_term_births",preTermBirths(state,district,block,facilitytype,facility,start,end));
		
		map.put("Sepsis",sepsis(state,district,block,facilitytype,facility,start));//not implemented in initial masters
		 map.put("post_partum_hemorrhage", pph(state,district,block,facilitytype,facility,start,end));
		// map.put("Partographs",Partographs(state,district,block,facilitytype,facility,start,end));
		 map.put("Partographs","NA");
		 map.put("mother_blood_pressure", motherBp(state,district,block,facilitytype,facility,start,end));
		  map.put("mother_temperature_at_Discharge","NA");
		  map.put("Number of deliveries where oxytocin was given as uterotonic","NA");
		 
		// map.put("mother_temperature_at_Discharge",motherTemperatureAtDischarge(state,district,block,facilitytype,facility,start,end));
		//  map.put("Number of deliveries where oxytocin was given as uterotonic",uterotonic(state,district,block,facilitytype,facility,start,end));
		  map.put("Number of livebirths where baby was breast feed within 1 hour of delivery",breastFeed(state,district,block,facilitytype,facility,start,end));
		 map.put("Number of deliveries where mother temperature was recorded at admission",tempAtAdmission(state,district,block,facilitytype,facility,start,end));
		map.put("Number of deliveries where Baby's birth weight was recorded after birth",babyWeight(state,district,block,facilitytype,facility,start,end));
		// map.put("Number of livebirths where Baby's Temperature was recorded at discharge",babyTempAtDischarge(state,district,block,facilitytype,facility,start,end));
		 map.put("Number of livebirths where Baby's Temperature was recorded at discharge","NA");

		map.put("No of mothers who adopted PPIUCD",ppiucd(state,district,block,facilitytype,facility,start,end));
		 map.put("Number of preterm births (<34 weeks) where antenatal corticosteroids were administered","NA");//removed in goi
		// map.put("Number of deliveries where Safe child birth checklist was used",safeChildList(state,district,block,facilitytype,facility,start,end));
			map.put("Number of deliveries where Safe child birth checklist was used","NA");

		 ArrayList<Map<String,Object>> arr1 = new ArrayList<Map<String,Object>>();
		arr1.add(actualdeliveryreport1_(userJsonReq));
		  JSONObject jobj = new JSONObject();
		  JSONArray ar=new JSONArray();
		 
			
		 JSONObject jobj1 = new JSONObject();	
		 JSONObject jobj2 = new JSONObject();		 
		 JSONObject jobj3 = new JSONObject();
		 JSONObject jobj4 = new JSONObject();
		 JSONObject jobj5 = new JSONObject();
		 JSONObject jobj6 = new JSONObject();
		 JSONObject jobj8 = new JSONObject();
		 JSONObject jobj9 = new JSONObject();
		 JSONObject jobj10 = new JSONObject();
		 JSONObject jobj11 = new JSONObject();
		 JSONObject jobj12 = new JSONObject();
		 JSONObject jobj13 = new JSONObject();
		 JSONObject jobj14 = new JSONObject();
		 JSONObject jobj15 = new JSONObject();
		 JSONObject jobj16 = new JSONObject();
		 JSONObject jobj17 = new JSONObject();
		 JSONObject jobj18 = new JSONObject();
		 JSONObject jobj19 = new JSONObject();
		 JSONObject jobj20 = new JSONObject();
		 JSONObject jobj21 = new JSONObject();
		 JSONObject jobj22 = new JSONObject();
		 JSONObject jobj23 = new JSONObject();
		 JSONObject jobj24 = new JSONObject();
		 JSONObject jobj25 = new JSONObject();
		 JSONObject jobjob = new JSONObject();




		 jobj.put("question_name", "<strong>Obstetric Services</strong>");
		 jobj.put("numbers_in_the_reporting_month_right", "<strong>Actual Reported</strong>");
		 jobj.put("group_name", "<strong>Obstetric Services</strong>");
		 jobj.put("question_sno", "A");
		 jobj.put("group_sno", "A");
		 jobj.put("numbers_in_the_reporting_month_left", "<strong>Case Sheet compilation</strong>");
		 
		 jobj1.put("question_name", "Total number of Normal Deliveries in the facility");
		 jobj1.put("numbers_in_the_reporting_month_right",  arr1.get(0).get("normal_deliveries"));
		 //jobj1.put("numbers_in_the_reporting_month_right","0");
		 jobj1.put("group_name", "Obstetric Services");
		 jobj1.put("question_sno", "A1");
		 jobj1.put("group_sno", "A1");
		 jobj1.put("numbers_in_the_reporting_month_left",map.get("Normal_Deliveries"));

		 jobj2.put("question_name", "Total number of assisted deliveries in the facility (Vacuum/ forceps)");
		 jobj2.put("numbers_in_the_reporting_month_right",  arr1.get(0).get("assisted_deliveries"));
		// jobj2.put("numbers_in_the_reporting_month_right", "0");

		 jobj2.put("group_name", "Obstetric Services");
		 jobj2.put("question_sno", "A2");
		 jobj2.put("group_sno", "A2");
		 jobj2.put("numbers_in_the_reporting_month_left",map.get("Assisted_Deliveries"));
		 
		 
		 jobj3.put("question_name", "Total number of caesarean deliveries in the facility");
		 jobj3.put("numbers_in_the_reporting_month_right",  arr1.get(0).get("caesarean_deliveries"));
		// jobj3.put("numbers_in_the_reporting_month_right", "0");

		 jobj3.put("group_name", "Obstetric Services");
		 jobj3.put("question_sno", "A3");
		 jobj3.put("group_sno", "A3");
		 jobj3.put("numbers_in_the_reporting_month_left",map.get("Caesarean_Deliveries"));
		 
		 
		 jobj4.put("question_name", "Number of live births in the facility");
		 jobj4.put("numbers_in_the_reporting_month_right", arr1.get(0).get("live_births"));
		// jobj4.put("numbers_in_the_reporting_month_right", "0");
		 jobj4.put("group_name", "Obstetric Services");
		 jobj4.put("question_sno", "A4");
		 jobj4.put("group_sno", "A4");
		 jobj4.put("numbers_in_the_reporting_month_left",map.get("Live_Births"));
		 

		 jobj5.put("question_name", "Number of still births in the facility");
		 jobj5.put("numbers_in_the_reporting_month_right", arr1.get(0).get("still_births") );
		// jobj5.put("numbers_in_the_reporting_month_right","0");
		 jobj5.put("group_name", "Obstetric Services");
		 jobj5.put("question_sno", "A5");
		 jobj5.put("group_sno", "A5");
		 jobj5.put("numbers_in_the_reporting_month_left",map.get("still_births"));
		 
		 jobj6.put("question_name", "Number of maternal deaths in facility in the month");
		jobj6.put("numbers_in_the_reporting_month_right", arr1.get(0).get("maternal_deaths"));
		// jobj6.put("numbers_in_the_reporting_month_right", "0");
		 jobj6.put("group_name", "Obstetric Services");
		 jobj6.put("question_sno", "A6");
		 jobj6.put("group_sno", "A6");
		 jobj6.put("numbers_in_the_reporting_month_left",map.get("maternal_deaths"));
		 
		 
		 jobj8.put("question_name", "<strong>Complicated deliveries</strong>");
		 jobj8.put("numbers_in_the_reporting_month_right", "<strong>Referred to higher center</strong>");
		 jobj8.put("group_name", "<strong>Complicated deliveries</strong>");
		 jobj8.put("question_sno", "B");
		 jobj8.put("group_sno", "B");
		 jobj8.put("numbers_in_the_reporting_month_left","<strong>Managed at facility</strong>");
		 
		 
		 jobjob.put("question_name", "Mothers with post-partum hemorrhage");
		 jobjob.put("numbers_in_the_reporting_month_right", "0");
		 jobjob.put("group_name", "<strong>Complicated deliveries</strong>");
		 jobjob.put("question_sno", "B1");
		 jobjob.put("group_sno", "B1");
		 jobjob.put("numbers_in_the_reporting_month_left",map.get("post_partum_hemorrhage"));
		 
		 
		 jobj9.put("question_name", "Mothers with Sepsis");
		 jobj9.put("numbers_in_the_reporting_month_right", "0");
		 jobj9.put("group_name", "<strong>Complicated deliveries</strong>");
		 jobj9.put("question_sno", "B2");
		 jobj9.put("group_sno", "B2");
		 jobj9.put("numbers_in_the_reporting_month_left",map.get("Sepsis"));
		 
		 jobj10.put("question_name", "Mothers with Severe Pre-eclampsia/ Eclampsia");
		 jobj10.put("numbers_in_the_reporting_month_right", "0");
		 jobj10.put("group_name", "<strong>Complicated deliveries</strong>");
		 jobj10.put("question_sno", "B3");
		 jobj10.put("group_sno", "B3");
		 jobj10.put("numbers_in_the_reporting_month_left",map.get("Eclampsia"));
		 
		 jobj11.put("question_name", "Mothers with Obstructed labor");
		 jobj11.put("numbers_in_the_reporting_month_right", "0");
		 jobj11.put("group_name", "<strong>Complicated deliveries</strong>");
		 jobj11.put("question_sno", "B4");
		 jobj11.put("group_sno", "B4");
		 jobj11.put("numbers_in_the_reporting_month_left",map.get("obstructed_labor"));
		 
		 
		 jobj12.put("question_name", "Newborn with asphyxia");
		 jobj12.put("numbers_in_the_reporting_month_right", "0");
		 jobj12.put("group_name", "<strong>Complicated deliveries</strong>");
		 jobj12.put("question_sno", "B5");
		 jobj12.put("group_sno", "B5");
		 jobj12.put("numbers_in_the_reporting_month_left",map.get("Newborn_with_asphyxia"));
		 
		 jobj13.put("question_name", "Number of pre term births (<34 weeks)");
		 jobj13.put("numbers_in_the_reporting_month_right", "0");
		 jobj13.put("group_name", "<strong>Complicated deliveries</strong>");
		 jobj13.put("question_sno", "B6");
		 jobj13.put("group_sno", "B6");
		 jobj13.put("numbers_in_the_reporting_month_left",map.get("Pre_term_births"));
		 
		 
		 jobj14.put("question_name", "<strong>Practices</strong>");
		 jobj14.put("group_sno", "C");
		 jobj14.put("numbers_in_the_reporting_month_left", "0");
		 jobj14.put("question_sno", "C");
		 jobj14.put("group_name", "<strong>Practices</strong>");
		 
		 
		 
		 jobj15.put("question_name", "deliveries where partographs were used for monitoring of labor");
		 jobj15.put("group_sno", "C1");
		 jobj15.put("numbers_in_the_reporting_month_left",map.get("Partographs"));
		 jobj15.put("question_sno", "C1");
		 jobj15.put("group_name", "deliveries where partographs were used for monitoring of labor");
 
		 jobj16.put("question_name", "Number of deliveries where mother's Blood pressure was recorded at admission");
		 jobj16.put("group_sno", "C2");
		 jobj16.put("numbers_in_the_reporting_month_left",map.get("mother_blood_pressure"));
		 jobj16.put("question_sno", "C2");
		 jobj16.put("group_name", "Number of deliveries where mother's Blood pressure was recorded at admission");
 
		 jobj17.put("question_name", "Number of deliveries where mother's temperature was recorded at admission");
		 jobj17.put("group_sno", "C3");
		 jobj17.put("numbers_in_the_reporting_month_left",map.get("Number of deliveries where mother temperature was recorded at admission"));
		 jobj17.put("question_sno", "C3");
		 jobj17.put("group_name", "Number of deliveries where mother's temperature was recorded at admission");
 
		 jobj18.put("question_name", "Number of deliveries where oxytocin was given as uterotonic for Active management of third stage of labor immediately after birth");
		 jobj18.put("group_sno", "C4");
		 jobj18.put("numbers_in_the_reporting_month_left",map.get("Number of deliveries where oxytocin was given as uterotonic"));
		 jobj18.put("question_sno", "C4");
		 jobj18.put("group_name", "Number of deliveries where oxytocin was given as uterotonic for Active management of third stage of labor immediately after birth");
 
		 
		 jobj19.put("question_name", "Number of livebirths where baby was breast feed within 1 hour of delivery");
		 jobj19.put("group_sno", "C5");
		 jobj19.put("numbers_in_the_reporting_month_left",map.get("Number of livebirths where baby was breast feed within 1 hour of delivery"));
		 jobj19.put("question_sno", "C5");
		 jobj19.put("group_name", "Number of livebirths where baby was breast feed within 1 hour of delivery");
 
		 jobj20.put("question_name", "Number of deliveries where mothers temperature was recorded at discharge");
		 jobj20.put("group_sno", "C6");
		 jobj20.put("numbers_in_the_reporting_month_left",map.get("mother_temperature_at_Discharge"));
		 jobj20.put("question_sno", "C6");
		 jobj20.put("group_name", "Number of deliveries where mothers temperature was recorded at discharge");
		 
		 
		 jobj21.put("question_name", "Number of deliveries where Baby's birth weight was recorded after birth");
		 jobj21.put("group_sno", "C7");
		 jobj21.put("numbers_in_the_reporting_month_left",map.get("Number of deliveries where Baby's birth weight was recorded after birth"));
		 jobj21.put("question_sno", "C7");
		 jobj21.put("group_name", "Number of deliveries where Baby's birth weight was recorded after birth");
 

		 jobj22.put("question_name", "Number of deliveries where Safe child birth checklist was used");
		 jobj22.put("group_sno", "C8");
		 jobj22.put("numbers_in_the_reporting_month_left",map.get("Number of deliveries where Safe child birth checklist was used"));
		 jobj22.put("question_sno", "C8");
		 jobj22.put("group_name", "Number of deliveries where Safe child birth checklist was used");
		 
		 jobj23.put("question_name", "Number of livebirths where Baby's Temperature was recorded at discharge");
		 jobj23.put("group_sno", "C9");
		 jobj23.put("numbers_in_the_reporting_month_left",map.get("Number of livebirths where Baby's Temperature was recorded at discharge"));
		 jobj23.put("question_sno", "C9");
		 jobj23.put("group_name", "Number of livebirths where Baby's Temperature was recorded at discharge");
		 
		 
		 jobj24.put("question_name", "Number of preterm births (<34 weeks) where antenatal corticosteroids were administered");
		 jobj24.put("group_sno", "C10");
		 jobj24.put("numbers_in_the_reporting_month_left",map.get("Number of preterm births (<34 weeks) where antenatal corticosteroids were administered"));
		 jobj24.put("question_sno", "C10");
		 jobj24.put("group_name", "Number of preterm births (<34 weeks) where antenatal corticosteroids were administered");
		 
		 jobj25.put("question_name", "No of mothers who adopted PPIUCD");
		 jobj25.put("group_sno", "C11");
		 jobj25.put("numbers_in_the_reporting_month_left",map.get("No of mothers who adopted PPIUCD"));
		 jobj25.put("question_sno", "C11");
		 jobj25.put("group_name", "No of mothers who adopted PPIUCD");
		 
		 
		 
		 ar.put(jobj);
		 ar.put(jobj1);
		 ar.put(jobj2);
		 ar.put(jobj3);
		 ar.put(jobj4);
		 ar.put(jobj5);
		 ar.put(jobj6);
		 ar.put(jobj8);
		 ar.put(jobjob);
		 ar.put(jobj9);
		 ar.put(jobj10);
		 ar.put(jobj11);
		 ar.put(jobj12);
		 ar.put(jobj13);
		 ar.put(jobj14);
		 ar.put(jobj15);
		 ar.put(jobj16);
		 ar.put(jobj17);
		 ar.put(jobj18);
		 ar.put(jobj19);
		 ar.put(jobj20);
		 ar.put(jobj21);
		 ar.put(jobj22);
		 ar.put(jobj23);
		 ar.put(jobj24);
		 ar.put(jobj25);

		 
		 JSONObject jobj26 = new JSONObject();
		 JSONObject jobj27 = new JSONObject();
	
		 Map<String,Object> map1 = new HashMap<>();
		 map1.put("result", ar);
		 map1.put("message", ar.length()+"records");
		   map1.put("total",ar.length());
		 
		 jobj26.put("data", map1);
		 jobj26.put("errors",jobj27);
		 return jobj26.toMap();

		}catch(Exception e)
		{
			e.printStackTrace();
			System.err.println(e);
		}
		return null;
		 
		
				
	}




	private Object ppiucd(String state, String district, String block, String facilitytype, String facility,
			String start, String end) {
		Long total = 0L;
		 
		   
		   if (state.length() > 0 && district.length() == 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0)
		   {
			   total=babynotesRepository.findByStatePpiucd(state,start);

			}
		   if (state.length() > 0 && district.length() > 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = babynotesRepository.findByDistrictPpiucd(state,district,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = babynotesRepository.findByBlockPpiucd(state,district,block,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() == 0) {
				 total = babynotesRepository.findByFacilitytypePpiucd(state,district,block,facilitytype,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() > 0) {
			  
				 total = babynotesRepository.findByFacilityCodePpiucd(state,district,block,facilitytype,facility,start);
		
		   }
		   if(total == null || total == 0)
			   total = 0L;
		   
		
		return total.toString();
	}

	private Object babyWeight(String state, String district, String block, String facilitytype, String facility,
			String start, String end) {
		Long total = 0L;
		
		   
		   if (state.length() > 0 && district.length() == 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0)
		   {
			   total=mpreportviewRepository.findStateBabyWeight(state,start);

			}
		   if (state.length() > 0 && district.length() > 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findDistrictBabyWeight(state,district,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findBlockBabyWeight(state,district,block,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findFacilitytypeBabyWeight(state,district,block,facilitytype,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() > 0) {
			  
				 total = mpreportviewRepository.findFacilityCodeBabyWeight(state,district,block,facilitytype,facility,start);
		
		   }
		   if(total == null || total == 0)
			   total = 0L;
		   
		
		return total.toString();
	}

	private Object tempAtAdmission(String state, String district, String block, String facilitytype, String facility,
			String start, String end)
	{
		Long total = 0L;
		
		   
		   if (state.length() > 0 && district.length() == 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0)
		   {
			   total=admissionRepository.findStateTempAdmission(state,start);

			}
		   if (state.length() > 0 && district.length() > 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = admissionRepository.findDistrictTempAdmission(state,district,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = admissionRepository.findBlockTempAdmission(state,district,block,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() == 0) {
				 total = admissionRepository.findFacilitytypeTempAdmission(state,district,block,facilitytype,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() > 0) {
			  
				 total = admissionRepository.findFacilityCodeTempAdmission(state,district,block,facilitytype,facility,start);
		
		   }
		   if(total == null || total == 0)
			   total = 0L;
		   
		
		return total.toString();
	}

	private Object breastFeed(String state, String district, String block, String facilitytype, String facility,
			String start, String end) {
		Long total = 0L;
		   
		   if (state.length() > 0 && district.length() == 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0)
		   {
			   total=mpreportviewRepository.findStateBreastFeed(state,start);

			}
		   if (state.length() > 0 && district.length() > 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findDistrictBreastFeed(state,district,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findBlockBreastFeed(state,district,block,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findFacilitytypeBreastFeed(state,district,block,facilitytype,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() > 0) {
			  
				 total = mpreportviewRepository.findFacilityCodeBreastFeed(state,district,block,facilitytype,facility,start);
		
		   }
		   if(total == null || total == 0)
			   total = 0L;
		   
		
		return total.toString();
	}

	private Object motherBp(String state, String district, String block, String facilitytype, String facility,
			String start, String end)
	{
		
		Long total = 0L;
		 
		   
		   if (state.length() > 0 && district.length() == 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0)
		   {
			   total=admissionRepository.findStateMotherBp(state,start);			   
			   
			}
	       if (state.length() > 0 && district.length() > 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = admissionRepository.findDistrictMotherBp(state,district,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = admissionRepository.findBlockMotherBp(state,district,block,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() == 0) {
				 total = admissionRepository.findFacilitytypeMotherBp(state,district,block,facilitytype,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() > 0) {
			  
				 total = admissionRepository.findFacilityCodeMotherBp(state,district,block,facilitytype,facility,start);
		
		   }
		   if(total == null || total == 0)
			   total = 0L;
		   
		
		return total.toString();
	}

	private Object pph(String state, String district, String block, String facilitytype, String facility, String start,
			String end) {
		Long total = 0L;
		
		   
		   if (state.length() > 0 && district.length() == 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0)
		   {
			   total=babynotesRepository.findStatePph(state,start);
			   
			   
			}
	        if (state.length() > 0 && district.length() > 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = babynotesRepository.findDistrictPph(state,district,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = babynotesRepository.findBlockPph(state,district,block,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() == 0) {
				 total = babynotesRepository.findFacilitytypePph(state,district,block,facilitytype,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() > 0) {
			  
				 total = babynotesRepository.findFacilityCodePph(state,district,block,facilitytype,facility,start);
		
		   }
		   if(total == null || total == 0)
			   total = 0L;
		   
		
		return total.toString();
	}

	private Object sepsis(String state, String district, String block, String facilitytype, String facility,
			String start) {
		Long total = 0L;
		 
		   
		   if (state.length() > 0 && district.length() == 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0)
		   {
			   total=babynotesRepository.findStateSepsis(state,start);

			}
		   if (state.length() > 0 && district.length() > 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = babynotesRepository.findDistrictSepsis(state,district,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = babynotesRepository.findBlockSepsis(state,district,block,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() == 0) {
				 total = babynotesRepository.findFacilitytypeSepsis(state,district,block,facilitytype,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() > 0) {
			  
				 total = babynotesRepository.findFacilityCodeSepsis(state,district,block,facilitytype,facility,start);
		
		   }
		   if(total == null || total == 0)
			   total = 0L;
		   
		
		return total.toString();
		
		
		
	}

	private Object Eclampsia(String state, String district, String block, String facilitytype, String facility,
			String start, String end) 
	{
		
		Long total = 0L;
		   
		   if (state.length() > 0 && district.length() == 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0)
		   {
			   total=babynotesRepository.findByStateEclampsia(state,start);
			   
			   
			}
	      if (state.length() > 0 && district.length() > 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = babynotesRepository.findByDistrictEclampsia(state,district,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = babynotesRepository.findByBlockEclampsia(state,district,block,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() == 0) {
				 total = babynotesRepository.findByFacilitytypeEclampsia(state,district,block,facilitytype,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() > 0) {
			  
				 total = babynotesRepository.findByFacilityCodeEclampsia(state,district,block,facilitytype,facility,start);
		
		   }
		   if(total == null || total == 0)
			   total = 0L;
		   
		
		return total.toString();
	}

	private Object newbornwithAsphyxia(String state, String district, String block, String facilitytype,
			String facility, String start, String end) {
		Long total = 0L;
		
		   
		   if (state.length() > 0 && district.length() == 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0)
		   {
			   total=mpreportviewRepository.findStateAsphyxia(state,start);
			   
			   
			}
	       if (state.length() > 0 && district.length() > 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findDistrictAsphyxia(state,district,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findBlockAsphyxia(state,district,block,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findFacilitytypeAsphyxia(state,district,block,facilitytype,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() > 0) {
			  
				 total = mpreportviewRepository.findFacilityAsphyxia(state,district,block,facilitytype,facility,start);
		
		   }
		   if(total == null || total == 0)
			   total = 0L;
		   
		
		return total.toString();
	}

	private Object preTermBirths(String state, String district, String block, String facilitytype, String facility,
			String start, String end) {
		Long total = 0L;
		
		   
		   if (state.length() > 0 && district.length() == 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0)
		   {
			   total=mpreportviewRepository.findStatePreTermBirths(state,start);
			   
			   
			}
	      if (state.length() > 0 && district.length() > 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findDistrictPreTermBirths(state,district,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findBlockPreTermBirths(state,district,block,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findFacilitytypePreTermBirths(state,district,block,facilitytype,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() > 0) {
			  
				 total = mpreportviewRepository.findFacilityCodePreTermBirths(state,district,block,facilitytype,facility,start);
		
		   }
		   if(total == null || total == 0)
			   total = 0L;
		   
		
		return total.toString();
	}

	private Object maternalDeaths(String state, String district, String block, String facilitytype, String facility,
			String start, String end) {
		Long total = 0L;
		
		   
		   if (state.length() > 0 && district.length() == 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0)
		   {
			   total=babynotesRepository.findStateMaternalDeaths(state,start);			   
			   
		  }
	       if (state.length() > 0 && district.length() > 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = babynotesRepository.findDistrictMaternalDeaths(state,district,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = babynotesRepository.findBlockMaternalDeaths(state,district,block,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() == 0) {
				 total = babynotesRepository.findFacilitytypeMaternalDeaths(state,district,block,facilitytype,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() > 0) {
			  
				 total = babynotesRepository.findFacilityCodeMaternalDeaths(state,district,block,facilitytype,facility,start);
		
		   }
		   if(total == null || total == 0)
			   total = 0L;
		   
		
		return total.toString();	
	}

	private Object obstructedLabor(String state, String district, String block, String facilitytype, String facility,
			String start, String end)
	{
		Long total = 0L;
		
		   
		   if (state.length() > 0 && district.length() == 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0)
		   {
			   total=babynotesRepository.findStateObstructedLabor(state,start);			   
			   
			}
	        if (state.length() > 0 && district.length() > 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = babynotesRepository.findDistrictObstructedLabor(state,district,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = babynotesRepository.findBlockObstructedLabor(state,district,block,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() == 0) {
				 total = babynotesRepository.findFacilitytypeObstructedLabor(state,district,block,facilitytype,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() > 0) {
			  
				 total = babynotesRepository.findFacilityCodeObstructedLabor(state,district,block,facilitytype,facility,start);
		
		   }
		   if(total == null || total == 0)
			   total = 0L;
		   
		
		return total.toString();
		
		
	}

	private Object stillBirths(String state, String district, String block, String facilitytype, String facility,
			String start, String end) {
		Long total = 0L;
		   
		   
		   if (state.length() > 0 && district.length() == 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0)
		    {
			   total=mpreportviewRepository.findStateStillBirthDetails(state,start);			   
			   
			}
	       if (state.length() > 0 && district.length() > 0 && block.length() == 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findDistrictStillBirth(state,district,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() == 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findBlockStillBirth(state,district,block,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() == 0) {
				 total = mpreportviewRepository.findFacilitytypeStillBirth(state,district,block,facilitytype,start);
			}
		   if (state.length() > 0 && district.length() > 0 && block.length() > 0
					&& facilitytype.length() > 0 && facility.length() > 0) {
			  
				 total = mpreportviewRepository.findFacilityCodeStillBirth(state,district,block,facilitytype,facility,start);
		
		   }
		   if(total == null || total == 0)
			   total = 0L;
		   
		
		return total.toString();
	}

	private Object liveBirths(String state, String district, String block, String facilitytype, String facility,
			String start, String end)	
	{
		
		
		       Long total = 0L;
			   
			   if (state.length() > 0 && district.length() == 0 && block.length() == 0
						&& facilitytype.length() == 0 && facility.length() == 0)
			   {
				   total=mpreportviewRepository.findByStateLiveBirthDetails(state,start);				   
				   
			   }
		      if (state.length() > 0 && district.length() > 0 && block.length() == 0
						&& facilitytype.length() == 0 && facility.length() == 0)
		        {
					 total = mpreportviewRepository.findByDistrictLiveBirthDetails(state,district,start);
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() > 0
						&& facilitytype.length() == 0 && facility.length() == 0)
			    {
					 total = mpreportviewRepository.findByBlockLiveBirthDetails(state,district,block,start);
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() > 0
						&& facilitytype.length() > 0 && facility.length() == 0) 
			    {
					 total = mpreportviewRepository.findByFacilitytypeLiveBirthDetails(state,district,block,facilitytype,start);
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() > 0
						&& facilitytype.length() > 0 && facility.length() > 0)
			   {
				  	 total = mpreportviewRepository.findByFacilityCodeLiveBirthDetails(state,district,block,facilitytype,facility,start);
			
			   }
			   if(total == null || total == 0)
				   total = 0L;
			   
			
			return total.toString();
		
		
	}

	private Map<String, Object> actualdeliveryreport1_(String userdetail) 
	{
		
		org.json.JSONObject map = new org.json.JSONObject(userdetail);
		String  block = map.getString("block_code").toString().equalsIgnoreCase("ALL")?"":map.getString("block_code").toString();
		String district = map.getString("district_code").toString().equalsIgnoreCase("ALL")?"":map.getString("district_code").toString();
		String state = map.getString("state_code").toString();
		String facility = map.getString("facility_code").toString().equalsIgnoreCase("ALL")?"":map.getString("facility_code").toString();
		String facilitytype = map.getString("facility_type_id").toString().equalsIgnoreCase("ALL")?"":map.getString("facility_type_id").toString();

		
		String year =map.getString("input_year").toString();
		String month =map.getString("input_month").toString();
		List<L1MPReportModel>	listUsers = null;
		List<L1MPReportModel>	listUsers1 = null;
		ArrayList<Map<String,Object>> arr = new ArrayList<Map<String, Object>>();
		ArrayList<Map<String,Object>> arrobj = new ArrayList<Map<String, Object>>();

		java.util.Date mpReport;

		if((!year.equalsIgnoreCase("ALL"))&&(!month.equalsIgnoreCase("ALL")))
		{
			
			
			   if (state.length() > 0 && district.length() == 0 && block.length() == 0
						&& facilitytype.length() == 0 && facility.length() == 0)
			   {
					//System.out.println("state--ifffff"+state.length());

				   listUsers=l1mpreportRepository.findByState(Integer.parseInt(state),Integer.parseInt(year),Integer.parseInt(month));
					System.out.println("listUsers--"+listUsers.size());
		   
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() == 0
						&& facilitytype.length() == 0 && facility.length() == 0) {
			 listUsers = l1mpreportRepository.findByDistrict(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(year),Integer.parseInt(month));
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() > 0
						&& facilitytype.length() == 0 && facility.length() == 0) {
				   listUsers = l1mpreportRepository.findByBlock(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),Integer.parseInt(year),Integer.parseInt(month));
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() > 0
						&& facilitytype.length() > 0 && facility.length() == 0) {
				   listUsers = l1mpreportRepository.findByFacilitytype(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),Integer.parseInt(facilitytype),Integer.parseInt(year),Integer.parseInt(month));
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() > 0
						&& facilitytype.length() > 0 && facility.length() > 0) {
				  
				   listUsers = l1mpreportRepository.findByFacilityCode(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),Integer.parseInt(facilitytype),Integer.parseInt(facility),Integer.parseInt(year),Integer.parseInt(month));
			
			   }
			
				System.out.println("listUsers11111------"+listUsers.size());

				ArrayList<Map<String,Object>> arrob = new ArrayList<Map<String, Object>>();

			Iterator<L1MPReportModel> it = listUsers.iterator();
			//System.out.println("listUsers------"+listUsers.size());
			while (it.hasNext()) {
				Map<String,Object> obj = new HashMap<String,Object>();
				L1MPReportModel user = it.next();
				obj.put("normal_deliveries", ""+user.getNormal_deliveries());
				obj.put("assisted_deliveries",""+ user.getAssisted_deliveries());
				obj.put("caesarean_deliveries", ""+user.getCaesarean_deliveries());
				obj.put("live_births", ""+user.getLive_births());
				obj.put("still_births", ""+user.getStillbirths());
				obj.put("maternal_deaths", ""+user.getMaternal_deaths());
				arrob.add(obj);

			}
			
			
			
			if(arrob.size() > 0) {

				return (Map<String, Object>) arrob.get(arrob.size()-1);
				
			}
			else if(arrob.size() == 0)
			{
				Map<String, Object> obj1 = new HashMap<String, Object>();
				obj1.put("live_births", 0);
				obj1.put("assisted_deliveries", 0);
				obj1.put("still_births", 0);
				obj1.put("maternal_deaths", 0);
				obj1.put("normal_deliveries", 0);
				obj1.put("caesarean_deliveries", 0);
				arrob.add((Map<String, Object>) obj1);
				return (Map<String,Object>) arrob.get(0);
			}
			else
			{
			return (Map<String,Object>) arrob.get(0);
		}
			
			}


		if(year.equalsIgnoreCase("ALL")&&month.equalsIgnoreCase("ALL"))
		{
			System.out.println("ALLLllllllllll");
			listUsers1=l1mpreportRepository.findMaxTimerecords();
			for (@SuppressWarnings("rawtypes")
				Iterator iterator = listUsers1.iterator(); iterator.hasNext();)
				{
					 mpReport= (java.util.Date) iterator.next();
				
				
			   if (state.length() > 0 && district.length() == 0 && block.length() == 0
						&& facilitytype.length() == 0 && facility.length() == 0)
			   {
				   listUsers=l1mpreportRepository.findByStateALL(Integer.parseInt(state),mpReport);
				   			   
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() == 0
						&& facilitytype.length() == 0 && facility.length() == 0) {
			 listUsers = l1mpreportRepository.findByDistrictALL(Integer.parseInt(state),Integer.parseInt(district),mpReport);
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() > 0
						&& facilitytype.length() == 0 && facility.length() == 0) {
				   listUsers = l1mpreportRepository.findByBlockALL(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),mpReport);
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() > 0
						&& facilitytype.length() > 0 && facility.length() == 0) {
				   listUsers = l1mpreportRepository.findByFacilitytypeALL(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),Integer.parseInt(facilitytype),mpReport);
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() > 0
						&& facilitytype.length() > 0 && facility.length() > 0) {
				  
				   listUsers = l1mpreportRepository.findByFacilityCodeALL(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),Integer.parseInt(facilitytype),Integer.parseInt(facility),mpReport);
			
			   }
				
			
System.out.println("listUsers===="+listUsers.size());
			Iterator<L1MPReportModel> itr = listUsers.iterator();
			while (itr.hasNext()) {
				Map<String,Object> obj = new HashMap<String,Object>();
				L1MPReportModel user = itr.next();
				obj.put("normal_deliveries", ""+user.getNormal_deliveries());
				obj.put("assisted_deliveries",""+ user.getAssisted_deliveries());
				obj.put("caesarean_deliveries", ""+user.getCaesarean_deliveries());
				obj.put("live_births", ""+user.getLive_births());
				obj.put("still_births", ""+user.getStillbirths());
				obj.put("maternal_deaths", ""+user.getMaternal_deaths());
				arr.add(obj);
				System.out.println("arr------"+arr.size());
			}
			
			}
		}
		
		if(!year.equalsIgnoreCase("ALL")&&month.equalsIgnoreCase("ALL"))
		{
			listUsers1=l1mpreportRepository.findMaxTimerecordsByYear(Integer.parseInt(year));
			for (@SuppressWarnings("rawtypes")
				Iterator iterator = listUsers1.iterator(); iterator.hasNext();)
				{
					 mpReport= (java.util.Date) iterator.next();
				
				
			   if (state.length() > 0 && district.length() == 0 && block.length() == 0
						&& facilitytype.length() == 0 && facility.length() == 0)
			   {
				   listUsers=l1mpreportRepository.findByStateALL(Integer.parseInt(state),mpReport);
				   			   
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() == 0
						&& facilitytype.length() == 0 && facility.length() == 0) {
			 listUsers = l1mpreportRepository.findByDistrictALL(Integer.parseInt(state),Integer.parseInt(district),mpReport);
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() > 0
						&& facilitytype.length() == 0 && facility.length() == 0) {
				   listUsers = l1mpreportRepository.findByBlockALL(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),mpReport);
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() > 0
						&& facilitytype.length() > 0 && facility.length() == 0) {
				   listUsers = l1mpreportRepository.findByFacilitytypeALL(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),Integer.parseInt(facilitytype),mpReport);
				}
			   if (state.length() > 0 && district.length() > 0 && block.length() > 0
						&& facilitytype.length() > 0 && facility.length() > 0) {
				  
				   listUsers = l1mpreportRepository.findByFacilityCodeALL(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),Integer.parseInt(facilitytype),Integer.parseInt(facility),mpReport);
			
			   }
				
			

			Iterator<L1MPReportModel> itr = listUsers.iterator();
			while (itr.hasNext()) {
				Map<String,Object> obj = new HashMap<String,Object>();
				L1MPReportModel user = itr.next();
				obj.put("normal_deliveries", ""+user.getNormal_deliveries());
				obj.put("assisted_deliveries",""+ user.getAssisted_deliveries());
				obj.put("caesarean_deliveries", ""+user.getCaesarean_deliveries());
				obj.put("live_births", ""+user.getLive_births());
				obj.put("still_births", ""+user.getStillbirths());
				obj.put("maternal_deaths", ""+user.getMaternal_deaths());
				arr.add(obj);
				System.out.println("arr------"+arr.size());

			}
				}
			
		}


			if(arr.size() > 0) 
				{
					int a=0,b=0,c=0,d=0,e=0,f=0;
					for(int i=0;i<arr.size();i++)
					{
						Map<String,Object> obj1 = new HashMap<String,Object>();

						a=a+Integer.parseInt(arr.get(i).get("normal_deliveries").toString());
						b=b+Integer.parseInt(arr.get(i).get("assisted_deliveries").toString());
						c=c+Integer.parseInt(arr.get(i).get("caesarean_deliveries").toString());
						d=d+Integer.parseInt(arr.get(i).get("live_births").toString());
						e=e+Integer.parseInt(arr.get(i).get("still_births").toString());
						f=f+Integer.parseInt(arr.get(i).get("maternal_deaths").toString());
						obj1.put("normal_deliveries",a);
						obj1.put("assisted_deliveries",b);
						obj1.put("caesarean_deliveries",c);
						obj1.put("live_births",d);
						obj1.put("still_births",e);
						obj1.put("maternal_deaths",f);
						arrobj.add(obj1);
						System.out.println("aaaaa"+a);

					}

					return (Map<String, Object>) arrobj.get(arrobj.size()-1);
					
				}
				else if(arr.size() == 0)
				{
					Map<String, Object> obj1 = new HashMap<String, Object>();
					obj1.put("live_births", 0);
					obj1.put("assisted_deliveries", 0);
					obj1.put("still_births", 0);
					obj1.put("maternal_deaths", 0);
					obj1.put("normal_deliveries", 0);
					obj1.put("caesarean_deliveries", 0);
					arr.add((Map<String, Object>) obj1);
					return (Map<String,Object>) arr.get(0);
				}
				else
				{
				return (Map<String,Object>) arr.get(0);
			}
			
		
			
		
	
		
		
		
	}

	
	
	

}
