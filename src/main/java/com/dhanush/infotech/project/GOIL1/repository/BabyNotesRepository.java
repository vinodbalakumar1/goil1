package com.dhanush.infotech.project.GOIL1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dhanush.infotech.project.GOIL1.model.BabyNotes;

public interface BabyNotesRepository extends JpaRepository<BabyNotes, Long> {

	
	@Query(value = "SELECT a FROM BabyNotes a WHERE unique_id = :caseId")
	List<BabyNotes> findByCaseId(@Param("caseId") String uniqueNo) ;
	
	
	@Query(value = "FROM BabyNotes  where unique_id = :uniqueId")
	List<BabyNotes> findByUniqueNo(@Param("uniqueId") String uniqueId);
	
}

