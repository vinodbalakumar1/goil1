package com.dhanush.infotech.project.GOIL1.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL1.model.Admission;
import com.dhanush.infotech.project.GOIL1.repository.AdmissionRepository;



@CrossOrigin
@RestController
@RequestMapping("/")
public class IcuBeneficiarySearch {

	@Autowired
	AdmissionRepository admissionRepository;
	
	
	@CrossOrigin
	@RequestMapping(value = "getbeneficarydetailsforicu/l1", method = RequestMethod.POST, produces = { "application/json" })
	public String getUserById(@RequestBody String userJsonReq) {
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String uniqueNo = openJson.getString("uniqueId");
		org.json.JSONObject map = new org.json.JSONObject();
		Optional<Admission> icu = admissionRepository.uniqueId(uniqueNo);
		if (icu.isPresent()){
		map.put("state"       ,icu.get().getState());
		map.put("district"    ,icu.get().getDistrict());
		map.put( "block"      ,icu.get().getBlock());
		map.put( "facility"   ,icu.get().getFacility());
		map.put("facilityType",icu.get().getFacilityType());
		map.put("admissionId", "");
		
		org.json.JSONObject admission = new org.json.JSONObject();  
		
		admission.put("district_name",                     icu.get().getDistrictname());
		 admission.put("name_of_facility",                 icu.get().getFacilityname());
		 admission.put("block_name",                       icu.get().getBlockname());
		admission.put("mcts_rch_number",                   icu.get().getMctsNo());
		 admission.put("obs_icu_reg_number",                "");
		 admission.put("bpl_status",                        "");
		 admission.put("aadhar_number",                     "");
		 admission.put("contact_number",                    "");
		 admission.put("name",                              icu.get().getPatientName());
		 admission.put("age",                               icu.get().getAge());
		 admission.put("w_o",                               icu.get().getWoDo());
		 admission.put("address",                           icu.get().getAddress());
		 admission.put("patient_contact_no",                "");
		 admission.put("caste",                             "");
		 admission.put("religion",                          "");
		 admission.put("admission_date",                    icu.get().getAdmissionDate());
		 admission.put("admission_time",                    icu.get().getTime());
		 admission.put("delivery_date",                    "");
		 admission.put("obstetric_etails_at_admission",     "");
		 admission.put("delivery_time",                     Integer.parseInt(icu.get().getCompleteness())>30 ? icu.get().getDelivery().getDeliveryNotesTime() : "");
		 admission.put("gravida",                           icu.get().getGravida());
		 admission.put("parity",                            icu.get().getParity());
		 admission.put("abortion",                          "");
		 admission.put("living_children",                   "");
		 
		 
		 	 admission.put("bedNo",                	"");
			 admission.put("provisionalDiagnosis",  "");
			 admission.put("referredIn",      		"");
			 admission.put("nameOfFacility1",    	icu.get().getFacilityname());
			 admission.put("reasonforReferral",     "");
			 admission.put("placeOfDelivery",       "");
			 admission.put("birthDate", 			"");
			 admission.put("birthTime",             "");
		 
		 
		 
		 
		 
		 admission.put("lmp",                               icu.get().getLmp());
		 admission.put("edd",                               icu.get().getEdd());
		 admission.put("number_of_anc",                    "");
		 admission.put("gestational_age",                  "");
		 admission.put("labour",                           "");
		 admission.put("presentation",                     "");
		 admission.put("uterine_tenderness",               "");
		 admission.put("fhr",                              "");
		 admission.put("if_fhr_yes",                       "");
		 admission.put("amniotic_fluid",                   "");
		 admission.put("course_of_labour",                 "");
		 admission.put("type_of_delivery",                "") ;
		 admission.put("aph",                              "");
		 admission.put("pph",                              "");
		 admission.put("p_v_foul_smelling",                "");
		 admission.put("indi_for_caesarean",               "");
		 admission.put("deliv_attended_by",                "");
		 admission.put("antenatal_steroids",               "");
		 admission.put("if_yes",                            Integer.parseInt(icu.get().getCompleteness())> 30 ? icu.get().getDelivery().getIfYesTimeOfInitiation() :"");
		 admission.put("no_of_doses",                      "");
		 admission.put("magnesium_sulphate",               "");
		 admission.put("if_yes_dose",                       Integer.parseInt(icu.get().getCompleteness())> 30 ? icu.get().getDelivery().getIfYesTimeOfInitiation() :"");
		 admission.put("time_of_last_doses",               "");
		 
		   org.json.JSONObject indication = new     org.json.JSONObject();
		 
		       indication.put("icu_admission",                "");
		       indication.put("hdu_admission",                "");
		       indication.put("hemorrhage_hdu",               "");
		       indication.put("hemorrhage_icu",               "");
		       indication.put("hemorrhage_others",            "");
		       indication.put("hypertensive_disorders_hdu",   "");
		       indication.put("hypertensive_disorders_icu",   "");
		       indication.put("sepsis_hdu",                   "");
		       indication.put("sepsis_icu",                   "");
		       indication.put("sepsis_others",                "");
		       indication.put("renal_dysfunction_hdu",        "");
		       indication.put("renal_dysfunction_icu",        "");
		       indication.put("jaundice_in_pregnancy_hdu",    "");
		       indication.put("jaundice_in_pregnancy_icu",    "");
		       indication.put("jaundice_in_pregnancy_others", "");
		       indication.put("coagulation_system_hdu",       "");
		       indication.put("coagulation_system_icu",       "");
		       indication.put("abnormal_vitals_hdu",          "");
		       indication.put("abnormal_vitals_icu",          "");
		       indication.put("abg_abnormalities_hdu",        "");
		       indication.put("abg_abnormalities_icu",        "");
		       indication.put("electrolyte_disturbances_hdu", "");
		       indication.put("electrolyte_disturbances_icu", "");
		       indication.put("medical_disorders_hdu",        "");
		       indication.put("medical_disorders_icu",        "");
		       indication.put("medical_disorders_others10",   "");
		       indication.put("cause_of_admission",           "");
		 
		            org.json.JSONObject history = new org.json.JSONObject();    
		                 history.put("past_medical_history",    ""); 
		                 history.put("past_surgical_history",   ""); 
		                 history.put("family_history",          ""); 
		 
		                 
		                 org.json.JSONObject baby = new org.json.JSONObject(); 
		                 
		                   baby.put("outcome_of_birth" ,  "");           
		                   baby.put("birth_weight",       "");           
		                   baby.put("multiple_births",    "");           
		                   baby.put("if_yes_number",      "");           
		                   baby.put("sexofbaby",          "");           
		                   baby.put("resu_required",      "");           
		                   baby.put("weeks_of_gestation", "");           
		                   baby.put("sncu_admission",     "");           
		                   baby.put("vitamin_K_given",    "");           
		                   baby.put("b_fed_within_1hour", "");           
		                 
		                    org.json.JSONObject general = new org.json.JSONObject(); 
		                 
		                  general.put("general_condition",   "");       
		                  general.put("height",              "");       
		                  general.put("weight",              "");       
		                  general.put("bmi",                 "");       
		                  general.put("heart_rate",          "");       
		                  general.put("blood_pressure",      "");       
		                  general.put("spo2",                "");       
		                  general.put("respiratory_rate",    "");       
		                  general.put("temperature",         "");       
		                  general.put("jvp",                 "");       
		                  general.put("pallor",              "");       
		                  general.put("icterus",             "");       
		                  general.put("cyanosis",            "");       
		                  general.put("oedema",              "");       
		                  general.put("g_ex_others",         "");       
		                                                             
		                    
		                     org.json.JSONObject systemic = new org.json.JSONObject(); 
		                    
		                        systemic.put("respiratory_system",           "");
		                        systemic.put("cardio_vascular_system",       "");
		                        systemic.put("central_nervous_system",       "");
		                        systemic.put("per_abdomen_examination",      "");
		                        systemic.put("per_speculum_vaginal",         "");
		                        systemic.put("any_others",                   "");
		                        systemic.put("respiratory_system_text",      "");
		                        systemic.put("cardio_vascular_system_text",  "");
		                        systemic.put("central_nervous_system_text",  "");
		                        systemic.put("per_abdomen_examination_text", "");
		                        systemic.put("per_speculum_vaginal_text",    "");
		                     
		                      org.json.JSONObject orders = new  org.json.JSONObject();
		                     
		                       orders.put("nutrition",             "");
		                       orders.put("investigations",        "");
		                       orders.put("medications",           "");
		                       orders.put("procedures",            "");
		                       orders.put("monitoring_schedule",   "");
		                       orders.put("multidiscipl_review",   "");
		                       orders.put("plan_for_next_24_hours","");
		                       orders.put("doctor_name_and_sign",  "");
		                      
		                       org.json.JSONObject sequentialorgan  = new   org.json.JSONObject();
		                       
		                       
		                        sequentialorgan.put("respiration",    "");
		                        sequentialorgan.put("coagulation",    "");
		                        sequentialorgan.put("liver",          "");
		                        sequentialorgan.put("cardiovascular", "");
		                        sequentialorgan.put("cns",            "");
		                        sequentialorgan.put("renal",          "");
		                       
		                         org.json.JSONObject laboratory = new org.json.JSONObject();
		                                                                              
		                  laboratory.put("day1_hb",                          "");   
		                  laboratory.put("day1_total_wbc_count",             "");   
		                  laboratory.put("day1_differential_wbc_count",      "");   
		                  laboratory.put("day1_platelet_count",              "");  
		                  laboratory.put("day1_peripheral_smear",            "");  
		                  laboratory.put("day1_bt_ct_crt_",                  "");  
		                  laboratory.put("day1_pt_aptt_inr",                 "");  
		                  laboratory.put("day1_blood_group_rh_type",         "");  
		                  laboratory.put("day1_sickling_test",               "");  
		                  laboratory.put("day1_random_blood_glucose",        "");  
		                  laboratory.put("day1_blood_urea_nitrogen",         "");  
		                  laboratory.put("day1_serum_creatinine",            "");  
		                  laboratory.put("day1_serum_calcium",               "");  
		                  laboratory.put("day1_serum_sodium",                "");  
		                  laboratory.put("day1_serum_potassium",             "");  
		                  laboratory.put("day1_total_protein_serum_albumin", "");   
		                  laboratory.put("day1_serum_biliorubin",            "");  
		                  laboratory.put("day1_sgpt_sgpt_ldh",               "");  
		                  laboratory.put("day1_serum_fibrinogen",            "");  
		                  laboratory.put("day1_d_dimer",                     "");  
		                  laboratory.put("day1_urine_rm",                    "");  
		                  laboratory.put("day1_stool_for_occult_blood",      "");  
		                  laboratory.put("day1_blood_gas_time",              "");  
		                  laboratory.put("day1_blood_gas_fio2",              "");  
		                  laboratory.put("day1_blood_gas_ph",                "");  
		                  laboratory.put("day1_blood_gas_pco2",              "");  
		                  laboratory.put("day1_blood_gas_po2",               "");  
		                  laboratory.put("day1_blood_gas_hco3",              "");  
		                  laboratory.put("day1_blood_gas_satn",              "");  
		                  laboratory.put("day2_hb",                          "");  
		                  laboratory.put("day2_total_wbc_count",             "");  
		                  laboratory.put("day2_differential_wbc_count",      "");  
		                  laboratory.put("day2_platelet_count",              "");  
		                  laboratory.put("day2_peripheral_smear",            "");  
		                  laboratory.put("day2_bt_ct_crt_",                  "");  
		                  laboratory.put("day2_pt_aptt_inr",                 "");  
		                  laboratory.put("day2_blood_group_rh_type",         "");  
		                  laboratory.put("day2_sickling_test",               "");  
		                  laboratory.put("day2_random_blood_glucose",        "");  
		                  laboratory.put("day2_blood_urea_nitrogen",         "");  
		                  laboratory.put("day2_serum_creatinine",            "");  
		                  laboratory.put("day2_serum_calcium",               "");  
		                  laboratory.put("day2_serum_sodium",                "");  
		                  laboratory.put("day2_serum_potassium",             "");  
		                  laboratory.put("day2_total_protein_serum_albumin", "");   
		                  laboratory.put("day2_serum_biliorubin",            "");  
		                  laboratory.put("day2_sgpt_sgpt_ldh",               "");  
		                  laboratory.put("day2_serum_fibrinogen",            "");  
		                  laboratory.put("day2_d_dimer",                     "");  
		                  laboratory.put("day2_urine_rm",                    "");  
		                  laboratory.put("day2_stool_for_occult_blood",      "");  
		                  laboratory.put("day2_blood_gas_time",              "");  
		                  laboratory.put("day2_blood_gas_fio2",              "");  
		                  laboratory.put("day2_blood_gas_ph",                "");  
		                  laboratory.put("day2_blood_gas_pco2",              "");  
		                  laboratory.put("day2_blood_gas_po2",               "");  
		                  laboratory.put("day2_blood_gas_hco3",              "");  
		                  laboratory.put("day2_blood_gas_satn",              "");  
		                  laboratory.put("day3_hb",                          "");  
		                  laboratory.put("day3_total_wbc_count",             "");  
		                  laboratory.put("day3_differential_wbc_count",      "");  
		                  laboratory.put("day3_platelet_count",              "");  
		                  laboratory.put("day3_peripheral_smear",            "");  
		                  laboratory.put("day3_bt_ct_crt_",                  "");  
		                  laboratory.put("day3_pt_aptt_inr",                 "");  
		                  laboratory.put("day3_blood_group_rh_type",         "");  
		                  laboratory.put("day3_sickling_test",               "");  
		                  laboratory.put("day3_random_blood_glucose",        "");  
		                  laboratory.put("day3_blood_urea_nitrogen",         "");  
		                  laboratory.put("day3_serum_creatinine",            "");  
		                  laboratory.put("day3_serum_calcium",               "");  
		                  laboratory.put("day3_serum_sodium",                "");  
		                  laboratory.put("day3_serum_potassium",             "");  
		                  laboratory.put("day3_total_protein_serum_albumin", "");   
		                  laboratory.put("day3_serum_biliorubin",            "");  
		                  laboratory.put("day3_sgpt_sgpt_ldh",               "");  
		                  laboratory.put("day3_serum_fibrinogen",            "");  
		                  laboratory.put("day3_d_dimer",                     "");  
		                  laboratory.put("day3_urine_rm",                    "");  
		                  laboratory.put("day3_stool_for_occult_blood",      "");  
		                  laboratory.put("day3_blood_gas_time",              "");  
		                  laboratory.put("day3_blood_gas_fio2",              "");  
		                  laboratory.put("day3_blood_gas_ph",                "");  
		                  laboratory.put("day3_blood_gas_pco2",              "");  
		                  laboratory.put("day3_blood_gas_po2",               "");  
		                  laboratory.put("day3_blood_gas_hco3",              "");  
		                  laboratory.put("day3_blood_gas_satn",              "");  
		                  laboratory.put("patient_name",                     "");  
		                  laboratory.put("reg_no",                           "");  
		                  laboratory.put("doa",                              "");  
		                  laboratory.put("doctor_in_charge",                 "");  
		                  laboratory.put("urine_culture",                    "");  
		                  laboratory.put("blood_culture",                    "");  
		                  laboratory.put("cervical_vaginal_swab",            "");  
		                  laboratory.put("any_other",                        "");  
		                  laboratory.put("hiv",                              "");  
		                  laboratory.put("hbsag",                            "");  
		                  laboratory.put("vdrl",                             "");  
		
		map.put("admission", admission);
		map.put("indication", indication);
		map.put("history", history);
		map.put("baby_information_at_birth", baby);
		map.put("general_examination", general); 
		map.put("systemic_examination", systemic);
		map.put("orders", orders);
		map.put("sequentialorgan", sequentialorgan);
		map.put("laboratory_investigation_sheet", laboratory);
		}
		return map.toString();
		
		
	}
}
