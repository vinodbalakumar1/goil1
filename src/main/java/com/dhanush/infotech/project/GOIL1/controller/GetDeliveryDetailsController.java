package com.dhanush.infotech.project.GOIL1.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL1.model.Admission;
import com.dhanush.infotech.project.GOIL1.model.BabyNotes;
import com.dhanush.infotech.project.GOIL1.model.Delivery;
import com.dhanush.infotech.project.GOIL1.repository.AdmissionRepository;
import com.dhanush.infotech.project.GOIL1.repository.AssessmentOfPostPartumConditionRepository;
import com.dhanush.infotech.project.GOIL1.repository.BabyNotesRepository;
import com.dhanush.infotech.project.GOIL1.repository.DeliveryRepoistory;
import com.dhanush.infotech.project.GOIL1.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api/")
public class GetDeliveryDetailsController {
	
	@Autowired
	AdmissionRepository admissionRepository;
	
	@Autowired
	BabyNotesRepository babyNotesRepository;
	
	
	@Autowired
	AssessmentOfPostPartumConditionRepository assessmentOfPostPartumConditionRepository;
	
	
	@Autowired
	DeliveryRepoistory DeliveryRepository;
	
	
	@CrossOrigin
	@RequestMapping(value = "/getdeliverydetailsbyid/l1/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public String getUserById(@PathVariable(value = "id") Long UserId) {
	
		JSONObject normal = new JSONObject();
		Admission admission = admissionRepository.findById(UserId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", UserId));
		JSONObject obj = new JSONObject();
	//	try {
		List<Delivery> obstetric1 = DeliveryRepository.findByCaseId(admission.getId());
		if(obstetric1.size() == 0) {
			obj.put("status","Record Not Found");
			return obj.toString();
		}
		
		obj.put("uniqueId", admission.getUniqueNo());
		obj.put("admissionId", Long.toString(admission.getId()));
		System.out.println(admission.getId());
		obj.put("id", "");
		
		JSONObject deliveryNotes = new JSONObject();
		
		deliveryNotes.put("deliveryOutcome1"              , admission.getDelivery().getDeliveryOutcome1())	;	              
		deliveryNotes.put("deliveryOutcome2"              , admission.getDelivery().getDeliveryOutcome2()) ;	                  
		deliveryNotes.put("deliveryNotesTime"             , admission.getDelivery().getDeliveryNotesTime()) ;	
		deliveryNotes.put("timeOfBirth"             	, admission.getDelivery().getDeliveryNotesTime()) ;
		deliveryNotes.put("sexOfBaby"                     , admission.getDelivery().getSexOfBaby() )        ;	                      
		deliveryNotes.put("preterm"                       , admission.getDelivery().getPreterm ());         	                          
		deliveryNotes.put("birthweight"                   , admission.getDelivery().getBirthweight());    	                          
		deliveryNotes.put("criedImmediatelyAfterBirth"    , admission.getDelivery().getCriedImmediatelyAfterBirth()); 
		deliveryNotes.put("resuscitated"                  , admission.getDelivery().getResuscitated ());                            
		deliveryNotes.put("breastFeedingInitiated"        , admission.getDelivery().getBreastFeedingInitiated());         
		deliveryNotes.put("ifYesTimeOfInitiation"         , admission.getDelivery().getIfYesTimeOfInitiation());           
		deliveryNotes.put("congenitalAnomaly"             , admission.getDelivery().getCongenitalAnomaly());                   
		deliveryNotes.put( "specify"                ,       admission.getDelivery().getSpecify ());                                      
        

		JSONObject others = new JSONObject();
		others.put("field1", ""+admission.getField1());
		others.put("field2", ""+admission.getField2());
		others.put("field3", ""+admission.getField3());
		others.put("field4", ""+admission.getField4());
		others.put("field5", ""+admission.getField5());
		others.put("field6", ""+admission.getField6());
		others.put("field7", ""+admission.getField7());
		others.put("field8", ""+admission.getField8());
		others.put("field9",  ""+admission.getField9());
		others.put("field10", ""+admission.getField10());

	                       
		   JSONArray arr = new JSONArray();
		List<BabyNotes> childList1 = admission.getBabynotes();
		for(BabyNotes listChild : childList1) {
			org.json.JSONObject listObj = new org.json.JSONObject();
			listObj.put("id", ""+listChild.getId());
			listObj.put("injVitaminKAdministered",               listChild.getInjVitaminKAdministered());                      
			listObj.put("ifYesDose",                             listChild.getIfYesDose());                                                  
			listObj.put("vaccinationDone",                       listChild.getVaccinationDone());                                      
			listObj.put("conditionOfBaby",                       listChild.getConditionOfBaby());                                      
			listObj.put("detailsOf3rd4thStageOfLabor",           listChild.getDetailsOf3rd4thStageOfLabor());              
			listObj.put("anyOtherDrugSpecify",                   listChild.getAnyOtherDrugSpecify());                              
			listObj.put("cctWard",                               listChild.getCctWard());                                                      
			listObj.put("utMassageWard",                         listChild.getUtMassageWard());                                          
			listObj.put("placentaCompleteWard",                  listChild.getPlacentaCompleteWard());                            
			listObj.put("episiotomyGivenWard",                   listChild.getEpisiotomyGivenWard());                              
			listObj.put("ppiucdInsertedWard",                    listChild.getPpiucdInsertedWard());                                
			listObj.put("complicationsDuringDeliveryWard",       listChild.getComplicationsDuringDeliveryWard());      
			listObj.put("dateOfTransferToPncWard",               listChild.getDateOfTransferToPncWard());                      
			listObj.put("timeOfTransferToPncWard",               listChild.getTimeOfTransferToPncWard());                      
			listObj.put("babyReferredWard",                      listChild.getBabyReferredWard());                                    
			listObj.put("ifYesgiveReasonWard",                   listChild.getIfYesgiveReasonWard());                              
			
			arr.put(listObj);
		}
		
		
		
		
		
		
			org.json.JSONObject listObj = new org.json.JSONObject();
			listObj.put("id", ""+admission.getId());
			listObj.put("MotherBPDay30min1",admission.getAssessmentOfPostPartumCondition().getMotherBPDay30min1());
			listObj.put("MotherBPDay30min2" ,   admission.getAssessmentOfPostPartumCondition().getMotherBPDay30min2  ());
			listObj.put("MotherBPDay30min3" ,   admission.getAssessmentOfPostPartumCondition().getMotherBPDay30min3  ());
			listObj.put("MotherBPDay30min4" ,   admission.getAssessmentOfPostPartumCondition().getMotherBPDay30min4  ());
			listObj.put("MotherBPDay6hrs5"  ,   admission.getAssessmentOfPostPartumCondition().getMotherBPDay6hrs5   ());
			listObj.put("MotherBPDay6hrs6"  ,   admission.getAssessmentOfPostPartumCondition().getMotherBPDay6hrs6   ());
			listObj.put("MotherBPDay6hrs7"  ,   admission.getAssessmentOfPostPartumCondition().getMotherBPDay6hrs7   ());
			listObj.put("MotherBPDay2morning" , admission.getAssessmentOfPostPartumCondition().getMotherBPDay2morning());
			listObj.put("MotherBPDay2evening" , admission.getAssessmentOfPostPartumCondition().getMotherBPDay2evening());
			listObj.put("MothertempDay30min1"  , admission.getAssessmentOfPostPartumCondition(). getMothertempDay30min1  ());
			listObj.put("MothertempDay30min2"  , admission.getAssessmentOfPostPartumCondition(). getMothertempDay30min2  ());
			listObj.put("MothertempDay30min3"  , admission.getAssessmentOfPostPartumCondition(). getMothertempDay30min3  ());
			listObj.put("MothertempDay30min4"  , admission.getAssessmentOfPostPartumCondition(). getMothertempDay30min4  ());
			listObj.put("MothertempDay6hrs5"   , admission.getAssessmentOfPostPartumCondition(). getMothertempDay6hrs5   ());
			listObj.put("MothertempDay6hrs6"   , admission.getAssessmentOfPostPartumCondition(). getMothertempDay6hrs6   ());
			listObj.put("MothertempDay6hrs7"   , admission.getAssessmentOfPostPartumCondition(). getMothertempDay6hrs7   ());
			listObj.put("MothertempDay2morning", admission.getAssessmentOfPostPartumCondition(). getMothertempDay2morning());
			listObj.put("MothertempDay2evening", admission.getAssessmentOfPostPartumCondition(). getMothertempDay2evening());
			listObj.put("MotherpulseDay30min1"   ,   admission.getAssessmentOfPostPartumCondition().getMotherpulseDay30min1    ()); 
			listObj.put("MotherpulseDay30min2"   ,   admission.getAssessmentOfPostPartumCondition().getMotherpulseDay30min2    ()); 
			listObj.put("MotherpulseDay30min3"   ,   admission.getAssessmentOfPostPartumCondition().getMotherpulseDay30min3    ()); 
			listObj.put("MotherpulseDay30min4"   ,   admission.getAssessmentOfPostPartumCondition().getMotherpulseDay30min4    ()); 
			listObj.put("MotherpulseDay6hrs5"    ,   admission.getAssessmentOfPostPartumCondition().getMotherpulseDay6hrs5     ()); 
			listObj.put("MotherpulseDay6hrs6"    ,   admission.getAssessmentOfPostPartumCondition().getMotherpulseDay6hrs6     ()); 
			listObj.put("MotherpulseDay6hrs7"    ,   admission.getAssessmentOfPostPartumCondition().getMotherpulseDay6hrs7     ()); 
			listObj.put("MotherpulseDay2morning" ,   admission.getAssessmentOfPostPartumCondition().getMotherpulseDay2morning  ()); 
			listObj.put("MotherpulseDay2evening" ,   admission.getAssessmentOfPostPartumCondition().getMotherpulseDay2evening  ()); 
			listObj.put("MotherBreastconditionDay30min1"  ,   admission.getAssessmentOfPostPartumCondition().getMotherBreastconditionDay30min1     ());   
			listObj.put("MotherBreastconditionDay30min2"  ,   admission.getAssessmentOfPostPartumCondition().getMotherBreastconditionDay30min2     ());   
			listObj.put( "MotherBreastconditionDay30min3" ,   admission.getAssessmentOfPostPartumCondition().getMotherBreastconditionDay30min3     ());   
			listObj.put( "MotherBreastconditionDay30min4" ,   admission.getAssessmentOfPostPartumCondition().getMotherBreastconditionDay30min4     ());   
			listObj.put( "MotherBreastconditionDay6hrs5"  ,   admission.getAssessmentOfPostPartumCondition().getMotherBreastconditionDay6hrs5      ());   
			listObj.put( "MotherBreastconditionDay6hrs6"  ,   admission.getAssessmentOfPostPartumCondition().getMotherBreastconditionDay6hrs6      ());   
			listObj.put( "MotherBreastconditionDay6hrs7"  ,   admission.getAssessmentOfPostPartumCondition().getMotherBreastconditionDay6hrs7      ());   
			listObj.put( "MotherBreastconditionDay2morning",  admission.getAssessmentOfPostPartumCondition().getMotherBreastconditionDay2morning   ());   
			listObj.put( "MotherBreastconditionDay2evening",  admission.getAssessmentOfPostPartumCondition().getMotherBreastconditionDay2evening   ());   
			listObj.put( "MotherBleedingPVDay30min1"    ,     admission.getAssessmentOfPostPartumCondition().getMotherBleedingPVDay30min1        ());                                    
			listObj.put( "MotherBleedingPVDay30min2"    ,     admission.getAssessmentOfPostPartumCondition().getMotherBleedingPVDay30min2        ()); 
			listObj.put( "MotherBleedingPVDay30min3"    ,     admission.getAssessmentOfPostPartumCondition().getMotherBleedingPVDay30min3        ()); 
			listObj.put( "MotherBleedingPVDay30min4"    ,     admission.getAssessmentOfPostPartumCondition().getMotherBleedingPVDay30min4        ()); 
			listObj.put( "MotherBleedingPVDay6hrs5"     ,     admission.getAssessmentOfPostPartumCondition().getMotherBleedingPVDay6hrs5         ()); 
			listObj.put( "MotherBleedingPVDay6hrs6"     ,     admission.getAssessmentOfPostPartumCondition().getMotherBleedingPVDay6hrs6         ()); 
			listObj.put( "MotherBleedingPVDay6hrs7"     ,     admission.getAssessmentOfPostPartumCondition().getMotherBleedingPVDay6hrs7         ()); 
			listObj.put( "MotherBleedingPVDay2morning"  ,     admission.getAssessmentOfPostPartumCondition().getMotherBleedingPVDay2morning      ()); 
			listObj.put( "MotherBleedingPVDay2evening"  ,     admission.getAssessmentOfPostPartumCondition().getMotherBleedingPVDay2evening      ()); 
			listObj.put( "MotherUterineToneDay30min1"     ,       admission.getAssessmentOfPostPartumCondition().getMotherUterineToneDay30min1      ());  
			listObj.put( "MotherUterineToneDay30min2"     ,       admission.getAssessmentOfPostPartumCondition().getMotherUterineToneDay30min2      ());  
			listObj.put( "MotherUterineToneDay30min3"     ,       admission.getAssessmentOfPostPartumCondition().getMotherUterineToneDay30min3      ());  
			listObj.put( "MotherUterineToneDay30min4"     ,       admission.getAssessmentOfPostPartumCondition().getMotherUterineToneDay30min4      ());  
			listObj.put( "MotherUterineToneDay6hrs5"      ,       admission.getAssessmentOfPostPartumCondition().getMotherUterineToneDay6hrs5       ());  
			listObj.put( "MotherUterineToneDay6hrs6"      ,       admission.getAssessmentOfPostPartumCondition().getMotherUterineToneDay6hrs6       ());  
			listObj.put( "MotherUterineToneDay6hrs7"      ,       admission.getAssessmentOfPostPartumCondition().getMotherUterineToneDay6hrs7       ());  
			listObj.put( "MotherUterineToneDay2morning"   ,       admission.getAssessmentOfPostPartumCondition().getMotherUterineToneDay2morning    ());  
			listObj.put( "MotherUterineToneDay2evening"   ,       admission.getAssessmentOfPostPartumCondition().getMotherUterineToneDay2evening    ());  
			listObj.put( "MotherEpisiotomyTearDay30min1"    ,       admission.getAssessmentOfPostPartumCondition().getMotherEpisiotomyTearDay30min1     ());                                               
			listObj.put( "MotherEpisiotomyTearDay30min2"    ,       admission.getAssessmentOfPostPartumCondition().getMotherEpisiotomyTearDay30min2     ());  
			listObj.put( "MotherEpisiotomyTearDay30min3"    ,       admission.getAssessmentOfPostPartumCondition().getMotherEpisiotomyTearDay30min3     ());  
			listObj.put( "MotherEpisiotomyTearDay30min4"    ,       admission.getAssessmentOfPostPartumCondition().getMotherEpisiotomyTearDay30min4     ());  
			listObj.put( "MotherEpisiotomyTearDay6hrs5"     ,       admission.getAssessmentOfPostPartumCondition().getMotherEpisiotomyTearDay6hrs5      ());  
			listObj.put( "MotherEpisiotomyTearDay6hrs6"     ,       admission.getAssessmentOfPostPartumCondition().getMotherEpisiotomyTearDay6hrs6      ());  
			listObj.put( "MotherEpisiotomyTearDay6hrs7"     ,       admission.getAssessmentOfPostPartumCondition().getMotherEpisiotomyTearDay6hrs7      ());  
			listObj.put( "MotherEpisiotomyTearDay2morning"  ,       admission.getAssessmentOfPostPartumCondition().getMotherEpisiotomyTearDay2morning   ());  
			listObj.put( "MotherEpisiotomyTearDay2evening"  ,       admission.getAssessmentOfPostPartumCondition().getMotherEpisiotomyTearDay2evening   ());  
			listObj.put(   "BabyResprateDay30min1"     ,       admission.getAssessmentOfPostPartumCondition().getBabyResprateDay30min1   ()); 
			listObj.put(  "BabyResprateDay30min2"      ,       admission.getAssessmentOfPostPartumCondition().getBabyResprateDay30min2   ()); 
			listObj.put(  "BabyResprateDay30min3"      ,       admission.getAssessmentOfPostPartumCondition().getBabyResprateDay30min3   ()); 
			listObj.put(  "BabyResprateDay30min4"      ,       admission.getAssessmentOfPostPartumCondition().getBabyResprateDay30min4   ()); 
			listObj.put(  "BabyResprateDay6hrs5"       ,       admission.getAssessmentOfPostPartumCondition().getBabyResprateDay6hrs5    ()); 
			listObj.put(  "BabyResprateDay6hrs6"       ,       admission.getAssessmentOfPostPartumCondition().getBabyResprateDay6hrs6    ()); 
			listObj.put(  "BabyResprateDay6hrs7"       ,       admission.getAssessmentOfPostPartumCondition().getBabyResprateDay6hrs7    ()); 
			listObj.put(  "BabyResprateDay2morning"    ,       admission.getAssessmentOfPostPartumCondition().getBabyResprateDay2morning ()); 
			listObj.put(  "BabyResprateDay2evening"    ,       admission.getAssessmentOfPostPartumCondition().getBabyResprateDay2evening ()); 
			listObj.put(  "BabytempDay30min1"              ,   admission.getAssessmentOfPostPartumCondition(). getBabytempDay30min1     ());
			listObj.put(  "BabytempDay30min2"              ,   admission.getAssessmentOfPostPartumCondition(). getBabytempDay30min2     ());
			listObj.put(  "BabytempDay30min3"              ,   admission.getAssessmentOfPostPartumCondition(). getBabytempDay30min3     ());
			listObj.put(  "BabytempDay30min4"              ,   admission.getAssessmentOfPostPartumCondition(). getBabytempDay30min4     ());
			listObj.put(  "BabytempDay6hrs5"               ,   admission.getAssessmentOfPostPartumCondition(). getBabytempDay6hrs5      ());
			listObj.put(  "BabytempDay6hrs6"               ,   admission.getAssessmentOfPostPartumCondition(). getBabytempDay6hrs6      ());
			listObj.put(  "BabytempDay6hrs7"               ,   admission.getAssessmentOfPostPartumCondition(). getBabytempDay6hrs7      ());
			listObj.put(  "BabytempDay2morning"            ,   admission.getAssessmentOfPostPartumCondition(). getBabytempDay2morning   ());
			listObj.put(  "BabytempDay2evening"            ,   admission.getAssessmentOfPostPartumCondition(). getBabytempDay2evening   ());
			listObj.put(  "BabyBreastfeedingDay30min1"         ,   admission.getAssessmentOfPostPartumCondition().getBabyBreastfeedingDay30min1  ());                             
			listObj.put(  "BabyBreastfeedingDay30min2"         ,   admission.getAssessmentOfPostPartumCondition().getBabyBreastfeedingDay30min2  ());
			listObj.put(  "BabyBreastfeedingDay30min3"         ,   admission.getAssessmentOfPostPartumCondition().getBabyBreastfeedingDay30min3  ());
			listObj.put(  "BabyBreastfeeding30min4"            ,   admission.getAssessmentOfPostPartumCondition().getBabyBreastfeeding30min4     ());
			listObj.put(  "BabyBreastfeedingDay6hrs5"          ,   admission.getAssessmentOfPostPartumCondition().getBabyBreastfeedingDay6hrs5   ());
			listObj.put(  "BabyBreastfeedingDay6hrs6"          ,   admission.getAssessmentOfPostPartumCondition().getBabyBreastfeedingDay6hrs6   ());
			listObj.put(  "BabyBreastfeedingDay6hrs7"          ,   admission.getAssessmentOfPostPartumCondition().getBabyBreastfeedingDay6hrs7   ());
			listObj.put(  "BabyBreastfeedingDay2morning"       ,   admission.getAssessmentOfPostPartumCondition().getBabyBreastfeedingDay2morning());
			listObj.put(  "BabyBreastfeedingDay2evening"       ,   admission.getAssessmentOfPostPartumCondition().getBabyBreastfeedingDay2evening());
			listObj.put(  "BabyActivityDay30min1"               ,   admission.getAssessmentOfPostPartumCondition().getBabyActivityDay30min1   ());
			listObj.put(  "BabyActivityDay30min2"               ,   admission.getAssessmentOfPostPartumCondition().getBabyActivityDay30min2   ());
			listObj.put(  "BabyActivityDay30min3"               ,   admission.getAssessmentOfPostPartumCondition().getBabyActivityDay30min3   ());
			listObj.put(  "BabyActivityDay30min4"               ,   admission.getAssessmentOfPostPartumCondition().getBabyActivityDay30min4   ());
			listObj.put(  "BabyActivityDay6hrs5"                ,   admission.getAssessmentOfPostPartumCondition().getBabyActivityDay6hrs5    ());
			listObj.put(  "BabyActivityDay6hrs6"                ,   admission.getAssessmentOfPostPartumCondition().getBabyActivityDay6hrs6    ());
			listObj.put(  "BabyActivityDay6hrs7"                ,   admission.getAssessmentOfPostPartumCondition().getBabyActivityDay6hrs7    ());
			listObj.put(  "BabyActivityDay2morning"             ,   admission.getAssessmentOfPostPartumCondition().getBabyActivityDay2morning ());
			listObj.put(  "BabyActivityDay2evening"             ,   admission.getAssessmentOfPostPartumCondition().getBabyActivityDay2evening ());
			listObj.put(   "BabyUmbilicalstumpDay30min1"          ,     admission .getAssessmentOfPostPartumCondition().getBabyUmbilicalstumpDay30min1   ());
			listObj.put(  "BabyUmbilicalstumpDay30min2"           ,     admission .getAssessmentOfPostPartumCondition().getBabyUmbilicalstumpDay30min2   ());
			listObj.put(  "BabyUmbilicalstumpDay30min3"           ,     admission .getAssessmentOfPostPartumCondition().getBabyUmbilicalstumpDay30min3   ());
			listObj.put(  "BabyUmbilicalstumpDay30min4"           ,     admission .getAssessmentOfPostPartumCondition().getBabyUmbilicalstumpDay30min4   ());
			listObj.put(  "BabyUmbilicalstumpDay6hrs5"            ,     admission .getAssessmentOfPostPartumCondition().getBabyUmbilicalstumpDay6hrs5    ());
			listObj.put(  "BabyUmbilicalstumpDay6hrs6"            ,     admission .getAssessmentOfPostPartumCondition().getBabyUmbilicalstumpDay6hrs6    ());
			listObj.put(  "BabyUmbilicalstumpDay6hrs7" 		      ,    admission.    getAssessmentOfPostPartumCondition().getBabyUmbilicalstumpDay6hrs7    ());
			listObj.put(  "BabyUmbilicalstumpDay2morning"         ,     admission .   getAssessmentOfPostPartumCondition().getBabyUmbilicalstumpDay2morning ());
			listObj.put(  "BabyUmbilicalstumpDay2evening"         ,     admission . getAssessmentOfPostPartumCondition().getBabyUmbilicalstumpDay2evening ());
			listObj.put(  "BabyJaundiceDay30min1"                 ,      admission. getAssessmentOfPostPartumCondition().getBabyJaundiceDay30min1   ());
			listObj.put(  "BabyJaundiceDay30min2"                 ,      admission. getAssessmentOfPostPartumCondition().getBabyJaundiceDay30min2   ());
			listObj.put(  "BabyJaundiceDay30min3"                 ,      admission. getAssessmentOfPostPartumCondition().getBabyJaundiceDay30min3   ());
			listObj.put(  "BabyJaundiceDay30min4"                 ,      admission. getAssessmentOfPostPartumCondition().getBabyJaundiceDay30min4   ());
			listObj.put(  "BabyJaundiceDay6hrs5"                  ,      admission. getAssessmentOfPostPartumCondition().getBabyJaundiceDay6hrs5    ());
			listObj.put(  "BabyJaundiceDay6hrs6"                  ,      admission. getAssessmentOfPostPartumCondition().getBabyJaundiceDay6hrs6    ());
			listObj.put(  "BabyJaundiceDay6hrs7"                  ,      admission. getAssessmentOfPostPartumCondition().getBabyJaundiceDay6hrs7    ());
			listObj.put(  "BabyJaundiceDay2morning"               ,      admission. getAssessmentOfPostPartumCondition().getBabyJaundiceDay2morning ());
			listObj.put(  "BabyJaundiceDay2evening"               ,      admission. getAssessmentOfPostPartumCondition().getBabyJaundiceDay2evening ());
			listObj.put(  "BabyPassedurineDay30min1"              ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedurineDay30min1  ());
			listObj.put(  "BabyPassedurineDay30min2"              ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedurineDay30min2  ());
			listObj.put(  "BabyPassedurineDay30min3"              ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedurineDay30min3  ());
			listObj.put(  "BabyPassedurineDay30min4"              ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedurineDay30min4  ());
			listObj.put(  "BabyPassedurineDay6hrs5"               ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedurineDay6hrs5   ());
			listObj.put(  "BabyPassedurineDay6hrs6"               ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedurineDay6hrs6   ());
			listObj.put(  "BabyPassedurineDay6hrs7"               ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedurineDay6hrs7   ());
			listObj.put( "BabyPassedurineDay2morning"             ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedurineDay2morning());
			listObj.put( "BabyPassedurineDay2evening"             ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedurineDay2evening());
			listObj.put( "BabyPassedstoolDay30min1"               ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedstoolDay30min1  ());
			listObj.put( "BabyPassedstoolDay30min2"               ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedstoolDay30min2  ());
			listObj.put( "BabyPassedstoolDay30min3"               ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedstoolDay30min3  ());
			listObj.put( "BabyPassedstoolDay30min4"               ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedstoolDay30min4  ());
			listObj.put( "BabyPassedstoolDay6hrs5"                ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedstoolDay6hrs5   ());
			listObj.put( "BabyPassedstoolDay6hrs6"                ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedstoolDay6hrs6   ());
			listObj.put( "BabyPassedstoolDay6hrs7"                ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedstoolDay6hrs7   ());
			listObj.put( "BabyPassedstoolDay2morning"             ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedstoolDay2morning());
			listObj.put( "BabyPassedstoolDay2evening"             ,        admission.getAssessmentOfPostPartumCondition().getBabyPassedstoolDay2evening());
		
		
		obj.put("deliveryNotes", deliveryNotes);
		obj.put("assessmentOfPostPartumCondition", listObj);
		obj.put("babyNotes", arr);
		obj.put("others", others);
		normal.put("status", "ok");
		normal.put("data", obj);
		
		/*catch(Exception e) {
			normal.put("status", "No Record Found");
			normal.put("data", obj);
		}*/
		return normal.toString();
	}
}
