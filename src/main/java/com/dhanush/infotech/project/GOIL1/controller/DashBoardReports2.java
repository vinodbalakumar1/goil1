package com.dhanush.infotech.project.GOIL1.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL1.repository.L1ComplicationsRepository;
import com.dhanush.infotech.project.GOIL1.repository.LiveCaesareanRepository;
import com.dhanush.infotech.project.GOIL1.repository.RecordingMotherBPRepository;
import com.dhanush.infotech.project.GOIL1.repository.TotalDeliveryRepository;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class DashBoardReports2 {
	
	public static String TOTALADMISSIONS="admissions_total";
	public static String STILLBIRTH="stillbirth_total";
	public static String MATERNALDEATH ="maternal_death_total";
	public static String TOTALECLAMPSIA ="pre_eclampsia_eclamps_pecentage";
	public static String PPH ="pph_pecentage"; 
	public static String CAESAREANDELIVERYPEC ="caesarean_section_pecentage";
	public static String STILLBIRTHPERC ="stillbirth_pecentage";
	public static String NEONATALASPHYXIA ="live_birth_with_neo_natal_asphyxia_pecentage";
	public static String PROLONGEDLABOR="prolonged_labor_pecentage";
	public static Long DELIVERIES = 0L;
	public static Long STILLBIRTHCOUNT = 0L;
	
	@Autowired
	RecordingMotherBPRepository recordingMotherBPRepository;
	
	@Autowired
	TotalDeliveryRepository totalDeliveryRepository ;
	
	@Autowired
	LiveCaesareanRepository liveCaesareanRepository;
	
	@Autowired
	L1ComplicationsRepository l1ComplicationsRepository;
	
	
	

	@CrossOrigin
	@RequestMapping(value = "outcomeindicator/l1", method = RequestMethod.POST, produces = { "application/json" })
	public Map<String, Object> discharge(@RequestBody String userJsonReq) throws NumberFormatException, Exception {
		org.json.JSONObject obj = new org.json.JSONObject(userJsonReq);
		String  block = obj.getString("block_code").toString();
		String district = obj.getString("district_code").toString();
		String state = obj.getString("state_code").toString();
		String facility = obj.getString("facility_code").toString();
		String facilitytype = obj.getString("facility_type_id").toString();
		String endDate = obj.getString("end_date").toString();
		String startDate = obj.getString("start_date").toString();
		JSONObject result = new JSONObject();
		JSONObject kpiResult1 = new JSONObject();
		 Map<String,Object> kpiResult = new HashMap<>();
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		   LocalDateTime now = LocalDateTime.now();
		 if(startDate.equalsIgnoreCase("ALL"))
		 {
			 startDate = "1995-01-01";
			 endDate = ""+dtf.format(now);
		 }
		 kpiResult.put(TOTALADMISSIONS,  totalAdmissions(state,district,block,facilitytype,facility,startDate,endDate));
		 kpiResult.put(STILLBIRTH,  totalStillBirth(state,district,block,facilitytype,facility,startDate,endDate));
		 kpiResult.put(MATERNALDEATH,  0);
		 kpiResult.put(TOTALECLAMPSIA, totalEclampsia(state,district,block,facilitytype,facility,startDate,endDate));
		 kpiResult.put(PPH, totalPPH(state,district,block,facilitytype,facility,startDate,endDate));	
		 kpiResult.put(CAESAREANDELIVERYPEC, 0);
		 kpiResult.put(STILLBIRTHPERC, stillBirth(state, district, block, facilitytype, facility, startDate, endDate));
		kpiResult.put(NEONATALASPHYXIA, 0);
		kpiResult.put(PROLONGEDLABOR, 0);
		result.put("result", kpiResult);
		 result.put("total", 8);
		 result.put("message", "8 Records Found");
		 kpiResult1.put("data", result);
		 kpiResult1.put("success", true);
		 return kpiResult1.toMap();
	}
	
	public String totalEclampsia(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   Long total = 0L;
		   
		  
		   
		   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {

				 total = l1ComplicationsRepository.findByState5(state,startDate,endDate)*100;
				 DELIVERIES = totalDeliveryRepository.findByState(state,startDate,endDate);
				 
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = l1ComplicationsRepository.findByDistrict5(state,district,startDate,endDate)*100;
				 DELIVERIES = totalDeliveryRepository.findByDistrict(state,district,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = l1ComplicationsRepository.findByBlock5(state,district,block,startDate,endDate)*100;
				 DELIVERIES = totalDeliveryRepository.findByBlock(state,district,block,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = l1ComplicationsRepository.findByFacilitytype5(state,district,block,facilitytype,startDate,endDate)*100;
				 DELIVERIES = totalDeliveryRepository.findByFacilitytype(state,district,block,facilitytype,startDate,endDate);
		   }
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL"))  {
				 total = l1ComplicationsRepository.findByFacility5(state,district,block,facilitytype,facility,startDate,endDate)*100;
				 DELIVERIES = totalDeliveryRepository.findByFacility(state,district,block,facilitytype,facility,startDate,endDate);
		   }
		   Long result;
		   if(total == null || total == 0 && DELIVERIES == null || DELIVERIES == 0) {
			   total = 0L;
			   DELIVERIES = 0L;
			   result = (long) 0;
		   }
		   else {
		   result = total/DELIVERIES;
		   }
		   return result.toString();
	   } 

	public String totalPPH(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   Long total = 0L;
		   
		  
		   
		   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {

				 total = l1ComplicationsRepository.findByState6(state,startDate,endDate)*100;
				System.out.println(total+"pph");
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = l1ComplicationsRepository.findByDistrict6(state,district,startDate,endDate)*100;
				
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = l1ComplicationsRepository.findByBlock6(state,district,block,startDate,endDate)*100;
				
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = l1ComplicationsRepository.findByFacilitytype6(state,district,block,facilitytype,startDate,endDate)*100;
				
		   }
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL")) {
				 total = l1ComplicationsRepository.findByFacility6(state,district,block,facilitytype,facility,startDate,endDate)*100;
			}
		   Long result ;
		   if(total == null || total == 0 && DELIVERIES == null || DELIVERIES == 0) {
			   total = 0L;
			   DELIVERIES = 0L;
			   result = (long) 0;
		   }
		   else {
			   System.out.println(total+"tot"+DELIVERIES+"del");
		    result = total/DELIVERIES;
		   }
		   return result.toString();
	   } 
	
	
	
	
	
	
	
	public String stillBirth(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate) {
		Long total;
		 if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {

				 STILLBIRTHCOUNT = liveCaesareanRepository.findByState3(state,startDate,endDate);
				 DELIVERIES = totalDeliveryRepository.findByState(state,startDate,endDate);
			}
		 if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
			   STILLBIRTHCOUNT = liveCaesareanRepository.findByDistrict3(state,district,startDate,endDate);
			   DELIVERIES = totalDeliveryRepository.findByDistrict(state,district,startDate,endDate);
			}
		 if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
			   STILLBIRTHCOUNT = liveCaesareanRepository.findByBlock3(state,district,block,startDate,endDate);
			   DELIVERIES = totalDeliveryRepository.findByBlock(state,district,block,startDate,endDate);
			}
		 if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
			   STILLBIRTHCOUNT = liveCaesareanRepository.findByFacilitytype3(state,district,block,facilitytype,startDate,endDate);
			   DELIVERIES = totalDeliveryRepository.findByFacilitytype(state,district,block,facilitytype,startDate,endDate);
			}
		 if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL"))  {
			   STILLBIRTHCOUNT = liveCaesareanRepository.findByFacility3(state,district,block,facilitytype,facility,startDate,endDate);
			   DELIVERIES = totalDeliveryRepository.findByFacility(state,district,block,facilitytype,facility,startDate,endDate);	
		 }
		 System.out.println(STILLBIRTHCOUNT+"STILLBIRTHCOUNT"+"DELIVERIES"+DELIVERIES);
		  
		   if( STILLBIRTHCOUNT == 0 || STILLBIRTHCOUNT == null && DELIVERIES == 0 || DELIVERIES ==0) {
			   total = (long) 0;
		   }
		   else {
			   total = STILLBIRTHCOUNT/STILLBIRTHCOUNT;	
		   }
		return total.toString();
		
	}
	 public String totalAdmissions(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   Long total = 0L;
		   
		   if (state.length() > 0 && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total =recordingMotherBPRepository.findByState1(state,startDate,endDate);
				 System.out.println("total"+total);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = recordingMotherBPRepository.findByDistrict1(state,district,startDate,endDate);
				 
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = recordingMotherBPRepository.findByBlock1(state,district,block,startDate,endDate);
				
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = recordingMotherBPRepository.findByFacilitytype1(state,district,block,facilitytype,startDate,endDate);
				
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL"))  {
				 total = recordingMotherBPRepository.findByFacility1(state,district,block,facilitytype,facility,startDate,endDate);
		   }
		   if(total == null || total == 0)
			   total = 0L;
		   return total.toString();
		  
	   }
	 
	
	 
	 public String totalStillBirth(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   
		 if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {

				 STILLBIRTHCOUNT = liveCaesareanRepository.findByState3(state,startDate,endDate);
			}
		 if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
			   STILLBIRTHCOUNT = liveCaesareanRepository.findByDistrict3(state,district,startDate,endDate);
			}
		 if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
			   STILLBIRTHCOUNT = liveCaesareanRepository.findByBlock3(state,district,block,startDate,endDate);
			}
		 if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
			   STILLBIRTHCOUNT = liveCaesareanRepository.findByFacilitytype3(state,district,block,facilitytype,startDate,endDate);
			}
		 if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL"))  {
			   STILLBIRTHCOUNT = liveCaesareanRepository.findByFacility3(state,district,block,facilitytype,facility,startDate,endDate);
			}
		   if(STILLBIRTHCOUNT == null || STILLBIRTHCOUNT == 0)
			   STILLBIRTHCOUNT = 0L;
		  
		   return STILLBIRTHCOUNT.toString();
	   }
	
	 
	 
	 
	 
	
	  
	  
	  
	  
}
