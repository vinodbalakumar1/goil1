package com.dhanush.infotech.project.GOIL1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dhanush.infotech.project.GOIL1.model.MPReportViewModel;

public interface MPReportViewRepository extends JpaRepository<MPReportViewModel, Long>
{

	//live birth
	
	@Query(value="select count(a.deliveryoutcome1) as count from MPReportViewModel a where a.state= :state and a.admissiondate like :start%  and a.deliveryoutcome1 like '%92%'")
	Long findByStateLiveBirthDetails(@Param("state")String state,@Param("start")String start);

	@Query(value="select count(a.deliveryoutcome1) as count from MPReportViewModel a where a.state= :state and a.district= :district and a.admissiondate like :start%  and a.deliveryoutcome1 like '%92%'")
	Long findByDistrictLiveBirthDetails(@Param("state")String state,@Param("district")String district,@Param("start")String start);
	
	
	@Query(value="select count(a.deliveryoutcome1) as count from MPReportViewModel a where a.state= :state and a.district= :district and a.block= :block and a.admissiondate like :start%  and a.deliveryoutcome1 like '%92%'")
	Long findByBlockLiveBirthDetails(@Param("state")String state,@Param("district")String district,@Param("block")String block,@Param("start")String start);
	

	 @Query(value = "select count(a.deliveryoutcome1) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and  a.deliveryoutcome1 like '%92%'")
	 Long findByFacilitytypeLiveBirthDetails(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	 @Query(value = "select  count(a.deliveryoutcome1) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.deliveryoutcome1 like '%92%'")
	 Long findByFacilityCodeLiveBirthDetails(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
	   @Param("start")String start);


//still birth
	
	 
	 @Query(value = "select count(a.deliveryoutcome1) as count FROM MPReportViewModel a where a.state = :state and a.admissiondate like :start%  and a.deliveryoutcome1 like '%93%' ")
	 Long findStateStillBirthDetails(@Param("state")String state, @Param("start") String start);

	 @Query(value = "select count(a.deliveryoutcome1) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.admissiondate like :start%  and a.deliveryoutcome1 like '%93%'")
	 Long findDistrictStillBirth(@Param("state")String state,@Param("district")String district, @Param("start")String start);

	 @Query(value = "select count(a.deliveryoutcome1) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start%  and a.deliveryoutcome1 like '%93%'")
	 Long findBlockStillBirth(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

	 @Query(value = "select count(a.deliveryoutcome1) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and  a.deliveryoutcome1 like '%93%'")
	 Long findFacilitytypeStillBirth(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	 @Query(value = "select  count(a.deliveryoutcome1) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.deliveryoutcome1 like '%93%'")
	 Long findFacilityCodeStillBirth(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
	   @Param("start")String start);
	 
	 
	 //for preterm
	 
	 
	 
	 
	   @Query(value = "select count(a.preterm) as count FROM MPReportViewModel a where a.state = :state and a.admissiondate like :start%  and a.preterm in('Yes','1') ")
		Long findStatePreTermBirths(@Param("state")String state, @Param("start") String start);

		@Query(value = "select count(a.preterm) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.admissiondate like :start%  and a.preterm in('Yes','1')")
		Long findDistrictPreTermBirths(@Param("state")String state,@Param("district")String district, @Param("start")String start);

		@Query(value = "select count(a.preterm) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and a.preterm in('Yes','1')")
		Long findBlockPreTermBirths(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

		@Query(value = "select count(a.preterm) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and a.preterm in('Yes','1')")
		Long findFacilitytypePreTermBirths(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		@Query(value = "select  count(a.preterm) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.preterm in('Yes','1')")
		Long findFacilityCodePreTermBirths(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
				@Param("start")String start);
		
		
		//for Asphyxia
		
		
		
		
		@Query(value = "select count(1) as count FROM MPReportViewModel a where a.state = :state and a.admissiondate like :start% and  a.resuscitated in ('true','yes','1')")
		Long findStateAsphyxia(@Param("state") String state,@Param("start") String start);
		
		
		@Query(value = "select count(1) as count FROM MPReportViewModel a where a.state = :state and a.district=:district and a.admissiondate like :start% and  a.resuscitated in ('true','yes','1')")
		Long findDistrictAsphyxia(@Param("state") String state,@Param("district") String district,@Param("start") String start);

		
		@Query(value = "select count(1) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start%  and a.resuscitated in ('true','Yes','1')")
		Long findBlockAsphyxia(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start);
		
		
		@Query(value = "select count(1) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start%  and a.resuscitated in ('true','Yes','1')")
		Long findFacilitytypeAsphyxia(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		@Query(value = "select  count(1) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.resuscitated in ('true','Yes','1')")
		Long findFacilityAsphyxia(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start);

		
		//for breast feed
		
		@Query(value = "select count(a.breastfeedinginitiated) as count FROM MPReportViewModel a where a.state = :state and a.admissiondate like :start% and a.breastfeedinginitiated in ('Yes','1')")
		Long findStateBreastFeed(@Param("state")String state, @Param("start") String start);

		@Query(value = "select count(a.breastfeedinginitiated) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.admissiondate like :start% and a.breastfeedinginitiated in ('Yes','1')")
		Long findDistrictBreastFeed(@Param("state")String state,@Param("district")String district, @Param("start")String start);

		@Query(value = "select count(a.breastfeedinginitiated) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and a.breastfeedinginitiated in ('Yes','1')")
		Long findBlockBreastFeed(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

		@Query(value = "select count(a.breastfeedinginitiated) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and a.breastfeedinginitiated in ('Yes','1')")
		Long findFacilitytypeBreastFeed(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		@Query(value = "select  count(a.breastfeedinginitiated) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.breastfeedinginitiated in ('Yes','1')")
		Long findFacilityCodeBreastFeed(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
				@Param("start")String start);
		
		
		
		 
		 //for baby weight
		
		

		@Query(value = "select count(a.birthweight) as count FROM MPReportViewModel a where a.state = :state and a.admissiondate like :start% and a.birthweight >0 ")
		Long findStateBabyWeight(@Param("state")String state, @Param("start") String start);

		@Query(value = "select count(a.birthweight) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.admissiondate like :start%  and a.birthweight >0")
		Long findDistrictBabyWeight(@Param("state")String state,@Param("district")String district, @Param("start")String start);

		@Query(value = "select count(a.birthweight) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start%  and  a.birthweight >0 ")
		Long findBlockBabyWeight(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

		@Query(value = "select count(a.birthweight) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start%  and  a.birthweight >0")
		Long findFacilitytypeBabyWeight(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		@Query(value = "select  count(a.birthweight) as count FROM MPReportViewModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.birthweight >0")
		Long findFacilityCodeBabyWeight(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
				@Param("start")String start);

		
		
	
	
}
