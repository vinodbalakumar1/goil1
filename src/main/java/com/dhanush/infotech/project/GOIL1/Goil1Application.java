package com.dhanush.infotech.project.GOIL1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Goil1Application {

	public static void main(String[] args) {
		
		SpringApplication.run(Goil1Application.class, args);
	}
}
