package com.dhanush.infotech.project.GOIL1.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.dhanush.infotech.project.GOIL1.model.AssessmentOfPostPartumCondition;

public interface AssessmentOfPostPartumConditionRepository extends JpaRepository<AssessmentOfPostPartumCondition, Long> {
	
	
	
	
}
