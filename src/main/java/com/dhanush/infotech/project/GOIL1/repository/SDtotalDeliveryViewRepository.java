package com.dhanush.infotech.project.GOIL1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dhanush.infotech.project.GOIL1.model.SDtotaldeliveryView;

public interface SDtotalDeliveryViewRepository extends JpaRepository<SDtotaldeliveryView,Long>{
	
	@Query(value = "select sum(a.delivery) as delivery FROM SDtotaldeliveryView a where a.state = :state and a.createdat like :date% ")
	Long findByState(@Param("state") String state,@Param("date") String date);

	@Query(value = "select sum(a.delivery) as delivery FROM SDtotaldeliveryView a where a.state = :state and a.district = :district and a.createdat like :date%")
	Long findByDistrict(@Param("state") String state,@Param("district") String district,@Param("date") String dte);
}
