package com.dhanush.infotech.project.GOIL1.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="l1csv")
public class L1CSVModel implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	private String state;
	private String district;
	private String block;
	private String facility;
	private String facility_type;
	private String birthweight;
	private String breast_feeding_initiated; 
	private String congenital_anomaly; 
	private String cried_immediately_after_birth;
	private String delivery_notes_time;
	private String delivery_outcome1 ;
	private String delivery_outcome2;
	private String if_yes_time_of_initiation;
	private String preterm ; 
	private String resuscitated ;
	private String sex_of_baby ; 
	private String specify ;
	private String any_other_drug_specify ;
	private String baby_referred_ward ;
	private String cct_ward ;
	private String complications_during_delivery_ward ;
	private String condition_of_baby ;
	private String date_of_transfer_to_pnc_ward ;
	private String details_of3rd4th_stage_of_labor ;
	private String episiotomy_given_ward ;
	private String if_yes_dose ;
	private String if_yesgive_reason_ward ;
	private String inj_vitaminkadministered ;
	private String placenta_complete_ward ;
	private String ppiucd_inserted_ward ;
	private String time_of_transfer_to_pnc_ward ;
	private String ut_massage_ward ;
	private String unique_id ;
	private String Baby1 ;
	private String Baby2 ;
	private String Baby3 ;
	private String vaccination_done;
	private String completeness ;
	private String gdm ;
	private String hbs_ag ;
	private String others_specify2 ;
	private String vdrlrpr ;
	private String abortion ;
	private String admission_date ;
	private String age ;
	private String antenatal_steroids_given ;
	private String anti_given_hb ;
	private String blockname ;
	private String blood_group ;
	private String blood_sugar ;
	private String booked;
	private String bpl_jsy_register ;
	private String contraception_history;
	private String districtname ;
	private String edd ;
	private String facilityname ;
	private String facilitytypename ;
	private String ga ;
	private String gravida ;
	private String hb ;
	private String hiv ;
	private String is_active ;
	private String living_children ;
	private String lmp ;
	private String malaria ;
	private String mcts_no ;
	private String medicalhoifany ;
	private String name_of_asha ;
	private String others1 ;
	private String pa_fundal_height ;
	private String pa_multiple_pregnancy ;
	private String pa_presenting_part ;
	private String parity ;
	private String past_obstetrics_history ;
	private String patient_name ;
	private String pv_exam_antibiotic_given ;
	private String pv_exam_cervical_dilation ;
	private String pv_exam_cervical_effacement ;
	private String pv_exam_membranes ;
	private String statename ;
	private String status;
	private String syphilis ;
	private String time ;
	private String unique_no;
	private String urine_protien ;
	private String urine_sugar ;
	private String vitals_bp_diastolic ;
	private String vitals_bp_systolic ;
	private String vitals_fhr ;
	private String vitals_pulse ;
	private String vitals_respiratory_rate ;
	private String vitals_temparature ;
	private String vitals_weight ;
	private String wo_do ;
	private String advice_of_baby ;
	private String advice_of_mother ;
	private String condition_of_mother ;
	private String others;
	
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}
	public String getFacility() {
		return facility;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	public String getFacility_type() {
		return facility_type;
	}
	public void setFacility_type(String facility_type) {
		this.facility_type = facility_type;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBirthweight() {
		return birthweight;
	}
	public void setBirthweight(String birthweight) {
		this.birthweight = birthweight;
	}
	public String getBreast_feeding_initiated() {
		return breast_feeding_initiated;
	}
	public void setBreast_feeding_initiated(String breast_feeding_initiated) {
		this.breast_feeding_initiated = breast_feeding_initiated;
	}
	public String getCongenital_anomaly() {
		return congenital_anomaly;
	}
	public void setCongenital_anomaly(String congenital_anomaly) {
		this.congenital_anomaly = congenital_anomaly;
	}
	public String getCried_immediately_after_birth() {
		return cried_immediately_after_birth;
	}
	public void setCried_immediately_after_birth(String cried_immediately_after_birth) {
		this.cried_immediately_after_birth = cried_immediately_after_birth;
	}
	public String getDelivery_notes_time() {
		return delivery_notes_time;
	}
	public void setDelivery_notes_time(String delivery_notes_time) {
		this.delivery_notes_time = delivery_notes_time;
	}
	public String getDelivery_outcome1() {
		return delivery_outcome1;
	}
	public void setDelivery_outcome1(String delivery_outcome1) {
		this.delivery_outcome1 = delivery_outcome1;
	}
	public String getDelivery_outcome2() {
		return delivery_outcome2;
	}
	public void setDelivery_outcome2(String delivery_outcome2) {
		this.delivery_outcome2 = delivery_outcome2;
	}
	public String getIf_yes_time_of_initiation() {
		return if_yes_time_of_initiation;
	}
	public void setIf_yes_time_of_initiation(String if_yes_time_of_initiation) {
		this.if_yes_time_of_initiation = if_yes_time_of_initiation;
	}
	public String getPreterm() {
		return preterm;
	}
	public void setPreterm(String preterm) {
		this.preterm = preterm;
	}
	public String getResuscitated() {
		return resuscitated;
	}
	public void setResuscitated(String resuscitated) {
		this.resuscitated = resuscitated;
	}
	public String getSex_of_baby() {
		return sex_of_baby;
	}
	public void setSex_of_baby(String sex_of_baby) {
		this.sex_of_baby = sex_of_baby;
	}
	public String getSpecify() {
		return specify;
	}
	public void setSpecify(String specify) {
		this.specify = specify;
	}
	public String getAny_other_drug_specify() {
		return any_other_drug_specify;
	}
	public void setAny_other_drug_specify(String any_other_drug_specify) {
		this.any_other_drug_specify = any_other_drug_specify;
	}
	public String getBaby_referred_ward() {
		return baby_referred_ward;
	}
	public void setBaby_referred_ward(String baby_referred_ward) {
		this.baby_referred_ward = baby_referred_ward;
	}
	public String getCct_ward() {
		return cct_ward;
	}
	public void setCct_ward(String cct_ward) {
		this.cct_ward = cct_ward;
	}
	public String getComplications_during_delivery_ward() {
		return complications_during_delivery_ward;
	}
	public void setComplications_during_delivery_ward(String complications_during_delivery_ward) {
		this.complications_during_delivery_ward = complications_during_delivery_ward;
	}
	public String getCondition_of_baby() {
		return condition_of_baby;
	}
	public void setCondition_of_baby(String condition_of_baby) {
		this.condition_of_baby = condition_of_baby;
	}
	public String getDate_of_transfer_to_pnc_ward() {
		return date_of_transfer_to_pnc_ward;
	}
	public void setDate_of_transfer_to_pnc_ward(String date_of_transfer_to_pnc_ward) {
		this.date_of_transfer_to_pnc_ward = date_of_transfer_to_pnc_ward;
	}
	public String getDetails_of3rd4th_stage_of_labor() {
		return details_of3rd4th_stage_of_labor;
	}
	public void setDetails_of3rd4th_stage_of_labor(String details_of3rd4th_stage_of_labor) {
		this.details_of3rd4th_stage_of_labor = details_of3rd4th_stage_of_labor;
	}
	public String getEpisiotomy_given_ward() {
		return episiotomy_given_ward;
	}
	public void setEpisiotomy_given_ward(String episiotomy_given_ward) {
		this.episiotomy_given_ward = episiotomy_given_ward;
	}
	public String getIf_yes_dose() {
		return if_yes_dose;
	}
	public void setIf_yes_dose(String if_yes_dose) {
		this.if_yes_dose = if_yes_dose;
	}
	public String getIf_yesgive_reason_ward() {
		return if_yesgive_reason_ward;
	}
	public void setIf_yesgive_reason_ward(String if_yesgive_reason_ward) {
		this.if_yesgive_reason_ward = if_yesgive_reason_ward;
	}
	public String getInj_vitaminkadministered() {
		return inj_vitaminkadministered;
	}
	public void setInj_vitaminkadministered(String inj_vitaminkadministered) {
		this.inj_vitaminkadministered = inj_vitaminkadministered;
	}
	public String getPlacenta_complete_ward() {
		return placenta_complete_ward;
	}
	public void setPlacenta_complete_ward(String placenta_complete_ward) {
		this.placenta_complete_ward = placenta_complete_ward;
	}
	public String getPpiucd_inserted_ward() {
		return ppiucd_inserted_ward;
	}
	public void setPpiucd_inserted_ward(String ppiucd_inserted_ward) {
		this.ppiucd_inserted_ward = ppiucd_inserted_ward;
	}
	public String getTime_of_transfer_to_pnc_ward() {
		return time_of_transfer_to_pnc_ward;
	}
	public void setTime_of_transfer_to_pnc_ward(String time_of_transfer_to_pnc_ward) {
		this.time_of_transfer_to_pnc_ward = time_of_transfer_to_pnc_ward;
	}
	public String getUt_massage_ward() {
		return ut_massage_ward;
	}
	public void setUt_massage_ward(String ut_massage_ward) {
		this.ut_massage_ward = ut_massage_ward;
	}
	public String getUnique_id() {
		return unique_id;
	}
	public void setUnique_id(String unique_id) {
		this.unique_id = unique_id;
	}
	public String getBaby1() {
		return Baby1;
	}
	public void setBaby1(String baby1) {
		Baby1 = baby1;
	}
	public String getBaby2() {
		return Baby2;
	}
	public void setBaby2(String baby2) {
		Baby2 = baby2;
	}
	public String getBaby3() {
		return Baby3;
	}
	public void setBaby3(String baby3) {
		Baby3 = baby3;
	}
	public String getVaccination_done() {
		return vaccination_done;
	}
	public void setVaccination_done(String vaccination_done) {
		this.vaccination_done = vaccination_done;
	}
	public String getCompleteness() {
		return completeness;
	}
	public void setCompleteness(String completeness) {
		this.completeness = completeness;
	}
	public String getGdm() {
		return gdm;
	}
	public void setGdm(String gdm) {
		this.gdm = gdm;
	}
	public String getHbs_ag() {
		return hbs_ag;
	}
	public void setHbs_ag(String hbs_ag) {
		this.hbs_ag = hbs_ag;
	}
	public String getOthers_specify2() {
		return others_specify2;
	}
	public void setOthers_specify2(String others_specify2) {
		this.others_specify2 = others_specify2;
	}
	public String getVdrlrpr() {
		return vdrlrpr;
	}
	public void setVdrlrpr(String vdrlrpr) {
		this.vdrlrpr = vdrlrpr;
	}
	public String getAbortion() {
		return abortion;
	}
	public void setAbortion(String abortion) {
		this.abortion = abortion;
	}
	public String getAdmission_date() {
		return admission_date;
	}
	public void setAdmission_date(String admission_date) {
		this.admission_date = admission_date;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getAntenatal_steroids_given() {
		return antenatal_steroids_given;
	}
	public void setAntenatal_steroids_given(String antenatal_steroids_given) {
		this.antenatal_steroids_given = antenatal_steroids_given;
	}
	public String getAnti_given_hb() {
		return anti_given_hb;
	}
	public void setAnti_given_hb(String anti_given_hb) {
		this.anti_given_hb = anti_given_hb;
	}
	public String getBlockname() {
		return blockname;
	}
	public void setBlockname(String blockname) {
		this.blockname = blockname;
	}
	public String getBlood_group() {
		return blood_group;
	}
	public void setBlood_group(String blood_group) {
		this.blood_group = blood_group;
	}
	public String getBlood_sugar() {
		return blood_sugar;
	}
	public void setBlood_sugar(String blood_sugar) {
		this.blood_sugar = blood_sugar;
	}
	public String getBooked() {
		return booked;
	}
	public void setBooked(String booked) {
		this.booked = booked;
	}
	public String getBpl_jsy_register() {
		return bpl_jsy_register;
	}
	public void setBpl_jsy_register(String bpl_jsy_register) {
		this.bpl_jsy_register = bpl_jsy_register;
	}
	public String getContraception_history() {
		return contraception_history;
	}
	public void setContraception_history(String contraception_history) {
		this.contraception_history = contraception_history;
	}
	public String getDistrictname() {
		return districtname;
	}
	public void setDistrictname(String districtname) {
		this.districtname = districtname;
	}
	public String getEdd() {
		return edd;
	}
	public void setEdd(String edd) {
		this.edd = edd;
	}
	public String getFacilityname() {
		return facilityname;
	}
	public void setFacilityname(String facilityname) {
		this.facilityname = facilityname;
	}
	public String getFacilitytypename() {
		return facilitytypename;
	}
	public void setFacilitytypename(String facilitytypename) {
		this.facilitytypename = facilitytypename;
	}
	public String getGa() {
		return ga;
	}
	public void setGa(String ga) {
		this.ga = ga;
	}
	public String getGravida() {
		return gravida;
	}
	public void setGravida(String gravida) {
		this.gravida = gravida;
	}
	public String getHb() {
		return hb;
	}
	public void setHb(String hb) {
		this.hb = hb;
	}
	public String getHiv() {
		return hiv;
	}
	public void setHiv(String hiv) {
		this.hiv = hiv;
	}
	public String getIs_active() {
		return is_active;
	}
	public void setIs_active(String is_active) {
		this.is_active = is_active;
	}
	public String getLiving_children() {
		return living_children;
	}
	public void setLiving_children(String living_children) {
		this.living_children = living_children;
	}
	public String getLmp() {
		return lmp;
	}
	public void setLmp(String lmp) {
		this.lmp = lmp;
	}
	public String getMalaria() {
		return malaria;
	}
	public void setMalaria(String malaria) {
		this.malaria = malaria;
	}
	public String getMcts_no() {
		return mcts_no;
	}
	public void setMcts_no(String mcts_no) {
		this.mcts_no = mcts_no;
	}
	public String getMedicalhoifany() {
		return medicalhoifany;
	}
	public void setMedicalhoifany(String medicalhoifany) {
		this.medicalhoifany = medicalhoifany;
	}
	public String getName_of_asha() {
		return name_of_asha;
	}
	public void setName_of_asha(String name_of_asha) {
		this.name_of_asha = name_of_asha;
	}
	public String getOthers1() {
		return others1;
	}
	public void setOthers1(String others1) {
		this.others1 = others1;
	}
	public String getPa_fundal_height() {
		return pa_fundal_height;
	}
	public void setPa_fundal_height(String pa_fundal_height) {
		this.pa_fundal_height = pa_fundal_height;
	}
	public String getPa_multiple_pregnancy() {
		return pa_multiple_pregnancy;
	}
	public void setPa_multiple_pregnancy(String pa_multiple_pregnancy) {
		this.pa_multiple_pregnancy = pa_multiple_pregnancy;
	}
	public String getPa_presenting_part() {
		return pa_presenting_part;
	}
	public void setPa_presenting_part(String pa_presenting_part) {
		this.pa_presenting_part = pa_presenting_part;
	}
	public String getParity() {
		return parity;
	}
	public void setParity(String parity) {
		this.parity = parity;
	}
	public String getPast_obstetrics_history() {
		return past_obstetrics_history;
	}
	public void setPast_obstetrics_history(String past_obstetrics_history) {
		this.past_obstetrics_history = past_obstetrics_history;
	}
	public String getPatient_name() {
		return patient_name;
	}
	public void setPatient_name(String patient_name) {
		this.patient_name = patient_name;
	}
	public String getPv_exam_antibiotic_given() {
		return pv_exam_antibiotic_given;
	}
	public void setPv_exam_antibiotic_given(String pv_exam_antibiotic_given) {
		this.pv_exam_antibiotic_given = pv_exam_antibiotic_given;
	}
	public String getPv_exam_cervical_dilation() {
		return pv_exam_cervical_dilation;
	}
	public void setPv_exam_cervical_dilation(String pv_exam_cervical_dilation) {
		this.pv_exam_cervical_dilation = pv_exam_cervical_dilation;
	}
	public String getPv_exam_cervical_effacement() {
		return pv_exam_cervical_effacement;
	}
	public void setPv_exam_cervical_effacement(String pv_exam_cervical_effacement) {
		this.pv_exam_cervical_effacement = pv_exam_cervical_effacement;
	}
	public String getPv_exam_membranes() {
		return pv_exam_membranes;
	}
	public void setPv_exam_membranes(String pv_exam_membranes) {
		this.pv_exam_membranes = pv_exam_membranes;
	}
	public String getStatename() {
		return statename;
	}
	public void setStatename(String statename) {
		this.statename = statename;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSyphilis() {
		return syphilis;
	}
	public void setSyphilis(String syphilis) {
		this.syphilis = syphilis;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getUnique_no() {
		return unique_no;
	}
	public void setUnique_no(String unique_no) {
		this.unique_no = unique_no;
	}
	public String getUrine_protien() {
		return urine_protien;
	}
	public void setUrine_protien(String urine_protien) {
		this.urine_protien = urine_protien;
	}
	public String getUrine_sugar() {
		return urine_sugar;
	}
	public void setUrine_sugar(String urine_sugar) {
		this.urine_sugar = urine_sugar;
	}
	public String getVitals_bp_diastolic() {
		return vitals_bp_diastolic;
	}
	public void setVitals_bp_diastolic(String vitals_bp_diastolic) {
		this.vitals_bp_diastolic = vitals_bp_diastolic;
	}
	public String getVitals_bp_systolic() {
		return vitals_bp_systolic;
	}
	public void setVitals_bp_systolic(String vitals_bp_systolic) {
		this.vitals_bp_systolic = vitals_bp_systolic;
	}
	public String getVitals_fhr() {
		return vitals_fhr;
	}
	public void setVitals_fhr(String vitals_fhr) {
		this.vitals_fhr = vitals_fhr;
	}
	public String getVitals_pulse() {
		return vitals_pulse;
	}
	public void setVitals_pulse(String vitals_pulse) {
		this.vitals_pulse = vitals_pulse;
	}
	public String getVitals_respiratory_rate() {
		return vitals_respiratory_rate;
	}
	public void setVitals_respiratory_rate(String vitals_respiratory_rate) {
		this.vitals_respiratory_rate = vitals_respiratory_rate;
	}
	public String getVitals_temparature() {
		return vitals_temparature;
	}
	public void setVitals_temparature(String vitals_temparature) {
		this.vitals_temparature = vitals_temparature;
	}
	public String getVitals_weight() {
		return vitals_weight;
	}
	public void setVitals_weight(String vitals_weight) {
		this.vitals_weight = vitals_weight;
	}
	public String getWo_do() {
		return wo_do;
	}
	public void setWo_do(String wo_do) {
		this.wo_do = wo_do;
	}
	public String getAdvice_of_baby() {
		return advice_of_baby;
	}
	public void setAdvice_of_baby(String advice_of_baby) {
		this.advice_of_baby = advice_of_baby;
	}
	public String getAdvice_of_mother() {
		return advice_of_mother;
	}
	public void setAdvice_of_mother(String advice_of_mother) {
		this.advice_of_mother = advice_of_mother;
	}
	public String getCondition_of_mother() {
		return condition_of_mother;
	}
	public void setCondition_of_mother(String condition_of_mother) {
		this.condition_of_mother = condition_of_mother;
	}
	public String getOthers() {
		return others;
	}
	public void setOthers(String others) {
		this.others = others;
	}
	
	
	
	
	
	
	
	
    
}   
    
    
    
    
    