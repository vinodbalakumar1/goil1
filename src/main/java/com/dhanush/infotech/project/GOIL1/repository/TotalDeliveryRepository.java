package com.dhanush.infotech.project.GOIL1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dhanush.infotech.project.GOIL1.model.TotalDelivery;


public interface TotalDeliveryRepository extends JpaRepository<TotalDelivery,Long>{
	@Query(value = "select sum(a.delivery) as delivery FROM TotalDelivery a where a.state = :state and a.createdat >= :start and a.createdat <= :end")
	Long findByState(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select sum(a.delivery) as delivery FROM TotalDelivery a where a.state = :state and a.district = :district and a.createdat >= :start and a.createdat <= :end")
	Long findByDistrict(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select sum(a.delivery) as delivery FROM TotalDelivery a where a.state = :state and a.district = :district and a.block = :block and a.createdat >= :start and a.createdat <= :end")
	Long findByBlock(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select sum(a.delivery) as delivery FROM TotalDelivery a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat >= :start and a.createdat <= :end")
	Long findByFacilitytype(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select sum(a.delivery) as delivery FROM TotalDelivery a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat >= :start and a.createdat <= :end")
	Long findByFacility(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);
	
	
	
	@Query(value = "select sum(a.delivery) as delivery FROM TotalDelivery a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat like %:date%")
	Long FacilityDashBoard(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("date") String date);
	
	@Query(value = "select sum(a.delivery) as delivery FROM TotalDelivery a where a.state = :state and a.createdat like %:date% ")
	Long FacilityDashBoardState(@Param("state") String state,@Param("date") String date);
	
}