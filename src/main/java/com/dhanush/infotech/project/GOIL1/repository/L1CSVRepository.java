package com.dhanush.infotech.project.GOIL1.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dhanush.infotech.project.GOIL1.model.L1CSVModel;
@Repository
public interface L1CSVRepository extends JpaRepository<L1CSVModel, Long>
{

	
	@Query(value="select a from L1CSVModel a where a.state = :state and a.admission_date like :date% ")
	List<L1CSVModel> findStateDetails(@Param("state")String state,@Param("date") String date);
	
	
	@Query(value="select a from L1CSVModel a where a.state = :state and a.district= :district and a.admission_date like :date% ")
	List<L1CSVModel> findDistrictDetails(@Param("state")String state,@Param("district")String district,@Param("date") String date);
	
	
	@Query(value=" select a from L1CSVModel a where a.state= :state and a.district = :district and a.block= :block and a.admission_date like :date% ")
	List<L1CSVModel> findBlockDetails(@Param("state")String state,@Param("district")String district,@Param("block")String block,@Param("date") String date);

	@Query(value="select a from L1CSVModel a where a.state = :state and a.district= :district and a.block= :block and a.facility_type= :facilityType and a.admission_date like :date% ")
	List<L1CSVModel> findFacilityTypeDetails(@Param("state")String state,@Param("district")String district,@Param("block")String block,@Param("facilityType")String facilityType,@Param("date") String date);
	
	@Query(value="select a from L1CSVModel a where a.state = :state and a.district= :district and a.block= :block and a.facility_type= :facilityType and a.facility= :facility and a.admission_date like :date% ")
	List<L1CSVModel> findFacilityDetails(@Param("state")String state,@Param("district")String district,@Param("block")String block,@Param("facilityType")String facilityType,@Param("facility")String facility,@Param("date") String date);
	


}
