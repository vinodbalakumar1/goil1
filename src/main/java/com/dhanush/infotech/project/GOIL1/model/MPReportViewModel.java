package com.dhanush.infotech.project.GOIL1.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="l1mpreportdeliveryoutcome")
public class MPReportViewModel implements Serializable
{
	private static final long serialVersionUID = 1L;
	@Id
	private Long id;
	@Column(name="state")
	public String state;
	@Column(name="district")
	public String district;
	@Column(name="block")
	public String block;
	@Column(name="facility")
	public String facility;
	@Column(name="facilitytype")
	public String facilitytype;
	@Column(name="admissiondate")
	public String admissiondate;
	@Column(name="deliveryoutcome1")
	public String deliveryoutcome1;
	@Column(name="preterm")
	public String preterm;
	@Column(name="resuscitated")
	public String resuscitated;
	@Column(name="breastfeedinginitiated")
	public String breastfeedinginitiated;
	@Column(name="birthweight")
	public String birthweight;
	
	
	
	
	
	public String getBirthweight() {
		return birthweight;
	}
	public void setBirthweight(String birthweight) {
		this.birthweight = birthweight;
	}
	public String getBreastfeedinginitiated() {
		return breastfeedinginitiated;
	}
	public void setBreastfeedinginitiated(String breastfeedinginitiated) {
		this.breastfeedinginitiated = breastfeedinginitiated;
	}
	public String getResuscitated() {
		return resuscitated;
	}
	public void setResuscitated(String resuscitated) {
		this.resuscitated = resuscitated;
	}
	public String getPreterm() {
		return preterm;
	}
	public void setPreterm(String preterm) {
		this.preterm = preterm;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}
	public String getFacility() {
		return facility;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	public String getFacilitytype() {
		return facilitytype;
	}
	public void setFacilitytype(String facilitytype) {
		this.facilitytype = facilitytype;
	}
	public String getAdmissiondate() {
		return admissiondate;
	}
	public void setAdmissiondate(String admissiondate) {
		this.admissiondate = admissiondate;
	}
	public String getDeliveryoutcome1() {
		return deliveryoutcome1;
	}
	public void setDeliveryoutcome1(String deliveryoutcome1) {
		this.deliveryoutcome1 = deliveryoutcome1;
	}
	
	
	
}
