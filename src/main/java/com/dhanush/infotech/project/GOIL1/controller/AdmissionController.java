package com.dhanush.infotech.project.GOIL1.controller;

import java.util.HashMap;
import java.util.List;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL1.exception.ResourceNotFoundException;
import com.dhanush.infotech.project.GOIL1.model.Admission;
import com.dhanush.infotech.project.GOIL1.model.LocationMasters;
import com.dhanush.infotech.project.GOIL1.repository.AdmissionRepository;
import com.dhanush.infotech.project.GOIL1.repository.LocationMastersRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class AdmissionController {

	/**
	 * Created by vinod on 23/07/18 13:23 - 22:10. // For insert admission data.
	
	 */
	@Autowired
	AdmissionRepository admissionRepository;


	
	
	
	
	@Autowired
	LocationMastersRepository locationmastersRepository;


	@CrossOrigin
	@RequestMapping(value = "/admission/l1", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "creating admission",
    notes = "Inserting Admission and Updating Admission.")
	@ApiParam(value = "name that need to be updated", required = true) 
	public HashMap<String, Object> createUser(@RequestBody String userJsonReq) throws NumberFormatException, Exception {
		HashMap<String, Object> userMap = new HashMap<>();
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String id = openJson.getString("admissionId");
		HashMap<String, Object> status = null;
		if(id.equalsIgnoreCase(""))
		{
			status = insertAdmission(userJsonReq);
		String statu = status.get("data") == "Beneficary Added successfull" ? "OK" :  "ERROR";
		userMap.put("status",statu);
		userMap.put("data", status.get("data"));
		userMap.put("uniqueId",status.get("unique_id"));
		userMap.put("admissionId", status.get("admissionId"));
		userMap.put("error", status.get("error"));
		}
		else 
		{
			status = updateAdmission(userJsonReq, Long.valueOf(id));
			String statu = status.get("data") == "Beneficary Updated Successfull" ? "OK" :  "ERROR";
			userMap.put("status",statu);
			userMap.put("data", status.get("data"));
			userMap.put("uniqueId",status.get("unique_id"));
			userMap.put("admissionId", status.get("admissionId"));
			userMap.put("error", status.get("error"));
		}
		return userMap;
	}

	public HashMap<String, Object> insertAdmission(String userJsonReq) throws JSONException {
		HashMap<String, Object> userMap = new HashMap<>();
		try {
			org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
			String state = openJson.getString("state");
			String district = openJson.getString("district");
			String block = openJson.getString("block");
			String facility = openJson.getString("facility");
			String facilityType = openJson.getString("facilityType");

			// Admission Object is Started

			org.json.JSONObject admission = openJson.getJSONObject("admission");
			String patientName = admission.getString("patientName");
			String age = admission.getString("age");
			String woDo = admission.getString("woDo");
			String mctsNo = admission.getString("mctsNo");
			String booked = admission.getString("booked");
			String bplJsyRegister = admission.getString("bplJsyRegister");
			
		//	String nameOfFacility = admission.getString("nameOfFacility");
			String nameOfAsha = admission.getString("nameOfAsha");
			
			String admissionDate = admission.getString("admissionDate");
			String time = admission.getString("time");
			String lmp = admission.getString("lmp");
			String edd = admission.getString("edd");
			String ga = admission.getString("ga");
			String antenatalSteroidsGiven = admission.getString("antenatalSteroidsGiven");
			String gravida = admission.getString("gravida");
			String parity = admission.getString("parity");
			String abortion = admission.getString("abortion");
			String livingChildren = admission.getString("livingChildren");
			String pastObstetricsHistory = admission.getString("pastObstetricsHistory");
			String contraceptionHistory = admission.getString("contraceptionHistory");
			String othersSpecify = admission.getString("othersSpecify");
			String bloodGroup = admission.getString("bloodGroup");
			String hb = admission.getString("hb");
			String urineProtein = admission.getString("urineProtein");
			String hiv = admission.getString("hiv");
			String VDRLRPR = admission.getString("VDRLRPR");
			String malaria = admission.getString("malaria"); 
			String antiGivenHb = admission.getString("antiGivenHb"); 
			String bloodSugar = admission.getString("bloodSugar"); 
			String urineSugar = admission.getString("urineSugar");
			String HBsAg = admission.getString("HBsAg");
			String syphilis = admission.getString("syphilis");
			String GDM = admission.getString("GDM");
			String OthersSpecify2 = admission.getString("OthersSpecify1");
			String address = admission.getString("presentho");
			String medicalHOifany = admission.getString("medicalHOifany");
			String uniqueNo = "L1" + System.currentTimeMillis();


			// Vitals

			org.json.JSONObject vitals = openJson.getJSONObject("vitals");
			String vitalsBpSystolic = vitals.getString("vitalsBpSystolic");
			String vitalsBpDiastolic = vitals.getString("vitalsBpDiastolic");
			String vitalsTemparature = vitals.getString("vitalsTemparature");
			String vitalsPulse = vitals.getString("vitalsPulse");
			String vitalsRespiratoryRate = vitals.getString("vitalsRespiratoryRate");
			String vitalsFhr = vitals.getString("vitalsFhr");
			String vitalsWeight = vitals.getString("vitalsWeight");

			// PAExamination

			org.json.JSONObject pAExamination = openJson.getJSONObject("pAExamination");
			String presentingPart = pAExamination.getString("presentingpart");
			String multiplePregnancy = pAExamination.getString("multiplepregnancy");
			String fundalHeight = pAExamination.getString("fundalheight");


			// PVExamination

			org.json.JSONObject setPvExamination = openJson.getJSONObject("pVExamination");
			String cervicalDilation = setPvExamination.getString("cervicalDilation");
			String cervicalEffacement = setPvExamination.getString("effacement");
			String membranes = setPvExamination.getString("membranes");
			String antibioticGiven = setPvExamination.getString("antibioticgiven");
			
			org.json.JSONObject others1 = openJson.getJSONObject("others");
			 String field1   =  others1.getString("field1");;
			 String field2   =  others1.getString("field2");
			 String field3   =  others1.getString("field3");
			 String field4   =  others1.getString("field4");
			 String field5   =  others1.getString("field5");
			 String field6   =  others1.getString("field6");
			 String field7   =  others1.getString("field7");
			 String field8   =  others1.getString("field8");
			 String field9   =  others1.getString("field9");
			 String field10  =  others1.getString("field10");
	
			
			
			
			String state_name="";
			String district_name="";
			String block_name="";
			String facility_name="";
			String facility_type_name="";
			
			try {
				
			   
			   
					List<LocationMasters> locations=null;
					   locations=locationmastersRepository.findByFacility(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),Integer.parseInt(facility),facilityType);
						for (LocationMasters locationMasters: locations)
						{
							 state_name=locationMasters.getState();
							 district_name=locationMasters.getDistrictCode();
							 block_name=locationMasters.getBlock();
							 facility_name=locationMasters.getFacility();
							 facility_type_name=locationMasters.getFacilityType();
							 
							
						}
						}catch (Exception e) {
							userMap.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
							userMap.put("data", e.getMessage());
							userMap.put("admissionId","");
							userMap.put("unique_id", "");
							userMap.put("baby", "");
							return userMap;
						}
			

			// Admission Setters

			Admission setAdmission = new Admission();
			setAdmission.setUniqueNo(uniqueNo);
			setAdmission.setState(state);
			setAdmission.setDistrict(district);
			setAdmission.setBlock(block);
			setAdmission.setFacility(facility);
			setAdmission.setFacilityType(facilityType);
			setAdmission.setStatename(state_name);
			setAdmission.setDistrictname(district_name);
			setAdmission.setFacilityname(facility_name);
			setAdmission.setBlockname(block_name);
			setAdmission.setFacilitytypename(facility_type_name);
			setAdmission.setPatientName(patientName);
			setAdmission.setAge(age);
			setAdmission.setWoDo(woDo);
			setAdmission.setMctsNo(mctsNo);
			setAdmission.setBooked(booked);
			setAdmission.setBplJsyRegister(bplJsyRegister);
		
			setAdmission.setNameOfAsha(nameOfAsha);
			setAdmission.setPatientName(patientName);
			
			setAdmission.setAdmissionDate(admissionDate);
			setAdmission.setTime(time);
			setAdmission.setLmp(lmp);
			setAdmission.setEdd(edd);
			setAdmission.setGa(ga);
			setAdmission.setAntenatalSteroidsGiven(antenatalSteroidsGiven);
			setAdmission.setGravida(gravida);
			setAdmission.setParity(parity);
			setAdmission.setAbortion(abortion);
			setAdmission.setLivingChildren(livingChildren);
			setAdmission.setPastObstetricsHistory(pastObstetricsHistory);
			setAdmission.setContraceptionHistory(contraceptionHistory);
			setAdmission.setBloodGroup(bloodGroup);
			setAdmission.setOthers1(othersSpecify);
			setAdmission.setBloodSugar(bloodSugar);
			setAdmission.setHb(hb);
			setAdmission.setUrineProtien(urineProtein);
			setAdmission.setHiv(hiv);
			setAdmission.setVDRLRPR(VDRLRPR);
			setAdmission.setMalaria(malaria);
			setAdmission.setAntiGivenHb(antiGivenHb);
			setAdmission.setBloodSugar(bloodSugar);
			setAdmission.setUrineSugar(urineSugar);
			setAdmission.setHBsAg(HBsAg);
			setAdmission.setSyphilis(syphilis);
			setAdmission.setGDM(GDM);
			setAdmission.setMedicalHOifany(medicalHOifany);
			setAdmission.setOthersSpecify2(OthersSpecify2);
			setAdmission.setAddress(address);
			setAdmission.setField1(field1);
			setAdmission.setField2(field2);
			setAdmission.setField3(field3);
			setAdmission.setField4(field4);
			setAdmission.setField5(field5);
			setAdmission.setField6(field6);
			setAdmission.setField7(field7);
			setAdmission.setField8(field8);
			setAdmission.setField9(field9);
			setAdmission.setField10(field10);
			
			setAdmission.setStatus(108);
			// GeneralExamination setters


			// Vitals setters

			setAdmission.setVitalsBpDiastolic(vitalsBpDiastolic);
			setAdmission.setVitalsBpSystolic(vitalsBpSystolic);
			setAdmission.setVitalsFhr(vitalsFhr);
			setAdmission.setVitalsPulse(vitalsPulse);
			setAdmission.setVitalsRespiratoryRate(vitalsRespiratoryRate);
			setAdmission.setVitalsTemparature(vitalsTemparature);
			setAdmission.setVitalsWeight(vitalsWeight);

			// PAExamination setters

			setAdmission.setPaFundalHeight(fundalHeight);
			setAdmission.setPaMultiplePregnancy(multiplePregnancy);
			setAdmission.setPaPresentingPart(presentingPart);
			// PVExamination setters

			setAdmission.setPvExamCervicalDilation(cervicalDilation);
			setAdmission.setPvExamCervicalEffacement(cervicalEffacement);
			setAdmission.setPvExamMembranes(membranes);
			setAdmission.setPvExamAntibioticGiven(antibioticGiven);
			


			// one to one relation
			
			setAdmission.setCompleteness("30%");
			admissionRepository.save(setAdmission); 
			// saving admission data in admission table and generate uniqu id
			// inserting data
			

			userMap.put("status", HttpStatus.OK);
			userMap.put("data", "Beneficary Added successfull");
			userMap.put("admissionId",setAdmission.getId().toString());
			userMap.put("unique_id",uniqueNo);
	
			
		} catch (Exception e) {
			userMap.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			userMap.put("data", "Add Beneficary failure");
			userMap.put("unique_id", "failed");
			userMap.put("admissionId","");
			userMap.put("baby","fail");
				userMap.put("error",e.getMessage());
		}
		return userMap;
	}
	
	
	public  HashMap<String, Object> updateAdmission(String userJsonReq,long id) throws Exception {
		HashMap<String, Object> userMap = new HashMap<>();
		   
		try {
			org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
			String state = openJson.getString("state");
			String district = openJson.getString("district");
			String block = openJson.getString("block");
			String facility = openJson.getString("facility");
			String facilityType = openJson.getString("facilityType");
			
			
			org.json.JSONObject admission = openJson.getJSONObject("admission");
			String patientName = admission.getString("patientName");
			String age = admission.getString("age");
			String woDo = admission.getString("woDo");
			String mctsNo = admission.getString("mctsNo");
			String booked = admission.getString("booked");
			String bplJsyRegister = admission.getString("bplJsyRegister");
			
		//	String nameOfFacility = admission.getString("nameOfFacility");
			String nameOfAsha = admission.getString("nameOfAsha");
			
			String admissionDate = admission.getString("admissionDate");
			String time = admission.getString("time");
			String lmp = admission.getString("lmp");
			String edd = admission.getString("edd");
			String ga = admission.getString("ga");
			String antenatalSteroidsGiven = admission.getString("antenatalSteroidsGiven");
			String gravida = admission.getString("gravida");
			String parity = admission.getString("parity");
			String abortion = admission.getString("abortion");
			String livingChildren = admission.getString("livingChildren");
			String pastObstetricsHistory = admission.getString("pastObstetricsHistory");
			String contraceptionHistory = admission.getString("contraceptionHistory");
			String othersSpecify = admission.getString("othersSpecify");
			String bloodGroup = admission.getString("bloodGroup");
			String hb = admission.getString("hb");
			String urineProtein = admission.getString("urineProtein");
			String hiv = admission.getString("hiv");
			String VDRLRPR = admission.getString("VDRLRPR");
			String malaria = admission.getString("malaria"); 
			String antiGivenHb = admission.getString("antiGivenHb"); 
			String bloodSugar = admission.getString("bloodSugar"); 
			String urineSugar = admission.getString("urineSugar");
			String HBsAg = admission.getString("HBsAg");
			String syphilis = admission.getString("syphilis");
			String GDM = admission.getString("GDM");
			String OthersSpecify2 = admission.getString("OthersSpecify1");
			String address = admission.getString("presentho");
			String medicalHOifany = admission.getString("medicalHOifany");

			// Vitals

			org.json.JSONObject vitals = openJson.getJSONObject("vitals");
			String vitalsBpSystolic = vitals.getString("vitalsBpSystolic");
			String vitalsBpDiastolic = vitals.getString("vitalsBpDiastolic");
			String vitalsTemparature = vitals.getString("vitalsTemparature");
			String vitalsPulse = vitals.getString("vitalsPulse");
			String vitalsRespiratoryRate = vitals.getString("vitalsRespiratoryRate");
			String vitalsFhr = vitals.getString("vitalsFhr");
			String vitalsWeight = vitals.getString("vitalsWeight");

			// PAExamination

			org.json.JSONObject pAExamination = openJson.getJSONObject("pAExamination");
			String presentingPart = pAExamination.getString("presentingpart");
			String multiplePregnancy = pAExamination.getString("multiplepregnancy");
			String fundalHeight = pAExamination.getString("fundalheight");

			
			org.json.JSONObject others1 = openJson.getJSONObject("others");
			 String field1   =  others1.getString("field1");;
			 String field2   =  others1.getString("field2");
			 String field3   =  others1.getString("field3");
			 String field4   =  others1.getString("field4");
			 String field5   =  others1.getString("field5");
			 String field6   =  others1.getString("field6");
			 String field7   =  others1.getString("field7");
			 String field8   =  others1.getString("field8");
			 String field9   =  others1.getString("field9");
			 String field10  =  others1.getString("field10");

			// PVExamination

			org.json.JSONObject setPvExamination = openJson.getJSONObject("pVExamination");
			String cervicalDilation = setPvExamination.getString("cervicalDilation");
			String cervicalEffacement = setPvExamination.getString("effacement");
			String membranes = setPvExamination.getString("membranes");
			String antibioticGiven = setPvExamination.getString("antibioticgiven");			
			
			
			@SuppressWarnings("unused")
			String state_name="";
			@SuppressWarnings("unused")
			String district_name="";
			@SuppressWarnings("unused")
			String block_name="";
			@SuppressWarnings("unused")
			String facility_name="";
			@SuppressWarnings("unused")
			String facility_type_name="";
			
			try {
				
			   
			   
					List<LocationMasters> locations=null;
					   locations=locationmastersRepository.findByFacility(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),Integer.parseInt(facility),facilityType);
						for (LocationMasters locationMasters : locations)
						{
							 state_name=locationMasters.getState();
							 district_name=locationMasters.getDistrictCode();
							 block_name=locationMasters.getBlock();
							 facility_name=locationMasters.getFacility();
							 facility_type_name=locationMasters.getFacilityType();
							 
							
							
						}
						}catch (Exception e) {
							userMap.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
							userMap.put("data", e.getMessage());
							userMap.put("admissionId","");
							userMap.put("unique_id", "");
							userMap.put("baby", "");
							return userMap;
						}
			
			

			// Admission Setters
			
			Admission setAdmission =  admissionRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Admission", "NOT FOUND", id));

			setAdmission.setState(state);
			setAdmission.setDistrict(district);
			setAdmission.setBlock(block);
			setAdmission.setFacility(facility);
			setAdmission.setFacilityType(facilityType);
			setAdmission.setStatename(state_name);
			setAdmission.setDistrictname(district_name);
			setAdmission.setFacilityname(facility_name);
			setAdmission.setBlockname(block_name);
			setAdmission.setFacilitytypename(facility_type_name);
			setAdmission.setPatientName(patientName);
			setAdmission.setAge(age);
			setAdmission.setWoDo(woDo);
			setAdmission.setMctsNo(mctsNo);
			setAdmission.setBooked(booked);
			setAdmission.setBplJsyRegister(bplJsyRegister);
			setAdmission.setAddress(address);
			setAdmission.setNameOfAsha(nameOfAsha);
			setAdmission.setPatientName(patientName);
			setAdmission.setBloodGroup(bloodGroup);
			setAdmission.setAdmissionDate(admissionDate);
			setAdmission.setTime(time);
			setAdmission.setLmp(lmp);
			setAdmission.setEdd(edd);
			setAdmission.setGa(ga);
			setAdmission.setAntenatalSteroidsGiven(antenatalSteroidsGiven);
			setAdmission.setGravida(gravida);
			setAdmission.setParity(parity);
			setAdmission.setAbortion(abortion);
			setAdmission.setLivingChildren(livingChildren);
			setAdmission.setPastObstetricsHistory(pastObstetricsHistory);
			setAdmission.setContraceptionHistory(contraceptionHistory);
			setAdmission.setOthers1(othersSpecify);
			setAdmission.setBloodSugar(bloodSugar);
			setAdmission.setHb(hb);
			setAdmission.setUrineProtien(urineProtein);
			setAdmission.setHiv(hiv);
			setAdmission.setVDRLRPR(VDRLRPR);
			setAdmission.setMalaria(malaria);
			setAdmission.setAntiGivenHb(antiGivenHb);
			setAdmission.setBloodSugar(bloodSugar);
			setAdmission.setUrineSugar(urineSugar);
			setAdmission.setHBsAg(HBsAg);
			setAdmission.setSyphilis(syphilis);
			setAdmission.setGDM(GDM);
			setAdmission.setMedicalHOifany(medicalHOifany);
			setAdmission.setOthersSpecify2(OthersSpecify2);
			setAdmission.setGDM(GDM);
			
			
			
			setAdmission.setField1(field1);
			setAdmission.setField2(field2);
			setAdmission.setField3(field3);
			setAdmission.setField4(field4);
			setAdmission.setField5(field5);
			setAdmission.setField6(field6);
			setAdmission.setField7(field7);
			setAdmission.setField8(field8);
			setAdmission.setField9(field9);
			setAdmission.setField10(field10);
			
			setAdmission.setStatus(108);
			// GeneralExamination setters


			// Vitals setters

			setAdmission.setVitalsBpDiastolic(vitalsBpDiastolic);
			setAdmission.setVitalsBpSystolic(vitalsBpSystolic);
			setAdmission.setVitalsFhr(vitalsFhr);
			setAdmission.setVitalsPulse(vitalsPulse);
			setAdmission.setVitalsRespiratoryRate(vitalsRespiratoryRate);
			setAdmission.setVitalsTemparature(vitalsTemparature);
			setAdmission.setVitalsWeight(vitalsWeight);
			// PAExamination setters

			setAdmission.setPaFundalHeight(fundalHeight);
			setAdmission.setPaMultiplePregnancy(multiplePregnancy);
			setAdmission.setPaPresentingPart(presentingPart);
			// PVExamination setters

			setAdmission.setPvExamCervicalDilation(cervicalDilation);
			setAdmission.setPvExamCervicalEffacement(cervicalEffacement);
			setAdmission.setPvExamMembranes(membranes);
			setAdmission.setPvExamAntibioticGiven(antibioticGiven);
			

			admissionRepository.save(setAdmission); // saving admission data in admission table and generate uniqu id

			// list child 

			/// one to one relation

			// inserting data
			

			userMap.put("status", HttpStatus.OK);
			userMap.put("data", "Beneficary Updated Successfull");
			userMap.put("admissionId",setAdmission.getId().toString());
			userMap.put("unique_id", setAdmission.getUniqueNo());
		}
		catch(Exception e)
		{
			userMap.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			userMap.put("data", "Add Beneficary failure");
			userMap.put("unique_id", "failed");
			userMap.put("baby","fail");
			userMap.put("error",e.getMessage());
		}
		return userMap;
	}

}
