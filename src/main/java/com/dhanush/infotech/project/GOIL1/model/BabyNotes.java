package com.dhanush.infotech.project.GOIL1.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.dhanush.infotech.project.GOIL1.model.Admission;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name = "l1_babynotes")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)

public class BabyNotes {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt  = new Date();;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt  = new Date();;

    public Admission getAdmission() {
		return admission;
	}
	public void setAdmission(Admission admission) {
		this.admission = admission;
	}
	@ManyToOne
    @JoinColumn(name = "babynotesid")
    private Admission admission;

	                
	 private String injVitaminKAdministered;
	 private String ifYesDose;
	 private String vaccinationDone;
	 private String conditionOfBaby;
	 private String detailsOf3rd4thStageOfLabor;
	 private String anyOtherDrugSpecify;
	 private String cctWard;
	 private String utMassageWard;
     private String placentaCompleteWard;
  
	private String episiotomyGivenWard;
     private String ppiucdInsertedWard;
     private String complicationsDuringDeliveryWard;
     private String dateOfTransferToPncWard;
     private String timeOfTransferToPncWard;
     private String babyReferredWard;
     private String ifYesgiveReasonWard;
     private String uniqueId;
	 private String babyId;
     
	   public String getBabyId() {
			return babyId;
		}
		public void setBabyId(String babyId) {
			this.babyId = babyId;
		}
     
     
	 public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getInjVitaminKAdministered() {
		return injVitaminKAdministered;
	}
	public void setInjVitaminKAdministered(String injVitaminKAdministered) {
		this.injVitaminKAdministered = injVitaminKAdministered;
	}
	public String getIfYesDose() {
		return ifYesDose;
	}
	public void setIfYesDose(String ifYesDose) {
		this.ifYesDose = ifYesDose;
	}
	public String getVaccinationDone() {
		return vaccinationDone;
	}
	public void setVaccinationDone(String vaccinationDone) {
		this.vaccinationDone = vaccinationDone;
	}
	public String getConditionOfBaby() {
		return conditionOfBaby;
	}
	public void setConditionOfBaby(String conditionOfBaby) {
		this.conditionOfBaby = conditionOfBaby;
	}
	public String getDetailsOf3rd4thStageOfLabor() {
		return detailsOf3rd4thStageOfLabor;
	}
	public void setDetailsOf3rd4thStageOfLabor(String detailsOf3rd4thStageOfLabor) {
		this.detailsOf3rd4thStageOfLabor = detailsOf3rd4thStageOfLabor;
	}
	public String getAnyOtherDrugSpecify() {
		return anyOtherDrugSpecify;
	}
	public void setAnyOtherDrugSpecify(String anyOtherDrugSpecify) {
		this.anyOtherDrugSpecify = anyOtherDrugSpecify;
	}
	public String getCctWard() {
		return cctWard;
	}
	public void setCctWard(String cctWard) {
		this.cctWard = cctWard;
	}
	public String getUtMassageWard() {
		return utMassageWard;
	}
	public void setUtMassageWard(String utMassageWard) {
		this.utMassageWard = utMassageWard;
	}
	public String getPlacentaCompleteWard() {
		return placentaCompleteWard;
	}
	public void setPlacentaCompleteWard(String placentaCompleteWard) {
		this.placentaCompleteWard = placentaCompleteWard;
	}
	public String getEpisiotomyGivenWard() {
		return episiotomyGivenWard;
	}
	public void setEpisiotomyGivenWard(String episiotomyGivenWard) {
		this.episiotomyGivenWard = episiotomyGivenWard;
	}
	public String getPpiucdInsertedWard() {
		return ppiucdInsertedWard;
	}
	public void setPpiucdInsertedWard(String ppiucdInsertedWard) {
		this.ppiucdInsertedWard = ppiucdInsertedWard;
	}
	public String getComplicationsDuringDeliveryWard() {
		return complicationsDuringDeliveryWard;
	}
	public void setComplicationsDuringDeliveryWard(String complicationsDuringDeliveryWard) {
		this.complicationsDuringDeliveryWard = complicationsDuringDeliveryWard;
	}
	public String getDateOfTransferToPncWard() {
		return dateOfTransferToPncWard;
	}
	public void setDateOfTransferToPncWard(String dateOfTransferToPncWard) {
		this.dateOfTransferToPncWard = dateOfTransferToPncWard;
	}
	public String getTimeOfTransferToPncWard() {
		return timeOfTransferToPncWard;
	}
	public void setTimeOfTransferToPncWard(String timeOfTransferToPncWard) {
		this.timeOfTransferToPncWard = timeOfTransferToPncWard;
	}
	public String getBabyReferredWard() {
		return babyReferredWard;
	}
	public void setBabyReferredWard(String babyReferredWard) {
		this.babyReferredWard = babyReferredWard;
	}
	public String getIfYesgiveReasonWard() {
		return ifYesgiveReasonWard;
	}
	public void setIfYesgiveReasonWard(String ifYesgiveReasonWard) {
		this.ifYesgiveReasonWard = ifYesgiveReasonWard;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	
	

}
