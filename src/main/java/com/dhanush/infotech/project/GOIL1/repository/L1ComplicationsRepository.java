package com.dhanush.infotech.project.GOIL1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dhanush.infotech.project.GOIL1.model.l1Complications;

public interface L1ComplicationsRepository extends JpaRepository<l1Complications, Long> {
	 
	@Query(value = "select count(1) as count FROM l1Complications a where a.state = :state and a.createdat > :start and a.createdat < :end and (a.deliverycomplications like '%96%'  or a.deliverycomplications like '%97%') ")
	Long findByState5(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM l1Complications a where a.state = :state and a.district = :district and a.createdat > :start and a.createdat < :end and (a.deliverycomplications like '%96%' or  a.deliverycomplications like '%97%') ")
	Long findByDistrict5(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(1) as count FROM l1Complications a where a.state = :state and a.district = :district and a.block = :block and a.createdat > :start and a.createdat < :end and (a.deliverycomplications like '%96%' or a.deliverycomplications like '%97%') ")
	Long findByBlock5(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM l1Complications a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat > :start and a.createdat < :end and (a.deliverycomplications like '%96%' or a.deliverycomplications like '%97%') ")
	Long findByFacilitytype5(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(1) as count FROM l1Complications a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat > :start and a.createdat < :end and (a.deliverycomplications like '%96%' or a.deliverycomplications like '%97%') ")
	Long findByFacility5(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);

	
	
	
	
	@Query(value = "select count(1) as count FROM l1Complications a where a.state = :state and a.createdat > :start and a.createdat < :end and a.deliverycomplications like '%100%' ")
	Long findByState6(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM l1Complications a where a.state = :state and a.district = :district and a.createdat > :start and a.createdat < :end and a.deliverycomplications like '%100%' ")
	Long findByDistrict6(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(1) as count FROM l1Complications a where a.state = :state and a.district = :district and a.block = :block and a.createdat > :start and a.createdat < :end and a.deliverycomplications like '%100%' ")
	Long findByBlock6(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM l1Complications a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat > :start and a.createdat < :end and a.deliverycomplications like '%100%' ")
	Long findByFacilitytype6(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(1) as count FROM l1Complications a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat > :start and a.createdat < :end and a.deliverycomplications like '%100%'")
	Long findByFacility6(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM l1Complications a where a.state = :state and a.createdat like %:date% and (a.deliverycomplications like '%96%' or a.deliverycomplications like '%97%') ")
	Long FacilityDashBoardState(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select  count(1) as count FROM l1Complications a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat  like :date% and (a.deliverycomplications like '%96%' or a.deliverycomplications like '%97%') ")
	Long FacilityDashBoardFility(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("date") String start);
	
	
	// ecl
	
	
	@Query(value = "select count(1) as count,a.district,a.districtname FROM l1Complications a where a.state = :state and a.createdat like :date% and (a.deliverycomplications like '%96%' or a.deliverycomplications like '%97%') group by a.district,a.districtname")
	List<Object[]> objByState5(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(1) as count,a.district,a.districtname,a.facility,a.facilityname FROM l1Complications a where a.state = :state and a.district = :district and a.createdat like :date% and (a.deliverycomplications like '%96%' or a.deliverycomplications like '%97%')  group by a.district,a.districtname,a.facility,a.facilityname")
	List<Object[]> objByDistrict5(@Param("state") String state,@Param("district") String district,@Param("date") String date);
	
	
	
	
	
	
	@Query(value = "select count(1) as count,a.district,a.districtname FROM l1Complications a where a.state = :state and a.createdat like :date% and a.deliverycomplications like '%100%' group by a.district,a.districtname")
	List<Object[]> objByState6(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(1) as count,a.district,a.districtname,a.facility,a.facilityname FROM l1Complications a where a.state = :state and a.district = :district and a.createdat like :date% and a.deliverycomplications like '%100%' group by a.district,a.districtname,a.facility,a.facilityname")
	List<Object[]> objByDistrict6(@Param("state") String state,@Param("district") String district,@Param("date") String date);

	

}
