package com.dhanush.infotech.project.GOIL1.controller;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL1.model.Admission;
import com.dhanush.infotech.project.GOIL1.model.BabyNotes;
import com.dhanush.infotech.project.GOIL1.repository.AdmissionRepository;
import com.dhanush.infotech.project.GOIL1.repository.AssessmentOfPostPartumConditionRepository;
import com.dhanush.infotech.project.GOIL1.repository.BabyNotesRepository;
import com.dhanush.infotech.project.GOIL1.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api")
public class GetBeneficarySearchController {

	@Autowired
	AdmissionRepository admissionRepository;
	
	@Autowired
	BabyNotesRepository babyNotesRepository;

	@Autowired
	AssessmentOfPostPartumConditionRepository assessmentOfPostPartumConditionRepository;
	
	
	@CrossOrigin
	@RequestMapping(value = "/getbeneficarydetailsbyid/l1", method = RequestMethod.POST, produces = { "application/json" })
	public String getUserById(@RequestBody String userJsonReq) {
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String uniqueNo = openJson.getString("uniqueId");
		String mctsno = openJson.getString("mctsno");
		
		org.json.JSONArray jo1 = new org.json.JSONArray();
		if(uniqueNo.length() > 0) {
		List<Admission> admission = admissionRepository.findByCaseId(uniqueNo);
		for(Admission admission11 : admission) {
			org.json.JSONObject jo = new org.json.JSONObject();
	    	jo.put("nameOfFacility", admission11.getFacilityname());
	    	jo.put("caseNo", admission11.getUniqueNo());
	    	jo.put("nameOfWomen", admission11.getPatientName());
	    	jo.put("district", admission11.getDistrictname());
	    	jo.put("completness", admission11.getCompleteness());
	    	jo.put("admission_time", admission11.getTime());
	    	jo.put("admission_date", admission11.getAdmissionDate());
	    	jo.put("id", admission11.getId());
	    	jo.put("mctsno", admission11.getMctsNo());
	    	jo.put("status", admission11.getStatus());
	    	jo1.put(jo);
		}
		}
		else {
			List<Admission> admission = admissionRepository.findByMCTSNO(mctsno);
			for(Admission admission11 : admission) {
				org.json.JSONObject jo = new org.json.JSONObject();
		    	jo.put("nameOfFacility", admission11.getFacilityname());
		    	jo.put("caseNo", admission11.getUniqueNo());
		    	jo.put("nameOfWomen", admission11.getPatientName());
		    	jo.put("district", admission11.getDistrictname());
		    	jo.put("completness", admission11.getCompleteness());
		    	jo.put("admission_time", admission11.getTime());
		    	jo.put("admission_date", admission11.getAdmissionDate());
		    	jo.put("id", admission11.getId());
		    	jo.put("mctsno", admission11.getMctsNo());
		    	jo.put("status", admission11.getStatus());
		    	jo1.put(jo);
			}
			}
		
		return jo1.toString();
	}
	
	
	
	@CrossOrigin
	@RequestMapping(value = "/getbeneficarydetailsbyname/l1", method = RequestMethod.POST, produces = { "application/json" })
	public String getBeneficaryBy(@RequestBody String userJsonReq) {
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String beneficary = openJson.getString("beneficary");
		String mobileno = openJson.getString("mobileno");
		String state = openJson.getString("state");
		String district = openJson.getString("district");
		String block = openJson.getString("block");
		String facilityType = openJson.getString("facilityType");
		String facility = openJson.getString("facility");
		
		org.json.JSONArray jo1 = new org.json.JSONArray();
		List<Admission> admission = null;
		if(mobileno.isEmpty())
		{
		if(!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL") && facilityType.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))
		{
			admission = admissionRepository.findByCaseNameState(beneficary,state);
			
		}
		if(!state.equalsIgnoreCase("ALL")&& !district.equalsIgnoreCase("ALL")&& block.equalsIgnoreCase("ALL") && facilityType.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))
		{
			admission = admissionRepository.findByCaseNameDistrict(beneficary,state,district);

		}
		if(!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")&& facilityType.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))
		{
			admission = admissionRepository.findByCaseNameBlock(beneficary,state,district,block);

		}
		if(!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL")&& !block.equalsIgnoreCase("ALL")  && !facilityType.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))
		{
			admission = admissionRepository.findByCaseNameFacilityType(beneficary,state,district,block,facilityType);

		}
		if(!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")  && !facilityType.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL"))
		{
			admission = admissionRepository.findByCaseNameFacility(beneficary,state,district,block,facilityType,facility);

		}
		for(Admission admission11 : admission) {
			org.json.JSONObject jo = new org.json.JSONObject();
	    	jo.put("nameOfFacility", admission11.getFacilityname());
	    	jo.put("caseNo", admission11.getUniqueNo());
	    	jo.put("nameOfWomen", admission11.getPatientName());
	    	jo.put("district", admission11.getDistrictname());
	    	jo.put("completness", admission11.getCompleteness());
	    	jo.put("admission_time", admission11.getTime());
	    	jo.put("admission_date", admission11.getAdmissionDate());
	    	jo.put("mctsno", admission11.getMctsNo());
	    	jo.put("id", admission11.getId());
	    	jo.put("status", admission11.getStatus());
	    	jo1.put(jo);
		}
		}
		
		return jo1.toString();
	}
	
	@CrossOrigin
	@RequestMapping(value = "/getbabynotes/bybeneficaryid/l1", method = RequestMethod.POST, produces = { "application/json" })
	public String getChildIdByUniqueID(@RequestBody String userJsonReq) {
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String uniqueNo = openJson.getString("uniqueId");
		List<BabyNotes> babynote = babyNotesRepository.findByCaseId(uniqueNo);
		org.json.JSONArray arr = new org.json.JSONArray();
		for(BabyNotes babyNotes : babynote) {
			org.json.JSONObject jo = new org.json.JSONObject();
	    	jo.put("cid",""+babyNotes.getId() );
	    	jo.put("child_id", babyNotes.getBabyId());
	    	arr.put(jo);
		}
		return arr.toString();
		
	}
	

	
	@CrossOrigin
	@RequestMapping(value = "/updatestatus", method = RequestMethod.POST, produces = { "application/json" })
	public String updateStatus(@RequestBody String userJsonReq) {
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		Long id = openJson.getLong("id");
		int status =openJson.getInt("status");
		Admission setAdmission =  admissionRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Admission", "NOT FOUND", id));
		setAdmission.setStatus(status);
		admissionRepository.save(setAdmission);
		JSONObject obj = new JSONObject();
		obj.put("message", "updated successfully");
		return obj.toString(); 
	}
	

	
	
}
	

