package com.dhanush.infotech.project.GOIL1.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.dhanush.infotech.project.GOIL1.model.Admission;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name = "l1_assmentpostpartumcondition")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class AssessmentOfPostPartumCondition {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt  = new Date();;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt  = new Date();
    

    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	  @OneToOne(fetch = FetchType.LAZY)
	    @JoinColumn(name = "admissionId", nullable = false)
	    private Admission admission;
	  
	  
	public Admission getAdmission() {
		return admission;
	}
	public void setAdmission(com.dhanush.infotech.project.GOIL1.model.Admission setAdmission1) {
		this.admission = setAdmission1;
	}
	
private String 	MotherBPDay30min1;
private String  MotherBPDay30min2;
private String  MotherBPDay30min3;
private String  MotherBPDay30min4;
private String  MotherBPDay6hrs5;
 private String   MotherBPDay6hrs6;
 private String   MotherBPDay6hrs7;
 private String   MotherBPDay2morning;
 private String   MotherBPDay2evening;
 private String   MothertempDay30min1;
private String   MothertempDay30min2;
private String   MothertempDay30min3;
private String   MothertempDay30min4;
private String    MothertempDay6hrs5;
private String    MothertempDay6hrs6;
private String  MothertempDay6hrs7;
private String  MothertempDay2morning;
private String  MothertempDay2evening;
private String  MotherpulseDay30min1;
private String  MotherpulseDay30min2;
 private String   MotherpulseDay30min3;
 private String   MotherpulseDay30min4;
 private String   MotherpulseDay6hrs5;
 private String   MotherpulseDay6hrs6;
 private String   MotherpulseDay6hrs7;
private String  MotherpulseDay2morning;
private String   MotherpulseDay2evening;
private String   MotherBreastconditionDay30min1;
private String   MotherBreastconditionDay30min2;
private String   MotherBreastconditionDay30min3;
private String  MotherBreastconditionDay30min4;
private String    MotherBreastconditionDay6hrs5;
private String    MotherBreastconditionDay6hrs6;
private String    MotherBreastconditionDay6hrs7;
private String    MotherBreastconditionDay2morning;
private String   MotherBreastconditionDay2evening;
private String   MotherBleedingPVDay30min1;
private String    MotherBleedingPVDay30min2;
private String    MotherBleedingPVDay30min3;
private String    MotherBleedingPVDay30min4;
 private String  MotherBleedingPVDay6hrs5;
 private String  MotherBleedingPVDay6hrs6;
 private String  MotherBleedingPVDay6hrs7;
 private String  MotherBleedingPVDay2morning;
 private String  MotherBleedingPVDay2evening;
 private String   MotherUterineToneDay30min1;
 private String   MotherUterineToneDay30min2;
 private String   MotherUterineToneDay30min3;
 private String   MotherUterineToneDay30min4;
 private String   MotherUterineToneDay6hrs5;
private String  MotherUterineToneDay6hrs6;
private String   MotherUterineToneDay6hrs7;
private String   MotherUterineToneDay2morning;
private String   MotherUterineToneDay2evening;
private String   MotherEpisiotomyTearDay30min1;
 private String  MotherEpisiotomyTearDay30min2;
 private String    MotherEpisiotomyTearDay30min3;
 private String    MotherEpisiotomyTearDay30min4;
 private String    MotherEpisiotomyTearDay6hrs5;
 private String    MotherEpisiotomyTearDay6hrs6;
 private String    MotherEpisiotomyTearDay6hrs7;
 private String    MotherEpisiotomyTearDay2morning;
 private String    MotherEpisiotomyTearDay2evening;
 private String    BabyResprateDay30min1;
 private String    BabyResprateDay30min2;
 private String  BabyResprateDay30min3;
 private String  BabyResprateDay30min4;
 private String  BabyResprateDay6hrs5;
 private String  BabyResprateDay6hrs6;
 private String  BabyResprateDay6hrs7;
private String BabyResprateDay2morning;
private String   BabyResprateDay2evening;
private String   BabytempDay30min1;
private String   BabytempDay30min2;
private String   BabytempDay30min3;
 private String  BabytempDay30min4;
 private String    BabytempDay6hrs5;
 private String    BabytempDay6hrs6;
 private String    BabytempDay6hrs7;
 private String    BabytempDay2morning;
private String   BabytempDay2evening;
private String   BabyBreastfeedingDay30min1;
private String   BabyBreastfeedingDay30min2;
private String   BabyBreastfeedingDay30min3;
private String   BabyBreastfeeding30min4;
private String  BabyBreastfeedingDay6hrs5;
private String  BabyBreastfeedingDay6hrs6;
private String  BabyBreastfeedingDay6hrs7;
private String  BabyBreastfeedingDay2morning;
private String  BabyBreastfeedingDay2evening;
private String  BabyActivityDay30min1;
private String BabyActivityDay30min2;
 private String  BabyActivityDay30min3;
 private String  BabyActivityDay30min4;
 private String  BabyActivityDay6hrs5;
 private String  BabyActivityDay6hrs6;
 private String  BabyActivityDay6hrs7;
 private String  BabyActivityDay2morning;
 private String   BabyActivityDay2evening;
  private String   BabyUmbilicalstumpDay30min1;
  private String   BabyUmbilicalstumpDay30min2;
  private String   BabyUmbilicalstumpDay30min3;
  private String   BabyUmbilicalstumpDay30min4;
  private String   BabyUmbilicalstumpDay6hrs5;
  private String    BabyUmbilicalstumpDay6hrs6;
  private String    BabyUmbilicalstumpDay6hrs7;
  private String    BabyUmbilicalstumpDay2morning;
  private String    BabyUmbilicalstumpDay2evening;
  private String    BabyJaundiceDay30min1;
  private String    BabyJaundiceDay30min2;
   private String   BabyJaundiceDay30min3;
   private String   BabyJaundiceDay30min4;
   private String   BabyJaundiceDay6hrs5;
   private String   BabyJaundiceDay6hrs6;
   private String   BabyJaundiceDay6hrs7;
 private String   BabyJaundiceDay2morning;
 private String   BabyJaundiceDay2evening;
 private String   BabyPassedurineDay30min1;
 private String   BabyPassedurineDay30min2;
 private String   BabyPassedurineDay30min3;
 private String   BabyPassedurineDay30min4;
  private String  BabyPassedurineDay6hrs5;
  private String  BabyPassedurineDay6hrs6;
  private String  BabyPassedurineDay6hrs7;
  private String  BabyPassedurineDay2morning;
  private String  BabyPassedurineDay2evening;
 private String   BabyPassedstoolDay30min1;
 private String   BabyPassedstoolDay30min2;
 private String   BabyPassedstoolDay30min3;
 private String     BabyPassedstoolDay30min4;
 private String     BabyPassedstoolDay6hrs5; 
 private String     BabyPassedstoolDay6hrs6; 
  private String    BabyPassedstoolDay6hrs7;
  private String    BabyPassedstoolDay2morning;
  private String    BabyPassedstoolDay2evening;

public String getMotherBPDay30min1() {
	return MotherBPDay30min1;
}
public void setMotherBPDay30min1(String motherBPDay30min1) {
	MotherBPDay30min1 = motherBPDay30min1;
}
public String getMotherBPDay30min2() {
	return MotherBPDay30min2;
}
public void setMotherBPDay30min2(String motherBPDay30min2) {
	MotherBPDay30min2 = motherBPDay30min2;
}
public String getMotherBPDay30min3() {
	return MotherBPDay30min3;
}
public void setMotherBPDay30min3(String motherBPDay30min3) {
	MotherBPDay30min3 = motherBPDay30min3;
}
public String getMotherBPDay30min4() {
	return MotherBPDay30min4;
}
public void setMotherBPDay30min4(String motherBPDay30min4) {
	MotherBPDay30min4 = motherBPDay30min4;
}
public String getMotherBPDay6hrs5() {
	return MotherBPDay6hrs5;
}
public void setMotherBPDay6hrs5(String motherBPDay6hrs5) {
	MotherBPDay6hrs5 = motherBPDay6hrs5;
}
public String getMotherBPDay6hrs6() {
	return MotherBPDay6hrs6;
}
public void setMotherBPDay6hrs6(String motherBPDay6hrs6) {
	MotherBPDay6hrs6 = motherBPDay6hrs6;
}
public String getMotherBPDay6hrs7() {
	return MotherBPDay6hrs7;
}
public void setMotherBPDay6hrs7(String motherBPDay6hrs7) {
	MotherBPDay6hrs7 = motherBPDay6hrs7;
}
public String getMotherBPDay2morning() {
	return MotherBPDay2morning;
}
public void setMotherBPDay2morning(String motherBPDay2morning) {
	MotherBPDay2morning = motherBPDay2morning;
}
public String getMotherBPDay2evening() {
	return MotherBPDay2evening;
}
public void setMotherBPDay2evening(String motherBPDay2evening) {
	MotherBPDay2evening = motherBPDay2evening;
}
public String getMothertempDay30min1() {
	return MothertempDay30min1;
}
public void setMothertempDay30min1(String mothertempDay30min1) {
	MothertempDay30min1 = mothertempDay30min1;
}
public String getMothertempDay30min2() {
	return MothertempDay30min2;
}
public void setMothertempDay30min2(String mothertempDay30min2) {
	MothertempDay30min2 = mothertempDay30min2;
}
public String getMothertempDay30min3() {
	return MothertempDay30min3;
}
public void setMothertempDay30min3(String mothertempDay30min3) {
	MothertempDay30min3 = mothertempDay30min3;
}
public String getMothertempDay30min4() {
	return MothertempDay30min4;
}
public void setMothertempDay30min4(String mothertempDay30min4) {
	MothertempDay30min4 = mothertempDay30min4;
}
public String getMothertempDay6hrs5() {
	return MothertempDay6hrs5;
}
public void setMothertempDay6hrs5(String mothertempDay6hrs5) {
	MothertempDay6hrs5 = mothertempDay6hrs5;
}
public String getMothertempDay6hrs6() {
	return MothertempDay6hrs6;
}
public void setMothertempDay6hrs6(String mothertempDay6hrs6) {
	MothertempDay6hrs6 = mothertempDay6hrs6;
}
public String getMothertempDay6hrs7() {
	return MothertempDay6hrs7;
}
public void setMothertempDay6hrs7(String mothertempDay6hrs7) {
	MothertempDay6hrs7 = mothertempDay6hrs7;
}
public String getMothertempDay2morning() {
	return MothertempDay2morning;
}
public void setMothertempDay2morning(String mothertempDay2morning) {
	MothertempDay2morning = mothertempDay2morning;
}
public String getMothertempDay2evening() {
	return MothertempDay2evening;
}
public void setMothertempDay2evening(String mothertempDay2evening) {
	MothertempDay2evening = mothertempDay2evening;
}
public String getMotherpulseDay30min1() {
	return MotherpulseDay30min1;
}
public void setMotherpulseDay30min1(String motherpulseDay30min1) {
	MotherpulseDay30min1 = motherpulseDay30min1;
}
public String getMotherpulseDay30min2() {
	return MotherpulseDay30min2;
}
public void setMotherpulseDay30min2(String motherpulseDay30min2) {
	MotherpulseDay30min2 = motherpulseDay30min2;
}
public String getMotherpulseDay30min3() {
	return MotherpulseDay30min3;
}
public void setMotherpulseDay30min3(String motherpulseDay30min3) {
	MotherpulseDay30min3 = motherpulseDay30min3;
}
public String getMotherpulseDay30min4() {
	return MotherpulseDay30min4;
}
public void setMotherpulseDay30min4(String motherpulseDay30min4) {
	MotherpulseDay30min4 = motherpulseDay30min4;
}
public String getMotherpulseDay6hrs5() {
	return MotherpulseDay6hrs5;
}
public void setMotherpulseDay6hrs5(String motherpulseDay6hrs5) {
	MotherpulseDay6hrs5 = motherpulseDay6hrs5;
}
public String getMotherpulseDay6hrs6() {
	return MotherpulseDay6hrs6;
}
public void setMotherpulseDay6hrs6(String motherpulseDay6hrs6) {
	MotherpulseDay6hrs6 = motherpulseDay6hrs6;
}
public String getMotherpulseDay6hrs7() {
	return MotherpulseDay6hrs7;
}
public void setMotherpulseDay6hrs7(String motherpulseDay6hrs7) {
	MotherpulseDay6hrs7 = motherpulseDay6hrs7;
}
public String getMotherpulseDay2morning() {
	return MotherpulseDay2morning;
}
public void setMotherpulseDay2morning(String motherpulseDay2morning) {
	MotherpulseDay2morning = motherpulseDay2morning;
}
public String getMotherpulseDay2evening() {
	return MotherpulseDay2evening;
}
public void setMotherpulseDay2evening(String motherpulseDay2evening) {
	MotherpulseDay2evening = motherpulseDay2evening;
}
public String getMotherBreastconditionDay30min1() {
	return MotherBreastconditionDay30min1;
}
public void setMotherBreastconditionDay30min1(String motherBreastconditionDay30min1) {
	MotherBreastconditionDay30min1 = motherBreastconditionDay30min1;
}
public String getMotherBreastconditionDay30min2() {
	return MotherBreastconditionDay30min2;
}
public void setMotherBreastconditionDay30min2(String motherBreastconditionDay30min2) {
	MotherBreastconditionDay30min2 = motherBreastconditionDay30min2;
}
public String getMotherBreastconditionDay30min3() {
	return MotherBreastconditionDay30min3;
}
public void setMotherBreastconditionDay30min3(String motherBreastconditionDay30min3) {
	MotherBreastconditionDay30min3 = motherBreastconditionDay30min3;
}
public String getMotherBreastconditionDay30min4() {
	return MotherBreastconditionDay30min4;
}
public void setMotherBreastconditionDay30min4(String motherBreastconditionDay30min4) {
	MotherBreastconditionDay30min4 = motherBreastconditionDay30min4;
}
public String getMotherBreastconditionDay6hrs5() {
	return MotherBreastconditionDay6hrs5;
}
public void setMotherBreastconditionDay6hrs5(String motherBreastconditionDay6hrs5) {
	MotherBreastconditionDay6hrs5 = motherBreastconditionDay6hrs5;
}
public String getMotherBreastconditionDay6hrs6() {
	return MotherBreastconditionDay6hrs6;
}
public void setMotherBreastconditionDay6hrs6(String motherBreastconditionDay6hrs6) {
	MotherBreastconditionDay6hrs6 = motherBreastconditionDay6hrs6;
}
public String getMotherBreastconditionDay6hrs7() {
	return MotherBreastconditionDay6hrs7;
}
public void setMotherBreastconditionDay6hrs7(String motherBreastconditionDay6hrs7) {
	MotherBreastconditionDay6hrs7 = motherBreastconditionDay6hrs7;
}
public String getMotherBreastconditionDay2morning() {
	return MotherBreastconditionDay2morning;
}
public void setMotherBreastconditionDay2morning(String motherBreastconditionDay2morning) {
	MotherBreastconditionDay2morning = motherBreastconditionDay2morning;
}
public String getMotherBreastconditionDay2evening() {
	return MotherBreastconditionDay2evening;
}
public void setMotherBreastconditionDay2evening(String motherBreastconditionDay2evening) {
	MotherBreastconditionDay2evening = motherBreastconditionDay2evening;
}
public String getMotherBleedingPVDay30min1() {
	return MotherBleedingPVDay30min1;
}
public void setMotherBleedingPVDay30min1(String motherBleedingPVDay30min1) {
	MotherBleedingPVDay30min1 = motherBleedingPVDay30min1;
}
public String getMotherBleedingPVDay30min2() {
	return MotherBleedingPVDay30min2;
}
public void setMotherBleedingPVDay30min2(String motherBleedingPVDay30min2) {
	MotherBleedingPVDay30min2 = motherBleedingPVDay30min2;
}
public String getMotherBleedingPVDay30min3() {
	return MotherBleedingPVDay30min3;
}
public void setMotherBleedingPVDay30min3(String motherBleedingPVDay30min3) {
	MotherBleedingPVDay30min3 = motherBleedingPVDay30min3;
}
public String getMotherBleedingPVDay30min4() {
	return MotherBleedingPVDay30min4;
}
public void setMotherBleedingPVDay30min4(String motherBleedingPVDay30min4) {
	MotherBleedingPVDay30min4 = motherBleedingPVDay30min4;
}
public String getMotherBleedingPVDay6hrs5() {
	return MotherBleedingPVDay6hrs5;
}
public void setMotherBleedingPVDay6hrs5(String motherBleedingPVDay6hrs5) {
	MotherBleedingPVDay6hrs5 = motherBleedingPVDay6hrs5;
}
public String getMotherBleedingPVDay6hrs6() {
	return MotherBleedingPVDay6hrs6;
}
public void setMotherBleedingPVDay6hrs6(String motherBleedingPVDay6hrs6) {
	MotherBleedingPVDay6hrs6 = motherBleedingPVDay6hrs6;
}
public String getMotherBleedingPVDay6hrs7() {
	return MotherBleedingPVDay6hrs7;
}
public void setMotherBleedingPVDay6hrs7(String motherBleedingPVDay6hrs7) {
	MotherBleedingPVDay6hrs7 = motherBleedingPVDay6hrs7;
}
public String getMotherBleedingPVDay2morning() {
	return MotherBleedingPVDay2morning;
}
public void setMotherBleedingPVDay2morning(String motherBleedingPVDay2morning) {
	MotherBleedingPVDay2morning = motherBleedingPVDay2morning;
}
public String getMotherBleedingPVDay2evening() {
	return MotherBleedingPVDay2evening;
}
public void setMotherBleedingPVDay2evening(String motherBleedingPVDay2evening) {
	MotherBleedingPVDay2evening = motherBleedingPVDay2evening;
}
public String getMotherUterineToneDay30min1() {
	return MotherUterineToneDay30min1;
}
public void setMotherUterineToneDay30min1(String motherUterineToneDay30min1) {
	MotherUterineToneDay30min1 = motherUterineToneDay30min1;
}
public String getMotherUterineToneDay30min2() {
	return MotherUterineToneDay30min2;
}
public void setMotherUterineToneDay30min2(String motherUterineToneDay30min2) {
	MotherUterineToneDay30min2 = motherUterineToneDay30min2;
}
public String getMotherUterineToneDay30min3() {
	return MotherUterineToneDay30min3;
}
public void setMotherUterineToneDay30min3(String motherUterineToneDay30min3) {
	MotherUterineToneDay30min3 = motherUterineToneDay30min3;
}
public String getMotherUterineToneDay30min4() {
	return MotherUterineToneDay30min4;
}
public void setMotherUterineToneDay30min4(String motherUterineToneDay30min4) {
	MotherUterineToneDay30min4 = motherUterineToneDay30min4;
}
public String getMotherUterineToneDay6hrs5() {
	return MotherUterineToneDay6hrs5;
}
public void setMotherUterineToneDay6hrs5(String motherUterineToneDay6hrs5) {
	MotherUterineToneDay6hrs5 = motherUterineToneDay6hrs5;
}
public String getMotherUterineToneDay6hrs6() {
	return MotherUterineToneDay6hrs6;
}
public void setMotherUterineToneDay6hrs6(String motherUterineToneDay6hrs6) {
	MotherUterineToneDay6hrs6 = motherUterineToneDay6hrs6;
}
public String getMotherUterineToneDay6hrs7() {
	return MotherUterineToneDay6hrs7;
}
public void setMotherUterineToneDay6hrs7(String motherUterineToneDay6hrs7) {
	MotherUterineToneDay6hrs7 = motherUterineToneDay6hrs7;
}
public String getMotherUterineToneDay2morning() {
	return MotherUterineToneDay2morning;
}
public void setMotherUterineToneDay2morning(String motherUterineToneDay2morning) {
	MotherUterineToneDay2morning = motherUterineToneDay2morning;
}
public String getMotherUterineToneDay2evening() {
	return MotherUterineToneDay2evening;
}
public void setMotherUterineToneDay2evening(String motherUterineToneDay2evening) {
	MotherUterineToneDay2evening = motherUterineToneDay2evening;
}
public String getMotherEpisiotomyTearDay30min1() {
	return MotherEpisiotomyTearDay30min1;
}
public void setMotherEpisiotomyTearDay30min1(String motherEpisiotomyTearDay30min1) {
	MotherEpisiotomyTearDay30min1 = motherEpisiotomyTearDay30min1;
}
public String getMotherEpisiotomyTearDay30min2() {
	return MotherEpisiotomyTearDay30min2;
}
public void setMotherEpisiotomyTearDay30min2(String motherEpisiotomyTearDay30min2) {
	MotherEpisiotomyTearDay30min2 = motherEpisiotomyTearDay30min2;
}
public String getMotherEpisiotomyTearDay30min3() {
	return MotherEpisiotomyTearDay30min3;
}
public void setMotherEpisiotomyTearDay30min3(String motherEpisiotomyTearDay30min3) {
	MotherEpisiotomyTearDay30min3 = motherEpisiotomyTearDay30min3;
}
public String getMotherEpisiotomyTearDay30min4() {
	return MotherEpisiotomyTearDay30min4;
}
public void setMotherEpisiotomyTearDay30min4(String motherEpisiotomyTearDay30min4) {
	MotherEpisiotomyTearDay30min4 = motherEpisiotomyTearDay30min4;
}
public String getMotherEpisiotomyTearDay6hrs5() {
	return MotherEpisiotomyTearDay6hrs5;
}
public void setMotherEpisiotomyTearDay6hrs5(String motherEpisiotomyTearDay6hrs5) {
	MotherEpisiotomyTearDay6hrs5 = motherEpisiotomyTearDay6hrs5;
}
public String getMotherEpisiotomyTearDay6hrs6() {
	return MotherEpisiotomyTearDay6hrs6;
}
public void setMotherEpisiotomyTearDay6hrs6(String motherEpisiotomyTearDay6hrs6) {
	MotherEpisiotomyTearDay6hrs6 = motherEpisiotomyTearDay6hrs6;
}
public String getMotherEpisiotomyTearDay6hrs7() {
	return MotherEpisiotomyTearDay6hrs7;
}
public void setMotherEpisiotomyTearDay6hrs7(String motherEpisiotomyTearDay6hrs7) {
	MotherEpisiotomyTearDay6hrs7 = motherEpisiotomyTearDay6hrs7;
}
public String getMotherEpisiotomyTearDay2morning() {
	return MotherEpisiotomyTearDay2morning;
}
public void setMotherEpisiotomyTearDay2morning(String motherEpisiotomyTearDay2morning) {
	MotherEpisiotomyTearDay2morning = motherEpisiotomyTearDay2morning;
}
public String getMotherEpisiotomyTearDay2evening() {
	return MotherEpisiotomyTearDay2evening;
}
public void setMotherEpisiotomyTearDay2evening(String motherEpisiotomyTearDay2evening) {
	MotherEpisiotomyTearDay2evening = motherEpisiotomyTearDay2evening;
}
public String getBabyResprateDay30min1() {
	return BabyResprateDay30min1;
}
public void setBabyResprateDay30min1(String babyResprateDay30min1) {
	BabyResprateDay30min1 = babyResprateDay30min1;
}
public String getBabyResprateDay30min2() {
	return BabyResprateDay30min2;
}
public void setBabyResprateDay30min2(String babyResprateDay30min2) {
	BabyResprateDay30min2 = babyResprateDay30min2;
}
public String getBabyResprateDay30min3() {
	return BabyResprateDay30min3;
}
public void setBabyResprateDay30min3(String babyResprateDay30min3) {
	BabyResprateDay30min3 = babyResprateDay30min3;
}
public String getBabyResprateDay30min4() {
	return BabyResprateDay30min4;
}
public void setBabyResprateDay30min4(String babyResprateDay30min4) {
	BabyResprateDay30min4 = babyResprateDay30min4;
}
public String getBabyResprateDay6hrs5() {
	return BabyResprateDay6hrs5;
}
public void setBabyResprateDay6hrs5(String babyResprateDay6hrs5) {
	BabyResprateDay6hrs5 = babyResprateDay6hrs5;
}
public String getBabyResprateDay6hrs6() {
	return BabyResprateDay6hrs6;
}
public void setBabyResprateDay6hrs6(String babyResprateDay6hrs6) {
	BabyResprateDay6hrs6 = babyResprateDay6hrs6;
}
public String getBabyResprateDay6hrs7() {
	return BabyResprateDay6hrs7;
}
public void setBabyResprateDay6hrs7(String babyResprateDay6hrs7) {
	BabyResprateDay6hrs7 = babyResprateDay6hrs7;
}
public String getBabyResprateDay2morning() {
	return BabyResprateDay2morning;
}
public void setBabyResprateDay2morning(String babyResprateDay2morning) {
	BabyResprateDay2morning = babyResprateDay2morning;
}
public String getBabyResprateDay2evening() {
	return BabyResprateDay2evening;
}
public void setBabyResprateDay2evening(String babyResprateDay2evening) {
	BabyResprateDay2evening = babyResprateDay2evening;
}
public String getBabytempDay30min1() {
	return BabytempDay30min1;
}
public void setBabytempDay30min1(String babytempDay30min1) {
	BabytempDay30min1 = babytempDay30min1;
}
public String getBabytempDay30min2() {
	return BabytempDay30min2;
}
public void setBabytempDay30min2(String babytempDay30min2) {
	BabytempDay30min2 = babytempDay30min2;
}
public String getBabytempDay30min3() {
	return BabytempDay30min3;
}
public void setBabytempDay30min3(String babytempDay30min3) {
	BabytempDay30min3 = babytempDay30min3;
}
public String getBabytempDay30min4() {
	return BabytempDay30min4;
}
public void setBabytempDay30min4(String babytempDay30min4) {
	BabytempDay30min4 = babytempDay30min4;
}
public String getBabytempDay6hrs5() {
	return BabytempDay6hrs5;
}
public void setBabytempDay6hrs5(String babytempDay6hrs5) {
	BabytempDay6hrs5 = babytempDay6hrs5;
}
public String getBabytempDay6hrs6() {
	return BabytempDay6hrs6;
}
public void setBabytempDay6hrs6(String babytempDay6hrs6) {
	BabytempDay6hrs6 = babytempDay6hrs6;
}
public String getBabytempDay6hrs7() {
	return BabytempDay6hrs7;
}
public void setBabytempDay6hrs7(String babytempDay6hrs7) {
	BabytempDay6hrs7 = babytempDay6hrs7;
}
public String getBabytempDay2morning() {
	return BabytempDay2morning;
}
public void setBabytempDay2morning(String babytempDay2morning) {
	BabytempDay2morning = babytempDay2morning;
}
public String getBabytempDay2evening() {
	return BabytempDay2evening;
}
public void setBabytempDay2evening(String babytempDay2evening) {
	BabytempDay2evening = babytempDay2evening;
}
public String getBabyBreastfeedingDay30min1() {
	return BabyBreastfeedingDay30min1;
}
public void setBabyBreastfeedingDay30min1(String babyBreastfeedingDay30min1) {
	BabyBreastfeedingDay30min1 = babyBreastfeedingDay30min1;
}
public String getBabyBreastfeedingDay30min2() {
	return BabyBreastfeedingDay30min2;
}
public void setBabyBreastfeedingDay30min2(String babyBreastfeedingDay30min2) {
	BabyBreastfeedingDay30min2 = babyBreastfeedingDay30min2;
}
public String getBabyBreastfeedingDay30min3() {
	return BabyBreastfeedingDay30min3;
}
public void setBabyBreastfeedingDay30min3(String babyBreastfeedingDay30min3) {
	BabyBreastfeedingDay30min3 = babyBreastfeedingDay30min3;
}
public String getBabyBreastfeeding30min4() {
	return BabyBreastfeeding30min4;
}
public void setBabyBreastfeeding30min4(String babyBreastfeeding30min4) {
	BabyBreastfeeding30min4 = babyBreastfeeding30min4;
}
public String getBabyBreastfeedingDay6hrs5() {
	return BabyBreastfeedingDay6hrs5;
}
public void setBabyBreastfeedingDay6hrs5(String babyBreastfeedingDay6hrs5) {
	BabyBreastfeedingDay6hrs5 = babyBreastfeedingDay6hrs5;
}
public String getBabyBreastfeedingDay6hrs6() {
	return BabyBreastfeedingDay6hrs6;
}
public void setBabyBreastfeedingDay6hrs6(String babyBreastfeedingDay6hrs6) {
	BabyBreastfeedingDay6hrs6 = babyBreastfeedingDay6hrs6;
}
public String getBabyBreastfeedingDay6hrs7() {
	return BabyBreastfeedingDay6hrs7;
}
public void setBabyBreastfeedingDay6hrs7(String babyBreastfeedingDay6hrs7) {
	BabyBreastfeedingDay6hrs7 = babyBreastfeedingDay6hrs7;
}
public String getBabyBreastfeedingDay2morning() {
	return BabyBreastfeedingDay2morning;
}
public void setBabyBreastfeedingDay2morning(String babyBreastfeedingDay2morning) {
	BabyBreastfeedingDay2morning = babyBreastfeedingDay2morning;
}
public String getBabyBreastfeedingDay2evening() {
	return BabyBreastfeedingDay2evening;
}
public void setBabyBreastfeedingDay2evening(String babyBreastfeedingDay2evening) {
	BabyBreastfeedingDay2evening = babyBreastfeedingDay2evening;
}
public String getBabyActivityDay30min1() {
	return BabyActivityDay30min1;
}
public void setBabyActivityDay30min1(String babyActivityDay30min1) {
	BabyActivityDay30min1 = babyActivityDay30min1;
}
public String getBabyActivityDay30min2() {
	return BabyActivityDay30min2;
}
public void setBabyActivityDay30min2(String babyActivityDay30min2) {
	BabyActivityDay30min2 = babyActivityDay30min2;
}
public String getBabyActivityDay30min3() {
	return BabyActivityDay30min3;
}
public void setBabyActivityDay30min3(String babyActivityDay30min3) {
	BabyActivityDay30min3 = babyActivityDay30min3;
}
public String getBabyActivityDay30min4() {
	return BabyActivityDay30min4;
}
public void setBabyActivityDay30min4(String babyActivityDay30min4) {
	BabyActivityDay30min4 = babyActivityDay30min4;
}
public String getBabyActivityDay6hrs5() {
	return BabyActivityDay6hrs5;
}
public void setBabyActivityDay6hrs5(String babyActivityDay6hrs5) {
	BabyActivityDay6hrs5 = babyActivityDay6hrs5;
}
public String getBabyActivityDay6hrs6() {
	return BabyActivityDay6hrs6;
}
public void setBabyActivityDay6hrs6(String babyActivityDay6hrs6) {
	BabyActivityDay6hrs6 = babyActivityDay6hrs6;
}
public String getBabyActivityDay6hrs7() {
	return BabyActivityDay6hrs7;
}
public void setBabyActivityDay6hrs7(String babyActivityDay6hrs7) {
	BabyActivityDay6hrs7 = babyActivityDay6hrs7;
}
public String getBabyActivityDay2morning() {
	return BabyActivityDay2morning;
}
public void setBabyActivityDay2morning(String babyActivityDay2morning) {
	BabyActivityDay2morning = babyActivityDay2morning;
}
public String getBabyActivityDay2evening() {
	return BabyActivityDay2evening;
}
public void setBabyActivityDay2evening(String babyActivityDay2evening) {
	BabyActivityDay2evening = babyActivityDay2evening;
}
public String getBabyUmbilicalstumpDay30min1() {
	return BabyUmbilicalstumpDay30min1;
}
public void setBabyUmbilicalstumpDay30min1(String babyUmbilicalstumpDay30min1) {
	BabyUmbilicalstumpDay30min1 = babyUmbilicalstumpDay30min1;
}
public String getBabyUmbilicalstumpDay30min2() {
	return BabyUmbilicalstumpDay30min2;
}
public void setBabyUmbilicalstumpDay30min2(String babyUmbilicalstumpDay30min2) {
	BabyUmbilicalstumpDay30min2 = babyUmbilicalstumpDay30min2;
}
public String getBabyUmbilicalstumpDay30min3() {
	return BabyUmbilicalstumpDay30min3;
}
public void setBabyUmbilicalstumpDay30min3(String babyUmbilicalstumpDay30min3) {
	BabyUmbilicalstumpDay30min3 = babyUmbilicalstumpDay30min3;
}
public String getBabyUmbilicalstumpDay30min4() {
	return BabyUmbilicalstumpDay30min4;
}
public void setBabyUmbilicalstumpDay30min4(String babyUmbilicalstumpDay30min4) {
	BabyUmbilicalstumpDay30min4 = babyUmbilicalstumpDay30min4;
}
public String getBabyUmbilicalstumpDay6hrs5() {
	return BabyUmbilicalstumpDay6hrs5;
}
public void setBabyUmbilicalstumpDay6hrs5(String babyUmbilicalstumpDay6hrs5) {
	BabyUmbilicalstumpDay6hrs5 = babyUmbilicalstumpDay6hrs5;
}
public String getBabyUmbilicalstumpDay6hrs6() {
	return BabyUmbilicalstumpDay6hrs6;
}
public void setBabyUmbilicalstumpDay6hrs6(String babyUmbilicalstumpDay6hrs6) {
	BabyUmbilicalstumpDay6hrs6 = babyUmbilicalstumpDay6hrs6;
}
public String getBabyUmbilicalstumpDay6hrs7() {
	return BabyUmbilicalstumpDay6hrs7;
}
public void setBabyUmbilicalstumpDay6hrs7(String babyUmbilicalstumpDay6hrs7) {
	BabyUmbilicalstumpDay6hrs7 = babyUmbilicalstumpDay6hrs7;
}
public String getBabyUmbilicalstumpDay2morning() {
	return BabyUmbilicalstumpDay2morning;
}
public void setBabyUmbilicalstumpDay2morning(String babyUmbilicalstumpDay2morning) {
	BabyUmbilicalstumpDay2morning = babyUmbilicalstumpDay2morning;
}
public String getBabyUmbilicalstumpDay2evening() {
	return BabyUmbilicalstumpDay2evening;
}
public void setBabyUmbilicalstumpDay2evening(String babyUmbilicalstumpDay2evening) {
	BabyUmbilicalstumpDay2evening = babyUmbilicalstumpDay2evening;
}
public String getBabyJaundiceDay30min1() {
	return BabyJaundiceDay30min1;
}
public void setBabyJaundiceDay30min1(String babyJaundiceDay30min1) {
	BabyJaundiceDay30min1 = babyJaundiceDay30min1;
}
public String getBabyJaundiceDay30min2() {
	return BabyJaundiceDay30min2;
}
public void setBabyJaundiceDay30min2(String babyJaundiceDay30min2) {
	BabyJaundiceDay30min2 = babyJaundiceDay30min2;
}
public String getBabyJaundiceDay30min3() {
	return BabyJaundiceDay30min3;
}
public void setBabyJaundiceDay30min3(String babyJaundiceDay30min3) {
	BabyJaundiceDay30min3 = babyJaundiceDay30min3;
}
public String getBabyJaundiceDay30min4() {
	return BabyJaundiceDay30min4;
}
public void setBabyJaundiceDay30min4(String babyJaundiceDay30min4) {
	BabyJaundiceDay30min4 = babyJaundiceDay30min4;
}
public String getBabyJaundiceDay6hrs5() {
	return BabyJaundiceDay6hrs5;
}
public void setBabyJaundiceDay6hrs5(String babyJaundiceDay6hrs5) {
	BabyJaundiceDay6hrs5 = babyJaundiceDay6hrs5;
}
public String getBabyJaundiceDay6hrs6() {
	return BabyJaundiceDay6hrs6;
}
public void setBabyJaundiceDay6hrs6(String babyJaundiceDay6hrs6) {
	BabyJaundiceDay6hrs6 = babyJaundiceDay6hrs6;
}
public String getBabyJaundiceDay6hrs7() {
	return BabyJaundiceDay6hrs7;
}
public void setBabyJaundiceDay6hrs7(String babyJaundiceDay6hrs7) {
	BabyJaundiceDay6hrs7 = babyJaundiceDay6hrs7;
}
public String getBabyJaundiceDay2morning() {
	return BabyJaundiceDay2morning;
}
public void setBabyJaundiceDay2morning(String babyJaundiceDay2morning) {
	BabyJaundiceDay2morning = babyJaundiceDay2morning;
}
public String getBabyJaundiceDay2evening() {
	return BabyJaundiceDay2evening;
}
public void setBabyJaundiceDay2evening(String babyJaundiceDay2evening) {
	BabyJaundiceDay2evening = babyJaundiceDay2evening;
}
public String getBabyPassedurineDay30min1() {
	return BabyPassedurineDay30min1;
}
public void setBabyPassedurineDay30min1(String babyPassedurineDay30min1) {
	BabyPassedurineDay30min1 = babyPassedurineDay30min1;
}
public String getBabyPassedurineDay30min2() {
	return BabyPassedurineDay30min2;
}
public void setBabyPassedurineDay30min2(String babyPassedurineDay30min2) {
	BabyPassedurineDay30min2 = babyPassedurineDay30min2;
}
public String getBabyPassedurineDay30min3() {
	return BabyPassedurineDay30min3;
}
public void setBabyPassedurineDay30min3(String babyPassedurineDay30min3) {
	BabyPassedurineDay30min3 = babyPassedurineDay30min3;
}
public String getBabyPassedurineDay30min4() {
	return BabyPassedurineDay30min4;
}
public void setBabyPassedurineDay30min4(String babyPassedurineDay30min4) {
	BabyPassedurineDay30min4 = babyPassedurineDay30min4;
}
public String getBabyPassedurineDay6hrs5() {
	return BabyPassedurineDay6hrs5;
}
public void setBabyPassedurineDay6hrs5(String babyPassedurineDay6hrs5) {
	BabyPassedurineDay6hrs5 = babyPassedurineDay6hrs5;
}
public String getBabyPassedurineDay6hrs6() {
	return BabyPassedurineDay6hrs6;
}
public void setBabyPassedurineDay6hrs6(String babyPassedurineDay6hrs6) {
	BabyPassedurineDay6hrs6 = babyPassedurineDay6hrs6;
}
public String getBabyPassedurineDay6hrs7() {
	return BabyPassedurineDay6hrs7;
}
public void setBabyPassedurineDay6hrs7(String babyPassedurineDay6hrs7) {
	BabyPassedurineDay6hrs7 = babyPassedurineDay6hrs7;
}
public String getBabyPassedurineDay2morning() {
	return BabyPassedurineDay2morning;
}
public void setBabyPassedurineDay2morning(String babyPassedurineDay2morning) {
	BabyPassedurineDay2morning = babyPassedurineDay2morning;
}
public String getBabyPassedurineDay2evening() {
	return BabyPassedurineDay2evening;
}
public void setBabyPassedurineDay2evening(String babyPassedurineDay2evening) {
	BabyPassedurineDay2evening = babyPassedurineDay2evening;
}
public String getBabyPassedstoolDay30min1() {
	return BabyPassedstoolDay30min1;
}
public void setBabyPassedstoolDay30min1(String babyPassedstoolDay30min1) {
	BabyPassedstoolDay30min1 = babyPassedstoolDay30min1;
}
public String getBabyPassedstoolDay30min2() {
	return BabyPassedstoolDay30min2;
}
public void setBabyPassedstoolDay30min2(String babyPassedstoolDay30min2) {
	BabyPassedstoolDay30min2 = babyPassedstoolDay30min2;
}
public String getBabyPassedstoolDay30min3() {
	return BabyPassedstoolDay30min3;
}
public void setBabyPassedstoolDay30min3(String babyPassedstoolDay30min3) {
	BabyPassedstoolDay30min3 = babyPassedstoolDay30min3;
}
public String getBabyPassedstoolDay30min4() {
	return BabyPassedstoolDay30min4;
}
public void setBabyPassedstoolDay30min4(String babyPassedstoolDay30min4) {
	BabyPassedstoolDay30min4 = babyPassedstoolDay30min4;
}
public String getBabyPassedstoolDay6hrs5() {
	return BabyPassedstoolDay6hrs5;
}
public void setBabyPassedstoolDay6hrs5(String babyPassedstoolDay6hrs5) {
	BabyPassedstoolDay6hrs5 = babyPassedstoolDay6hrs5;
}
public String getBabyPassedstoolDay6hrs6() {
	return BabyPassedstoolDay6hrs6;
}
public void setBabyPassedstoolDay6hrs6(String babyPassedstoolDay6hrs6) {
	BabyPassedstoolDay6hrs6 = babyPassedstoolDay6hrs6;
}
public String getBabyPassedstoolDay6hrs7() {
	return BabyPassedstoolDay6hrs7;
}
public void setBabyPassedstoolDay6hrs7(String babyPassedstoolDay6hrs7) {
	BabyPassedstoolDay6hrs7 = babyPassedstoolDay6hrs7;
}
public String getBabyPassedstoolDay2morning() {
	return BabyPassedstoolDay2morning;
}
public void setBabyPassedstoolDay2morning(String babyPassedstoolDay2morning) {
	BabyPassedstoolDay2morning = babyPassedstoolDay2morning;
}
public String getBabyPassedstoolDay2evening() {
	return BabyPassedstoolDay2evening;
}
public void setBabyPassedstoolDay2evening(String babyPassedstoolDay2evening) {
	BabyPassedstoolDay2evening = babyPassedstoolDay2evening;
}
  
  	
	 

}
