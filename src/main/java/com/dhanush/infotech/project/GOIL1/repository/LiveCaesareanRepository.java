package com.dhanush.infotech.project.GOIL1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dhanush.infotech.project.GOIL1.model.LiveCaesareanCount;

public interface LiveCaesareanRepository extends JpaRepository<LiveCaesareanCount, Long>{

	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.createdat >= :start and a.createdat <= :end and a.deliveryoutcome1 like '%92%'")
	Long findByState1(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.createdat >= :start and a.createdat <= :end and a.deliveryoutcome1 like '%92%'")
	Long findByDistrict1(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.createdat >= :start and a.createdat <= :end and a.deliveryoutcome1 like '%92%'")
	Long findByBlock1(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat >= :start and a.createdat <= :end and a.deliveryoutcome1 like '%92%'")
	Long findByFacilitytype1(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat >= :start and a.createdat <= :end and a.deliveryoutcome1 like '%92%'")
	Long findByFacility1(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);
	
	
	
	
	
	
	@Query(value = "select count(a.deliveryoutcome1) as count FROM LiveCaesareanCount a where a.state = :state and a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%93%'")
	Long findByState3(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(a.deliveryoutcome1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%93%' ")
	Long findByDistrict3(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(a.deliveryoutcome1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%93%' ")
	Long findByBlock3(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(a.deliveryoutcome1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%93%' ")
	Long findByFacilitytype3(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(a.deliveryoutcome1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%93%' ")
	Long findByFacility3(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);

	
	
	
	 

	
	
	
	

	
}
