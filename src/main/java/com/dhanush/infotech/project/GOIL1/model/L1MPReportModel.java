package com.dhanush.infotech.project.GOIL1.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="l1mpreport")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class L1MPReportModel {
	
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Integer getNormal_deliveries() {
		return normal_deliveries;
	}
	public void setNormal_deliveries(Integer normal_deliveries) {
		this.normal_deliveries = normal_deliveries;
	}
	public Integer getAssisted_deliveries() {
		return assisted_deliveries;
	}
	public void setAssisted_deliveries(Integer assisted_deliveries) {
		this.assisted_deliveries = assisted_deliveries;
	}
	public Integer getCaesarean_deliveries() {
		return caesarean_deliveries;
	}
	public void setCaesarean_deliveries(Integer caesarean_deliveries) {
		this.caesarean_deliveries = caesarean_deliveries;
	}
	public Integer getLive_births() {
		return live_births;
	}
	public void setLive_births(Integer live_births) {
		this.live_births = live_births;
	}
	public Integer getStillbirths() {
		return stillbirths;
	}
	public void setStillbirths(Integer stillbirths) {
		this.stillbirths = stillbirths;
	}
	public Integer getMaternal_deaths() {
		return maternal_deaths;
	}
	public void setMaternal_deaths(Integer maternal_deaths) {
		this.maternal_deaths = maternal_deaths;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public int getDistrict() {
		return district;
	}
	public void setDistrict(int district) {
		this.district = district;
	}
	public int getBlock() {
		return block;
	}
	public void setBlock(int block) {
		this.block = block;
	}
	public int getFacilitytype() {
		return facilitytype;
	}
	public void setFacilitytype(int facilitytype) {
		this.facilitytype = facilitytype;
	}
	public int getFacility() {
		return facility;
	}
	public void setFacility(int facility) {
		this.facility = facility;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Column(nullable = false, updatable = false) 
	@Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt=new Date();
	@Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt=new Date();
	 private Integer normal_deliveries;
	 private Integer assisted_deliveries;
	 private Integer caesarean_deliveries;
	 private Integer  live_births;
	 private Integer stillbirths ;
	 private Integer  maternal_deaths;
	 
	 private int  state;
	 private int  district;
	 private int  block;
	 private int  facilitytype;	 
	 private int  facility;
	 private int  month;
	 private int  year;
	 
	 

	 
	
	 

}
