package com.dhanush.infotech.project.GOIL1.controller;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL1.model.Admission;
import com.dhanush.infotech.project.GOIL1.model.DischargeDetails;
import com.dhanush.infotech.project.GOIL1.repository.AdmissionRepository;
import com.dhanush.infotech.project.GOIL1.repository.DischargeDetailsRepository;
import com.dhanush.infotech.project.GOIL1.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api")
public class GetDischargeDetailsController {

	@Autowired
	AdmissionRepository admissionRepository;
	
	@Autowired
	DischargeDetailsRepository dischargeDetailsRepository;
	
	@CrossOrigin
	@RequestMapping(value = "/getdischargedetailsbyid/l1/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public String getUserById(@PathVariable(value = "id") Long UserId) {
		
		JSONObject normal = new JSONObject();
		JSONObject obj = new JSONObject();
		try {
			Admission admission = admissionRepository.findById(UserId)
					.orElseThrow(() -> new ResourceNotFoundException("User", "id", UserId));
		List<DischargeDetails> obstetric1 = dischargeDetailsRepository.findByCaseId(admission.getId());
		if(obstetric1.size() == 0) {
			obj.put("status","Record Not Found");
			return obj.toString();
		}
		else
		obj.put("id", ""+admission.getDischargeDetails().getId());
		obj.put("conditionOfMother", admission.getDischargeDetails().getConditionOfMother());
		obj.put("conditionOfBaby", admission.getDischargeDetails().getConditionOfBaby());
		obj.put("adviceOfMother", admission.getDischargeDetails().getAdviceOfMother());
		obj.put("adviceOfBaby", admission.getDischargeDetails().getAdviceOfBaby());
		obj.put("othernotes", admission.getDischargeDetails().getOthers());
		obj.put("time_of_Discharge_Referral", admission.getDischargeDetails().getTime_of_Discharge_Referral());
		obj.put("date_of_Discharge_Referral", admission.getDischargeDetails().getDate_of_Discharge_Referral());
		obj.put("admissionId", admission.getDischargeDetails().getAdmission().getId());
		JSONObject others = new JSONObject();
		others.put("field1", ""+admission.getField1());
		others.put("field2", ""+admission.getField2());
		others.put("field3", ""+admission.getField3());
		others.put("field4", ""+admission.getField4());
		others.put("field5", ""+admission.getField5());
		others.put("field6", ""+admission.getField6());
		others.put("field7", ""+admission.getField7());
		others.put("field8", ""+admission.getField8());
		others.put("field9",""+ admission.getField9());
		others.put("field10", ""+admission.getField10());
		obj.put("others", others);
		normal.put("status", "ok");
		normal.put("data", obj);
		}
		catch(Exception e) {
			normal.put("status", "No Record Found");
			normal.put("data", obj);
		}
		return normal.toString();
	}

}
