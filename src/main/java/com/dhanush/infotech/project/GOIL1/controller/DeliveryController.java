package com.dhanush.infotech.project.GOIL1.controller;

import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL1.model.Admission;
import com.dhanush.infotech.project.GOIL1.model.AssessmentOfPostPartumCondition;
import com.dhanush.infotech.project.GOIL1.model.BabyNotes;
import com.dhanush.infotech.project.GOIL1.model.Delivery;
import com.dhanush.infotech.project.GOIL1.repository.AdmissionRepository;
import com.dhanush.infotech.project.GOIL1.repository.AssessmentOfPostPartumConditionRepository;
import com.dhanush.infotech.project.GOIL1.repository.BabyNotesRepository;
import com.dhanush.infotech.project.GOIL1.repository.DeliveryRepoistory;
import com.dhanush.infotech.project.GOIL1.exception.ResourceNotFoundException;



@RestController
@RequestMapping("/api")
public class DeliveryController {
	
	@Autowired
	AdmissionRepository admissionRepository;
	
	
	
	@Autowired
	AssessmentOfPostPartumConditionRepository assessmentOfPostPartumConditionRepository;
	

	@Autowired
	DeliveryRepoistory deliveryRepository;
	
	
	@Autowired
	BabyNotesRepository babyNotesRepository;

	@CrossOrigin
	@RequestMapping(value = "/delivery/l1", method = RequestMethod.POST, produces = { "application/json" })
	public HashMap<String, Object> delivery(@RequestBody String userJsonReq) throws NumberFormatException, Exception {
		HashMap<String, Object> userMap = new HashMap<>();
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String id = openJson.getString("id");
		HashMap<String, Object> status = null;
		if (id.equalsIgnoreCase("")) {
			status = insertDischarge(userJsonReq);
			String statu = status.get("data").equals("Delivery Added successfull") ? "OK"
					: "ERROR";
			userMap.put("status", statu);
			userMap.put("data", status.get("data"));
			userMap.put("id", status.get("id"));
			userMap.put("babyNotesarr",  status.get("babyNotesarr"));
			userMap.put("postPartumArr", status.get("postPartumArr"));
		}
		else
		{
		status = updateDischarge(userJsonReq, id);
			String statu = status.get("data").equals("Delivery Updated successfull") ? "OK"
					: "ERROR";
			userMap.put("status", statu);
			userMap.put("data", status.get("data"));
			userMap.put("id", status.get("id"));
			userMap.put("babyNotesarr",  status.get("babyNotesarr"));
			userMap.put("postPartumArr", status.get("postPartumArr"));
		}
		return userMap;
		
	}
		public HashMap<String, Object> insertDischarge(String userJsonReq) {
		HashMap<String, Object> userMap = new HashMap<>();
	//	try {
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String admissionId = openJson.getString("admissionId");
	
			
	
		
		org.json.JSONObject deliveryNotes 		= openJson.getJSONObject("deliveryNotes");
		String deliveryOutcome1				    = deliveryNotes.getString("deliveryOutcome1");
		String deliveryOutcome2 				= deliveryNotes.getString("deliveryOutcome2");
		String deliveryNotesTime 				= deliveryNotes.getString("timeOfBirth");
		String sexOfBaby         				= deliveryNotes.getString("sexOfBaby");
		String preterm          				= deliveryNotes.getString("preterm");
		String birthweight      				= deliveryNotes.getString("birthweight");
		String criedImmediatelyAfterBirth 		= deliveryNotes.getString("criedImmediatelyAfterBirth");
		String resuscitated 					= deliveryNotes.getString("resuscitated");
		String breastFeedingInitiated 			= deliveryNotes.getString("breastFeedingInitiated");
		String ifYesTimeOfInitiation 			= deliveryNotes.getString("ifYesTimeOfInitiation");
		String congenitalAnomaly 				= deliveryNotes.getString("congenitalAnomaly");
		String specify 							= deliveryNotes.getString("specify");
		
		org.json.JSONObject others1 = openJson.getJSONObject("others");
		 String field1   =  others1.getString("field1");;
		 String field2   =  others1.getString("field2");
		 String field3   =  others1.getString("field3");
		 String field4   =  others1.getString("field4");
		 String field5   =  others1.getString("field5");
		 String field6   =  others1.getString("field6");
		 String field7   =  others1.getString("field7");
		 String field8   =  others1.getString("field8");
		 String field9   =  others1.getString("field9");
		 String field10  =  others1.getString("field10");
		
	
		
		
		
		org.json.JSONObject app = openJson.getJSONObject("assessmentOfPostPartumCondition");
		 String 	MotherBPDay30min1 = app.getString("MotherBPDay30min1");                       
		String  MotherBPDay30min2= app.getString("MotherBPDay30min2");                       
		String  MotherBPDay30min3= app.getString("MotherBPDay30min3");                       
		String  MotherBPDay30min4= app.getString("MotherBPDay30min4");                       
		String  MotherBPDay6hrs5= app.getString("MotherBPDay6hrs5");                        
		String   MotherBPDay6hrs6= app.getString("MotherBPDay6hrs6");                      
		String   MotherBPDay6hrs7= app.getString("MotherBPDay6hrs7");                      
		String   MotherBPDay2morning= app.getString("MotherBPDay2morning");                   
		String   MotherBPDay2evening= app.getString("MotherBPDay2evening");                   
		String   MothertempDay30min1= app.getString("MothertempDay30min1");                   
		String   MothertempDay30min2= app.getString("MothertempDay30min2");                    
		String   MothertempDay30min3= app.getString("MothertempDay30min3");                    
		String   MothertempDay30min4= app.getString("MothertempDay30min4");                    
		String    MothertempDay6hrs5= app.getString("MothertempDay6hrs5");                    
		String    MothertempDay6hrs6= app.getString("MothertempDay6hrs6");                    
		String  MothertempDay6hrs7= app.getString("MothertempDay6hrs7");                      
		String  MothertempDay2morning= app.getString("MothertempDay2morning");                   
		String  MothertempDay2evening= app.getString("MothertempDay2evening");                   
		String  MotherpulseDay30min1= app.getString("MotherpulseDay30min1");                    
		String  MotherpulseDay30min2= app.getString("MotherpulseDay30min2");                    
		String   MotherpulseDay30min3= app.getString("MotherpulseDay30min3");                  
		String   MotherpulseDay30min4= app.getString("MotherpulseDay30min4");                  
		String   MotherpulseDay6hrs5= app.getString("MotherpulseDay6hrs5");                   
		String   MotherpulseDay6hrs6= app.getString("MotherpulseDay6hrs6");                   
		String   MotherpulseDay6hrs7= app.getString("MotherpulseDay6hrs7");                   
		String  MotherpulseDay2morning= app.getString("MotherpulseDay2morning");                  
		String   MotherpulseDay2evening= app.getString("MotherpulseDay2evening");                 
		String   MotherBreastconditionDay30min1= app.getString("MotherBreastconditionDay30min1");         
		String   MotherBreastconditionDay30min2= app.getString("MotherBreastconditionDay30min2");         
		String   MotherBreastconditionDay30min3= app.getString("MotherBreastconditionDay30min3");         
		String  MotherBreastconditionDay30min4= app.getString("MotherBreastconditionDay30min4");          
		String    MotherBreastconditionDay6hrs5= app.getString("MotherBreastconditionDay6hrs5");         
		String    MotherBreastconditionDay6hrs6= app.getString("MotherBreastconditionDay6hrs6");         
		String    MotherBreastconditionDay6hrs7= app.getString("MotherBreastconditionDay6hrs7");         
		String    MotherBreastconditionDay2morning= app.getString("MotherBreastconditionDay2morning");      
		String   MotherBreastconditionDay2evening= app.getString("MotherBreastconditionDay2evening");       
		String   MotherBleedingPVDay30min1= app.getString("MotherBleedingPVDay30min1");              
		String    MotherBleedingPVDay30min2= app.getString("MotherBleedingPVDay30min2");             
		String    MotherBleedingPVDay30min3= app.getString("MotherBleedingPVDay30min3");             
		String    MotherBleedingPVDay30min4= app.getString("MotherBleedingPVDay30min4");             
		String  MotherBleedingPVDay6hrs5= app.getString("MotherBleedingPVDay6hrs5");               
		String  MotherBleedingPVDay6hrs6= app.getString("MotherBleedingPVDay6hrs6");               
		String  MotherBleedingPVDay6hrs7= app.getString("MotherBleedingPVDay6hrs7");               
		String  MotherBleedingPVDay2morning= app.getString("MotherBleedingPVDay2morning");            
		String  MotherBleedingPVDay2evening= app.getString("MotherBleedingPVDay2evening");            
		String   MotherUterineToneDay30min1= app.getString("MotherUterineToneDay30min1");            
		String   MotherUterineToneDay30min2= app.getString("MotherUterineToneDay30min2");            
		String   MotherUterineToneDay30min3= app.getString("MotherUterineToneDay30min3");            
		String   MotherUterineToneDay30min4= app.getString("MotherUterineToneDay30min4");            
		String   MotherUterineToneDay6hrs5= app.getString("MotherUterineToneDay6hrs5");             
		String  MotherUterineToneDay6hrs6= app.getString("MotherUterineToneDay6hrs6");               
		String   MotherUterineToneDay6hrs7= app.getString("MotherUterineToneDay6hrs7");              
		String   MotherUterineToneDay2morning= app.getString("MotherUterineToneDay2morning");           
		String   MotherUterineToneDay2evening= app.getString("MotherUterineToneDay2evening");           
		String   MotherEpisiotomyTearDay30min1= app.getString("MotherEpisiotomyTearDay30min1");          
		String  MotherEpisiotomyTearDay30min2= app.getString("MotherEpisiotomyTearDay30min2");          
		String    MotherEpisiotomyTearDay30min3= app.getString("MotherEpisiotomyTearDay30min3");        
		String    MotherEpisiotomyTearDay30min4= app.getString("MotherEpisiotomyTearDay30min4");        
		String    MotherEpisiotomyTearDay6hrs5= app.getString("MotherEpisiotomyTearDay6hrs5");         
		String    MotherEpisiotomyTearDay6hrs6= app.getString("MotherEpisiotomyTearDay6hrs6");         
		String    MotherEpisiotomyTearDay6hrs7= app.getString("MotherEpisiotomyTearDay6hrs7");         
		String    MotherEpisiotomyTearDay2morning= app.getString("MotherEpisiotomyTearDay2morning");      
		String    MotherEpisiotomyTearDay2evening= app.getString("MotherEpisiotomyTearDay2evening");      
		String    BabyResprateDay30min1 = app.getString("BabyResprateDay30min1");   ;                
		String    BabyResprateDay30min2= app.getString("BabyResprateDay30min2");                
		String  BabyResprateDay30min3= app.getString("BabyResprateDay30min3");                  
		String  BabyResprateDay30min4= app.getString("BabyResprateDay30min4");                  
		String  BabyResprateDay6hrs5= app.getString("BabyResprateDay6hrs5");                   
		String  BabyResprateDay6hrs6= app.getString("BabyResprateDay6hrs6");                   
		String  BabyResprateDay6hrs7= app.getString("BabyResprateDay6hrs7");                   
		String BabyResprateDay2morning= app.getString("BabyResprateDay2morning");                  
		String   BabyResprateDay2evening= app.getString("BabyResprateDay2evening");                
		String   BabytempDay30min1= app.getString("BabytempDay30min1");                      
		String   BabytempDay30min2= app.getString("BabytempDay30min2");                      
		String   BabytempDay30min3= app.getString("BabytempDay30min3");                      
		String  BabytempDay30min4= app.getString("BabytempDay30min4");                      
		String    BabytempDay6hrs5= app.getString("BabytempDay6hrs5");                     
		String    BabytempDay6hrs6= app.getString("BabytempDay6hrs6");                     
		String    BabytempDay6hrs7= app.getString("BabytempDay6hrs7");                     
		String    BabytempDay2morning= app.getString("BabytempDay2morning");                  
		String   BabytempDay2evening= app.getString("BabytempDay2evening");                    
		String   BabyBreastfeedingDay30min1= app.getString("BabyBreastfeedingDay30min1");             
		String   BabyBreastfeedingDay30min2= app.getString("BabyBreastfeedingDay30min2");             
		String   BabyBreastfeedingDay30min3= app.getString("BabyBreastfeedingDay30min3");             
		String   BabyBreastfeeding30min4= app.getString("BabyBreastfeeding30min4");                
		String  BabyBreastfeedingDay6hrs5= app.getString("BabyBreastfeedingDay6hrs5");               
		String  BabyBreastfeedingDay6hrs6= app.getString("BabyBreastfeedingDay6hrs6");               
		String  BabyBreastfeedingDay6hrs7= app.getString("BabyBreastfeedingDay6hrs7");               
		String  BabyBreastfeedingDay2morning= app.getString("BabyBreastfeedingDay2morning");            
		String  BabyBreastfeedingDay2evening= app.getString("BabyBreastfeedingDay2evening");            
		String  BabyActivityDay30min1= app.getString("BabyActivityDay30min1");                   
		String BabyActivityDay30min2= app.getString("BabyActivityDay30min2");                    
		String  BabyActivityDay30min3= app.getString("BabyActivityDay30min3");                  
		String  BabyActivityDay30min4= app.getString("BabyActivityDay30min4");                  
		String  BabyActivityDay6hrs5= app.getString("BabyActivityDay6hrs5");                   
		String  BabyActivityDay6hrs6= app.getString("BabyActivityDay6hrs6");                   
		String  BabyActivityDay6hrs7= app.getString("BabyActivityDay6hrs7");                   
		String  BabyActivityDay2morning= app.getString("BabyActivityDay2morning");                
		String   BabyActivityDay2evening= app.getString("BabyActivityDay2evening");               
		 String   BabyUmbilicalstumpDay30min1= app.getString("BabyUmbilicalstumpDay30min1");          
		 String   BabyUmbilicalstumpDay30min2= app.getString("BabyUmbilicalstumpDay30min2");          
		 String   BabyUmbilicalstumpDay30min3= app.getString("BabyUmbilicalstumpDay30min3");          
		 String   BabyUmbilicalstumpDay30min4= app.getString("BabyUmbilicalstumpDay30min4");          
		 String   BabyUmbilicalstumpDay6hrs5= app.getString("BabyUmbilicalstumpDay6hrs5");           
		 String    BabyUmbilicalstumpDay6hrs6= app.getString("BabyUmbilicalstumpDay6hrs6");          
		 String    BabyUmbilicalstumpDay6hrs7= app.getString("BabyUmbilicalstumpDay6hrs7");          
		 String    BabyUmbilicalstumpDay2morning= app.getString("BabyUmbilicalstumpDay2morning");       
		 String    BabyUmbilicalstumpDay2evening= app.getString("BabyUmbilicalstumpDay2evening");       
		 String    BabyJaundiceDay30min1= app.getString("BabyJaundiceDay30min1");               
		 String    BabyJaundiceDay30min2= app.getString("BabyJaundiceDay30min2");               
		String   BabyJaundiceDay30min3= app.getString("BabyJaundiceDay30min3");               
		String   BabyJaundiceDay30min4= app.getString("BabyJaundiceDay30min4");               
		String   BabyJaundiceDay6hrs5= app.getString("BabyJaundiceDay6hrs5");                
		String   BabyJaundiceDay6hrs6= app.getString("BabyJaundiceDay6hrs6");                
		String   BabyJaundiceDay6hrs7= app.getString("BabyJaundiceDay6hrs7");                
		String   BabyJaundiceDay2morning= app.getString("BabyJaundiceDay2morning");               
		String   BabyJaundiceDay2evening= app.getString("BabyJaundiceDay2evening");               
		String   BabyPassedurineDay30min1= app.getString("BabyPassedurineDay30min1");              
		String   BabyPassedurineDay30min2= app.getString("BabyPassedurineDay30min2");              
		String   BabyPassedurineDay30min3= app.getString("BabyPassedurineDay30min3");              
		String   BabyPassedurineDay30min4= app.getString("BabyPassedurineDay30min4");              
		 String  BabyPassedurineDay6hrs5= app.getString("BabyPassedurineDay6hrs5");               
		 String  BabyPassedurineDay6hrs6= app.getString("BabyPassedurineDay6hrs6");               
		 String  BabyPassedurineDay6hrs7= app.getString("BabyPassedurineDay6hrs7");               
		 String  BabyPassedurineDay2morning= app.getString("BabyPassedurineDay2morning");            
		 String  BabyPassedurineDay2evening= app.getString("BabyPassedurineDay2evening");            
		String   BabyPassedstoolDay30min1= app.getString("BabyPassedstoolDay30min1");              
		String   BabyPassedstoolDay30min2= app.getString("BabyPassedstoolDay30min2");              
		String   BabyPassedstoolDay30min3= app.getString("BabyPassedstoolDay30min3");              
		String     BabyPassedstoolDay30min4= app.getString("BabyPassedstoolDay30min4");            
		String     BabyPassedstoolDay6hrs5= app.getString("BabyPassedstoolDay6hrs5");             
		String     BabyPassedstoolDay6hrs6= app.getString("BabyPassedstoolDay6hrs6");             
		 String    BabyPassedstoolDay6hrs7= app.getString("BabyPassedstoolDay6hrs7");             
		 String    BabyPassedstoolDay2morning= app.getString("BabyPassedstoolDay2morning");          
		 String    BabyPassedstoolDay2evening= app.getString("BabyPassedstoolDay2evening");          
		                                                     
		
		
		Admission setAdmission1 = admissionRepository.findById(Long.valueOf(admissionId))
				.orElseThrow(() -> new ResourceNotFoundException("Admission", "NOT FOUND", admissionId));
	
	
		List<Delivery> obstetric1 = deliveryRepository.findByCaseId(setAdmission1.getId());
		if(obstetric1.size() > 0) {
			userMap.put("status", HttpStatus.OK);
			userMap.put("data", "Delivery Already Exist");
			userMap.put("id", setAdmission1.getId());
			return userMap;
		}
		AssessmentOfPostPartumCondition assessmentOfPostPartumCondition = new AssessmentOfPostPartumCondition();
		assessmentOfPostPartumCondition.setMotherBPDay30min1        (MotherBPDay30min1);
		assessmentOfPostPartumCondition.setMotherBPDay30min2        (MotherBPDay30min2);
		assessmentOfPostPartumCondition.setMotherBPDay30min3        (MotherBPDay30min3);
		assessmentOfPostPartumCondition.setMotherBPDay30min4        (MotherBPDay30min4);
		assessmentOfPostPartumCondition.setMotherBPDay6hrs5         (MotherBPDay6hrs5);
		assessmentOfPostPartumCondition.setMotherBPDay6hrs6         (MotherBPDay6hrs6);
		assessmentOfPostPartumCondition.setMotherBPDay6hrs7         (MotherBPDay6hrs7);
		assessmentOfPostPartumCondition.setMotherBPDay2morning      (MotherBPDay2morning);
		assessmentOfPostPartumCondition.setMotherBPDay2evening      (MotherBPDay2evening);
		
		
		assessmentOfPostPartumCondition.setMothertempDay30min1      (MothertempDay30min1);
		assessmentOfPostPartumCondition.setMothertempDay30min2      (MothertempDay30min2);
		assessmentOfPostPartumCondition.setMothertempDay30min3      (MothertempDay30min3);
		assessmentOfPostPartumCondition.setMothertempDay30min4      (MothertempDay30min4);
		assessmentOfPostPartumCondition.setMothertempDay6hrs5       (MothertempDay6hrs5);
		assessmentOfPostPartumCondition.setMothertempDay6hrs6       (MothertempDay6hrs6);
		assessmentOfPostPartumCondition.setMothertempDay6hrs7       (MothertempDay6hrs7);
		assessmentOfPostPartumCondition.setMothertempDay2morning    (MothertempDay2morning);
		assessmentOfPostPartumCondition.setMothertempDay2evening    (MothertempDay2evening);
		
		
		
		                                                            
		assessmentOfPostPartumCondition.setMotherpulseDay30min1     (MotherpulseDay30min1);
		assessmentOfPostPartumCondition.setMotherpulseDay30min2     (MotherpulseDay30min2);
		assessmentOfPostPartumCondition.setMotherpulseDay30min3     (MotherpulseDay30min3);
		assessmentOfPostPartumCondition.setMotherpulseDay30min4     (MotherpulseDay30min4);
		assessmentOfPostPartumCondition.setMotherpulseDay6hrs5      (MotherpulseDay6hrs5);
		assessmentOfPostPartumCondition.setMotherpulseDay6hrs6      (MotherpulseDay6hrs6);
		assessmentOfPostPartumCondition.setMotherpulseDay6hrs7      (MotherpulseDay6hrs7);
		assessmentOfPostPartumCondition.setMotherpulseDay2morning   (MotherpulseDay2morning);
		assessmentOfPostPartumCondition.setMotherpulseDay2evening   (MotherpulseDay2evening);
		                                                            
		
		
		
		assessmentOfPostPartumCondition.setMotherBreastconditionDay30min1    (MotherBreastconditionDay30min1  );
		assessmentOfPostPartumCondition.setMotherBreastconditionDay30min2    (MotherBreastconditionDay30min2  );
		assessmentOfPostPartumCondition.setMotherBreastconditionDay30min3    (MotherBreastconditionDay30min3  );
		assessmentOfPostPartumCondition.setMotherBreastconditionDay30min4    (MotherBreastconditionDay30min4  );
		assessmentOfPostPartumCondition.setMotherBreastconditionDay6hrs5     (MotherBreastconditionDay6hrs5   );
		assessmentOfPostPartumCondition.setMotherBreastconditionDay6hrs6     (MotherBreastconditionDay6hrs6   );
		assessmentOfPostPartumCondition.setMotherBreastconditionDay6hrs7     (MotherBreastconditionDay6hrs7   );
		assessmentOfPostPartumCondition.setMotherBreastconditionDay2morning  (MotherBreastconditionDay2morning);
		assessmentOfPostPartumCondition.setMotherBreastconditionDay2evening  (MotherBreastconditionDay2evening);
		
		
		
		
		
		
		assessmentOfPostPartumCondition.setMotherBleedingPVDay30min1  (MotherBleedingPVDay30min1   );
		assessmentOfPostPartumCondition.setMotherBleedingPVDay30min2  (MotherBleedingPVDay30min2   );
		assessmentOfPostPartumCondition.setMotherBleedingPVDay30min3  (MotherBleedingPVDay30min3   );
		assessmentOfPostPartumCondition.setMotherBleedingPVDay30min4  (MotherBleedingPVDay30min4   );
		assessmentOfPostPartumCondition.setMotherBleedingPVDay6hrs5   (MotherBleedingPVDay6hrs5    );
		assessmentOfPostPartumCondition.setMotherBleedingPVDay6hrs6   (MotherBleedingPVDay6hrs6    );
		assessmentOfPostPartumCondition.setMotherBleedingPVDay6hrs7   (MotherBleedingPVDay6hrs7    );
		assessmentOfPostPartumCondition.setMotherBleedingPVDay2morning(MotherBleedingPVDay2morning );
		assessmentOfPostPartumCondition.setMotherBleedingPVDay2evening(MotherBleedingPVDay2evening );
		
		
		
		
		
		
		assessmentOfPostPartumCondition.setMotherUterineToneDay30min1       ( MotherUterineToneDay30min1   );
		assessmentOfPostPartumCondition.setMotherUterineToneDay30min2       ( MotherUterineToneDay30min2   );
		assessmentOfPostPartumCondition.setMotherUterineToneDay30min3       ( MotherUterineToneDay30min3   );
		assessmentOfPostPartumCondition.setMotherUterineToneDay30min4       ( MotherUterineToneDay30min4   );
		assessmentOfPostPartumCondition.setMotherUterineToneDay6hrs5        ( MotherUterineToneDay6hrs5    );
		assessmentOfPostPartumCondition.setMotherUterineToneDay6hrs6        ( MotherUterineToneDay6hrs6    );
		assessmentOfPostPartumCondition.setMotherUterineToneDay6hrs7        ( MotherUterineToneDay6hrs7    );
		assessmentOfPostPartumCondition.setMotherUterineToneDay2morning     ( MotherUterineToneDay2morning );
		assessmentOfPostPartumCondition.setMotherUterineToneDay2evening     ( MotherUterineToneDay2evening );
		
        
		assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay30min1       ( MotherEpisiotomyTearDay30min1   );       
		assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay30min2       ( MotherEpisiotomyTearDay30min2   );       
		assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay30min3       ( MotherEpisiotomyTearDay30min3   );       
		assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay30min4       ( MotherEpisiotomyTearDay30min4   );       
		assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay6hrs5        ( MotherEpisiotomyTearDay6hrs5    );       
		assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay6hrs6        ( MotherEpisiotomyTearDay6hrs6    );       
		assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay6hrs7        ( MotherEpisiotomyTearDay6hrs7    );       
		assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay2morning     ( MotherEpisiotomyTearDay2morning );       
		assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay2evening     ( MotherEpisiotomyTearDay2evening );       
        
        
		assessmentOfPostPartumCondition.setBabyResprateDay30min1       ( BabyResprateDay30min1   );  
		assessmentOfPostPartumCondition.setBabyResprateDay30min2       ( BabyResprateDay30min2   );  
		assessmentOfPostPartumCondition.setBabyResprateDay30min3       ( BabyResprateDay30min3   );  
		assessmentOfPostPartumCondition.setBabyResprateDay30min4       ( BabyResprateDay30min4   );  
		assessmentOfPostPartumCondition.setBabyResprateDay6hrs5        ( BabyResprateDay6hrs5    );  
		assessmentOfPostPartumCondition.setBabyResprateDay6hrs6        ( BabyResprateDay6hrs6    );  
		assessmentOfPostPartumCondition.setBabyResprateDay6hrs7        ( BabyResprateDay6hrs7    );  
		assessmentOfPostPartumCondition.setBabyResprateDay2morning     ( BabyResprateDay2morning );  
		assessmentOfPostPartumCondition.setBabyResprateDay2evening     ( BabyResprateDay2evening );  
		
		
        
		assessmentOfPostPartumCondition.setBabytempDay30min1       ( BabytempDay30min1   );  
		assessmentOfPostPartumCondition.setBabytempDay30min2       ( BabytempDay30min2   );  
		assessmentOfPostPartumCondition.setBabytempDay30min3       ( BabytempDay30min3   );  
		assessmentOfPostPartumCondition.setBabytempDay30min4       ( BabytempDay30min4   );  
		assessmentOfPostPartumCondition.setBabytempDay6hrs5        ( BabytempDay6hrs5    );  
		assessmentOfPostPartumCondition.setBabytempDay6hrs6        ( BabytempDay6hrs6    );  
		assessmentOfPostPartumCondition.setBabytempDay6hrs7        ( BabytempDay6hrs7    );  
		assessmentOfPostPartumCondition.setBabytempDay2morning     ( BabytempDay2morning );  
		assessmentOfPostPartumCondition.setBabytempDay2evening     ( BabytempDay2evening );  
				
		
		assessmentOfPostPartumCondition.setBabyBreastfeedingDay30min1       ( BabyBreastfeedingDay30min1  );   
		assessmentOfPostPartumCondition.setBabyBreastfeedingDay30min2       ( BabyBreastfeedingDay30min2  );   
		assessmentOfPostPartumCondition.setBabyBreastfeedingDay30min3       ( BabyBreastfeedingDay30min3  );   
		assessmentOfPostPartumCondition.setBabyBreastfeeding30min4      	( BabyBreastfeeding30min4  );   
		assessmentOfPostPartumCondition.setBabyBreastfeedingDay6hrs5        ( BabyBreastfeedingDay6hrs5   );   
		assessmentOfPostPartumCondition.setBabyBreastfeedingDay6hrs6        ( BabyBreastfeedingDay6hrs6   );   
		assessmentOfPostPartumCondition.setBabyBreastfeedingDay6hrs7        ( BabyBreastfeedingDay6hrs7   );   
		assessmentOfPostPartumCondition.setBabyBreastfeedingDay2morning     ( BabyBreastfeedingDay2morning);   
		assessmentOfPostPartumCondition.setBabyBreastfeedingDay2evening     ( BabyBreastfeedingDay2evening);   
				                                                                                  
		assessmentOfPostPartumCondition.setBabyActivityDay30min1       ( BabyActivityDay30min1  ); 
		assessmentOfPostPartumCondition.setBabyActivityDay30min2       ( BabyActivityDay30min2  ); 
		assessmentOfPostPartumCondition.setBabyActivityDay30min3       ( BabyActivityDay30min3  ); 
		assessmentOfPostPartumCondition.setBabyActivityDay30min4       ( BabyActivityDay30min4  );    
		assessmentOfPostPartumCondition.setBabyActivityDay6hrs5        ( BabyActivityDay6hrs5   ); 
		assessmentOfPostPartumCondition.setBabyActivityDay6hrs6        ( BabyActivityDay6hrs6   ); 
		assessmentOfPostPartumCondition.setBabyActivityDay6hrs7        ( BabyActivityDay6hrs7   ); 
		assessmentOfPostPartumCondition.setBabyActivityDay2morning     ( BabyActivityDay2morning); 
		assessmentOfPostPartumCondition.setBabyActivityDay2evening     ( BabyActivityDay2evening); 
				                                                                                       
		
        
			assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay30min1       ( BabyUmbilicalstumpDay30min1    );     
			assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay30min2       ( BabyUmbilicalstumpDay30min2    );     
			assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay30min3       ( BabyUmbilicalstumpDay30min3    );     
			assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay30min4       ( BabyUmbilicalstumpDay30min4    );     
			assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay6hrs5        ( BabyUmbilicalstumpDay6hrs5     );     
			assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay6hrs6        ( BabyUmbilicalstumpDay6hrs6     );     
			assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay6hrs7        ( BabyUmbilicalstumpDay6hrs7     );     
			assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay2morning     ( BabyUmbilicalstumpDay2morning  );     
			assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay2evening     ( BabyUmbilicalstumpDay2evening  ); 
			
			
            
			assessmentOfPostPartumCondition.setBabyJaundiceDay30min1       ( BabyJaundiceDay30min1   ); 
			assessmentOfPostPartumCondition.setBabyJaundiceDay30min2       ( BabyJaundiceDay30min2   ); 
			assessmentOfPostPartumCondition.setBabyJaundiceDay30min3       ( BabyJaundiceDay30min3   ); 
			assessmentOfPostPartumCondition.setBabyJaundiceDay30min4       ( BabyJaundiceDay30min4   ); 
			assessmentOfPostPartumCondition.setBabyJaundiceDay6hrs5        ( BabyJaundiceDay6hrs5    ); 
			assessmentOfPostPartumCondition.setBabyJaundiceDay6hrs6        ( BabyJaundiceDay6hrs6    ); 
			assessmentOfPostPartumCondition.setBabyJaundiceDay6hrs7        ( BabyJaundiceDay6hrs7    ); 
			assessmentOfPostPartumCondition.setBabyJaundiceDay2morning     ( BabyJaundiceDay2morning ); 
			assessmentOfPostPartumCondition.setBabyJaundiceDay2evening     ( BabyJaundiceDay2evening ); 
			
			
			assessmentOfPostPartumCondition.setBabyPassedurineDay30min1       ( BabyPassedurineDay30min1   );         
			assessmentOfPostPartumCondition.setBabyPassedurineDay30min2       ( BabyPassedurineDay30min2   );         
			assessmentOfPostPartumCondition.setBabyPassedurineDay30min3       ( BabyPassedurineDay30min3   );         
			assessmentOfPostPartumCondition.setBabyPassedurineDay30min4       ( BabyPassedurineDay30min4   );         
			assessmentOfPostPartumCondition.setBabyPassedurineDay6hrs5        ( BabyPassedurineDay6hrs5    );         
			assessmentOfPostPartumCondition.setBabyPassedurineDay6hrs6        ( BabyPassedurineDay6hrs6    );         
			assessmentOfPostPartumCondition.setBabyPassedurineDay6hrs7        ( BabyPassedurineDay6hrs7    );         
			assessmentOfPostPartumCondition.setBabyPassedurineDay2morning     ( BabyPassedurineDay2morning );         
			assessmentOfPostPartumCondition.setBabyPassedurineDay2evening     ( BabyPassedurineDay2evening ); 
			
			
			assessmentOfPostPartumCondition.setBabyPassedstoolDay30min1       (BabyPassedstoolDay30min1  ); 
			assessmentOfPostPartumCondition.setBabyPassedstoolDay30min2       (BabyPassedstoolDay30min2  ); 
			assessmentOfPostPartumCondition.setBabyPassedstoolDay30min3       (BabyPassedstoolDay30min3  ); 
			assessmentOfPostPartumCondition.setBabyPassedstoolDay30min4       (BabyPassedstoolDay30min4  ); 
			assessmentOfPostPartumCondition.setBabyPassedstoolDay6hrs5        (BabyPassedstoolDay6hrs5   ); 
			assessmentOfPostPartumCondition.setBabyPassedstoolDay6hrs6        (BabyPassedstoolDay6hrs6   ); 
			assessmentOfPostPartumCondition.setBabyPassedstoolDay6hrs7        (BabyPassedstoolDay6hrs7   ); 
			assessmentOfPostPartumCondition.setBabyPassedstoolDay2morning     (BabyPassedstoolDay2morning); 
			assessmentOfPostPartumCondition.setBabyPassedstoolDay2evening     (BabyPassedstoolDay2evening); 
        
		
	 	assessmentOfPostPartumCondition.setAdmission(setAdmission1);
	 	assessmentOfPostPartumConditionRepository.save(assessmentOfPostPartumCondition);
	 	 assessmentOfPostPartumCondition.getId();
	 	 
	 	 
	 	 
	 	
			Delivery obsteric = new Delivery();
			obsteric.setDeliveryOutcome1(deliveryOutcome1)	;	
			obsteric.setDeliveryOutcome2(deliveryOutcome2) ;	
			obsteric.setDeliveryNotesTime(deliveryNotesTime) ;	
			obsteric.setSexOfBaby(sexOfBaby)         ;	
			obsteric.setPreterm (preterm);         	
			obsteric.setBirthweight(birthweight);    	
			obsteric.setCriedImmediatelyAfterBirth(criedImmediatelyAfterBirth);
			obsteric.setResuscitated (resuscitated);
			obsteric.setBreastFeedingInitiated(breastFeedingInitiated);
			obsteric.setIfYesTimeOfInitiation(ifYesTimeOfInitiation);
			obsteric.setCongenitalAnomaly(congenitalAnomaly);
			obsteric.setSpecify (specify);
			
			
			obsteric.setField1(field1);
			obsteric.setField2(field2);
			obsteric.setField3(field3);
			obsteric.setField4(field4);
			obsteric.setField5(field5);
			obsteric.setField6(field6);
			obsteric.setField7(field7);
			obsteric.setField8(field8);
			obsteric.setField9(field9);
			obsteric.setField10(field10);
			
			
			obsteric.setAdmission(setAdmission1);
			setAdmission1.setId(Long.valueOf(admissionId));
			setAdmission1.setCompleteness("60%");
			obsteric.setAdmission(setAdmission1);
			deliveryRepository.save(obsteric);
	
		JSONArray arr = (JSONArray) openJson.get("babyNotes");
		Long[] babyNotesarr = new Long[arr.length()];
		List<BabyNotes> babyNotesObj = babyNotesRepository.findByCaseId(setAdmission1.getUniqueNo());
		if(babyNotesObj.size() > 0) {
			userMap.put("status", HttpStatus.OK);
			userMap.put("data", "Delivery Already Exist");
			userMap.put("id", setAdmission1.getId());
			return userMap;
		}
		for (int i = 0; i < arr.length(); i++) {
		  String injVitaminKAdministered=arr.getJSONObject(i).getString("injVitaminKAdministered");        
		  String ifYesDose               =arr.getJSONObject(i).getString("ifYesDose");                       
		  String vaccinationDone        =arr.getJSONObject(i).getString("vaccinationDone");         
		  String conditionOfBaby         =arr.getJSONObject(i).getString("conditionOfBaby");      
		  String detailsOf3rd4thStageOfLabor=arr.getJSONObject(i).getString("detailsOf3rd4thStageOfLabor");     
		  String anyOtherDrugSpecify=arr.getJSONObject(i).getString("anyOtherDrugSpecify");            
		  String cctWard		=arr.getJSONObject(i).getString("cctWard");                      
		  String utMassageWard=arr.getJSONObject(i).getString("utMassageWard");                  
		  String placentaCompleteWard=arr.getJSONObject(i).getString("placentaCompleteWard");           
		  String episiotomyGivenWard=arr.getJSONObject(i).getString("episiotomyGivenWard");             
		  String ppiucdInsertedWard=arr.getJSONObject(i).getString("ppiucdInsertedWard");             
		  String complicationsDuringDeliveryWard =arr.getJSONObject(i).getString("complicationsDuringDeliveryWard");
		  String dateOfTransferToPncWard =arr.getJSONObject(i).getString("dateOfTransferToPncWard");        
	      String timeOfTransferToPncWard =arr.getJSONObject(i).getString("timeOfTransferToPncWard");      
	      String babyReferredWard =arr.getJSONObject(i).getString("babyReferredWard");             
	      String ifYesgiveReasonWard =arr.getJSONObject(i).getString("ifYesgiveReasonWard");            
			
		
		String child_id =setAdmission1.getUniqueNo()+"GOI"+i;
		BabyNotes babynotes = new BabyNotes();
		babynotes.setInjVitaminKAdministered(injVitaminKAdministered);
		babynotes.setIfYesDose(ifYesDose);
		babynotes.setVaccinationDone(vaccinationDone);
		babynotes.setConditionOfBaby(conditionOfBaby);
		babynotes.setDetailsOf3rd4thStageOfLabor(detailsOf3rd4thStageOfLabor);
		babynotes.setAnyOtherDrugSpecify(anyOtherDrugSpecify);
		babynotes.setCctWard(cctWard);
		babynotes.setUtMassageWard(utMassageWard);
		babynotes.setPlacentaCompleteWard(placentaCompleteWard);
		babynotes.setEpisiotomyGivenWard(episiotomyGivenWard);
		babynotes.setPpiucdInsertedWard(ppiucdInsertedWard);
		babynotes.setComplicationsDuringDeliveryWard(complicationsDuringDeliveryWard);
		babynotes.setDateOfTransferToPncWard(dateOfTransferToPncWard);
		babynotes.setTimeOfTransferToPncWard(timeOfTransferToPncWard);
		babynotes.setBabyReferredWard(babyReferredWard);
		babynotes.setIfYesgiveReasonWard(ifYesgiveReasonWard);
		babynotes.setBabyId(child_id);
		babynotes.setAdmission(setAdmission1);
		babynotes.setUniqueId(setAdmission1.getUniqueNo());
		babyNotesRepository.save(babynotes);
		babyNotesarr[i] = babynotes.getId();
		}
		
		
		
	
		
		
		
		
		
	
		userMap.put("status", HttpStatus.OK);
		userMap.put("data", "Delivery Added successfull");
		userMap.put("id", obsteric.getId());
		userMap.put("babyNotesarr", babyNotesarr);
		userMap.put("postPartumArr", assessmentOfPostPartumCondition.getId());
	//	}
	/*	catch(Exception e)
		{
			userMap.put("status",  HttpStatus.INTERNAL_SERVER_ERROR);
			userMap.put("data", "Delivery Failure");
			userMap.put("id", "fail");
			userMap.put("babyNotesarr",  "fail");
			userMap.put("postPartumArr",  "fail");
		}*/
		return userMap;
		
	}
		public HashMap<String, Object> updateDischarge(String userJsonReq, String id) {
			HashMap<String, Object> userMap = new HashMap<>();
		//		try {
				org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
				String admissionId = openJson.getString("admissionId");
				org.json.JSONObject deliveryNotes 		= openJson.getJSONObject("deliveryNotes");
				String deliveryOutcome1				    = deliveryNotes.getString("deliveryOutcome1");
				String deliveryOutcome2 				= deliveryNotes.getString("deliveryOutcome2");
				String deliveryNotesTime 				= deliveryNotes.getString("timeOfBirth");
				String sexOfBaby         				= deliveryNotes.getString("sexOfBaby");
				String preterm          				= deliveryNotes.getString("preterm");
				String birthweight      				= deliveryNotes.getString("birthweight");
				String criedImmediatelyAfterBirth 		= deliveryNotes.getString("criedImmediatelyAfterBirth");
				String resuscitated 					= deliveryNotes.getString("resuscitated");
				String breastFeedingInitiated 			= deliveryNotes.getString("breastFeedingInitiated");
				String ifYesTimeOfInitiation 			= deliveryNotes.getString("ifYesTimeOfInitiation");
				String congenitalAnomaly 				= deliveryNotes.getString("congenitalAnomaly");
				String specify 							= deliveryNotes.getString("specify");
				
				
				
				
				
				
				org.json.JSONObject app = openJson.getJSONObject("assessmentOfPostPartumCondition");
				 String 	MotherBPDay30min1 = app.getString("MotherBPDay30min1");                       
				String  MotherBPDay30min2= app.getString("MotherBPDay30min2");                       
				String  MotherBPDay30min3= app.getString("MotherBPDay30min3");                       
				String  MotherBPDay30min4= app.getString("MotherBPDay30min4");                       
				String  MotherBPDay6hrs5= app.getString("MotherBPDay6hrs5");                        
				String   MotherBPDay6hrs6= app.getString("MotherBPDay6hrs6");                      
				String   MotherBPDay6hrs7= app.getString("MotherBPDay6hrs7");                      
				String   MotherBPDay2morning= app.getString("MotherBPDay2morning");                   
				String   MotherBPDay2evening= app.getString("MotherBPDay2evening");                   
				String   MothertempDay30min1= app.getString("MothertempDay30min1");                   
				String   MothertempDay30min2= app.getString("MothertempDay30min2");                    
				String   MothertempDay30min3= app.getString("MothertempDay30min3");                    
				String   MothertempDay30min4= app.getString("MothertempDay30min4");                    
				String    MothertempDay6hrs5= app.getString("MothertempDay6hrs5");                    
				String    MothertempDay6hrs6= app.getString("MothertempDay6hrs6");                    
				String  MothertempDay6hrs7= app.getString("MothertempDay6hrs7");                      
				String  MothertempDay2morning= app.getString("MothertempDay2morning");                   
				String  MothertempDay2evening= app.getString("MothertempDay2evening");                   
				String  MotherpulseDay30min1= app.getString("MotherpulseDay30min1");                    
				String  MotherpulseDay30min2= app.getString("MotherpulseDay30min2");                    
				String   MotherpulseDay30min3= app.getString("MotherpulseDay30min3");                  
				String   MotherpulseDay30min4= app.getString("MotherpulseDay30min4");                  
				String   MotherpulseDay6hrs5= app.getString("MotherpulseDay6hrs5");                   
				String   MotherpulseDay6hrs6= app.getString("MotherpulseDay6hrs6");                   
				String   MotherpulseDay6hrs7= app.getString("MotherpulseDay6hrs7");                   
				String  MotherpulseDay2morning= app.getString("MotherpulseDay2morning");                  
				String   MotherpulseDay2evening= app.getString("MotherpulseDay2evening");                 
				String   MotherBreastconditionDay30min1= app.getString("MotherBreastconditionDay30min1");         
				String   MotherBreastconditionDay30min2= app.getString("MotherBreastconditionDay30min2");         
				String   MotherBreastconditionDay30min3= app.getString("MotherBreastconditionDay30min3");         
				String  MotherBreastconditionDay30min4= app.getString("MotherBreastconditionDay30min4");          
				String    MotherBreastconditionDay6hrs5= app.getString("MotherBreastconditionDay6hrs5");         
				String    MotherBreastconditionDay6hrs6= app.getString("MotherBreastconditionDay6hrs6");         
				String    MotherBreastconditionDay6hrs7= app.getString("MotherBreastconditionDay6hrs7");         
				String    MotherBreastconditionDay2morning= app.getString("MotherBreastconditionDay2morning");      
				String   MotherBreastconditionDay2evening= app.getString("MotherBreastconditionDay2evening");       
				String   MotherBleedingPVDay30min1= app.getString("MotherBleedingPVDay30min1");              
				String    MotherBleedingPVDay30min2= app.getString("MotherBleedingPVDay30min2");             
				String    MotherBleedingPVDay30min3= app.getString("MotherBleedingPVDay30min3");             
				String    MotherBleedingPVDay30min4= app.getString("MotherBleedingPVDay30min4");             
				String  MotherBleedingPVDay6hrs5= app.getString("MotherBleedingPVDay6hrs5");               
				String  MotherBleedingPVDay6hrs6= app.getString("MotherBleedingPVDay6hrs6");               
				String  MotherBleedingPVDay6hrs7= app.getString("MotherBleedingPVDay6hrs7");               
				String  MotherBleedingPVDay2morning= app.getString("MotherBleedingPVDay2morning");            
				String  MotherBleedingPVDay2evening= app.getString("MotherBleedingPVDay2evening");            
				String   MotherUterineToneDay30min1= app.getString("MotherUterineToneDay30min1");            
				String   MotherUterineToneDay30min2= app.getString("MotherUterineToneDay30min2");            
				String   MotherUterineToneDay30min3= app.getString("MotherUterineToneDay30min3");            
				String   MotherUterineToneDay30min4= app.getString("MotherUterineToneDay30min4");            
				String   MotherUterineToneDay6hrs5= app.getString("MotherUterineToneDay6hrs5");             
				String  MotherUterineToneDay6hrs6= app.getString("MotherUterineToneDay6hrs6");               
				String   MotherUterineToneDay6hrs7= app.getString("MotherUterineToneDay6hrs7");              
				String   MotherUterineToneDay2morning= app.getString("MotherUterineToneDay2morning");           
				String   MotherUterineToneDay2evening= app.getString("MotherUterineToneDay2evening");           
				String   MotherEpisiotomyTearDay30min1= app.getString("MotherEpisiotomyTearDay30min1");          
				String  MotherEpisiotomyTearDay30min2= app.getString("MotherEpisiotomyTearDay30min2");          
				String    MotherEpisiotomyTearDay30min3= app.getString("MotherEpisiotomyTearDay30min3");        
				String    MotherEpisiotomyTearDay30min4= app.getString("MotherEpisiotomyTearDay30min4");        
				String    MotherEpisiotomyTearDay6hrs5= app.getString("MotherEpisiotomyTearDay6hrs5");         
				String    MotherEpisiotomyTearDay6hrs6= app.getString("MotherEpisiotomyTearDay6hrs6");         
				String    MotherEpisiotomyTearDay6hrs7= app.getString("MotherEpisiotomyTearDay6hrs7");         
				String    MotherEpisiotomyTearDay2morning= app.getString("MotherEpisiotomyTearDay2morning");      
				String    MotherEpisiotomyTearDay2evening= app.getString("MotherEpisiotomyTearDay2evening");      
				String    BabyResprateDay30min1 = app.getString("BabyResprateDay30min1");   ;                
				String    BabyResprateDay30min2= app.getString("BabyResprateDay30min2");                
				String  BabyResprateDay30min3= app.getString("BabyResprateDay30min3");                  
				String  BabyResprateDay30min4= app.getString("BabyResprateDay30min4");                  
				String  BabyResprateDay6hrs5= app.getString("BabyResprateDay6hrs5");                   
				String  BabyResprateDay6hrs6= app.getString("BabyResprateDay6hrs6");                   
				String  BabyResprateDay6hrs7= app.getString("BabyResprateDay6hrs7");                   
				String BabyResprateDay2morning= app.getString("BabyResprateDay2morning");                  
				String   BabyResprateDay2evening= app.getString("BabyResprateDay2evening");                
				String   BabytempDay30min1= app.getString("BabytempDay30min1");                      
				String   BabytempDay30min2= app.getString("BabytempDay30min2");                      
				String   BabytempDay30min3= app.getString("BabytempDay30min3");                      
				String  BabytempDay30min4= app.getString("BabytempDay30min4");                      
				String    BabytempDay6hrs5= app.getString("BabytempDay6hrs5");                     
				String    BabytempDay6hrs6= app.getString("BabytempDay6hrs6");                     
				String    BabytempDay6hrs7= app.getString("BabytempDay6hrs7");                     
				String    BabytempDay2morning= app.getString("BabytempDay2morning");                  
				String   BabytempDay2evening= app.getString("BabytempDay2evening");                    
				String   BabyBreastfeedingDay30min1= app.getString("BabyBreastfeedingDay30min1");             
				String   BabyBreastfeedingDay30min2= app.getString("BabyBreastfeedingDay30min2");             
				String   BabyBreastfeedingDay30min3= app.getString("BabyBreastfeedingDay30min3");             
				String   BabyBreastfeeding30min4= app.getString("BabyBreastfeeding30min4");                
				String  BabyBreastfeedingDay6hrs5= app.getString("BabyBreastfeedingDay6hrs5");               
				String  BabyBreastfeedingDay6hrs6= app.getString("BabyBreastfeedingDay6hrs6");               
				String  BabyBreastfeedingDay6hrs7= app.getString("BabyBreastfeedingDay6hrs7");               
				String  BabyBreastfeedingDay2morning= app.getString("BabyBreastfeedingDay2morning");            
				String  BabyBreastfeedingDay2evening= app.getString("BabyBreastfeedingDay2evening");            
				String  BabyActivityDay30min1= app.getString("BabyActivityDay30min1");                   
				String BabyActivityDay30min2= app.getString("BabyActivityDay30min2");                    
				String  BabyActivityDay30min3= app.getString("BabyActivityDay30min3");                  
				String  BabyActivityDay30min4= app.getString("BabyActivityDay30min4");                  
				String  BabyActivityDay6hrs5= app.getString("BabyActivityDay6hrs5");                   
				String  BabyActivityDay6hrs6= app.getString("BabyActivityDay6hrs6");                   
				String  BabyActivityDay6hrs7= app.getString("BabyActivityDay6hrs7");                   
				String  BabyActivityDay2morning= app.getString("BabyActivityDay2morning");                
				String   BabyActivityDay2evening= app.getString("BabyActivityDay2evening");               
				 String   BabyUmbilicalstumpDay30min1= app.getString("BabyUmbilicalstumpDay30min1");          
				 String   BabyUmbilicalstumpDay30min2= app.getString("BabyUmbilicalstumpDay30min2");          
				 String   BabyUmbilicalstumpDay30min3= app.getString("BabyUmbilicalstumpDay30min3");          
				 String   BabyUmbilicalstumpDay30min4= app.getString("BabyUmbilicalstumpDay30min4");          
				 String   BabyUmbilicalstumpDay6hrs5= app.getString("BabyUmbilicalstumpDay6hrs5");           
				 String    BabyUmbilicalstumpDay6hrs6= app.getString("BabyUmbilicalstumpDay6hrs6");          
				 String    BabyUmbilicalstumpDay6hrs7= app.getString("BabyUmbilicalstumpDay6hrs7");          
				 String    BabyUmbilicalstumpDay2morning= app.getString("BabyUmbilicalstumpDay2morning");       
				 String    BabyUmbilicalstumpDay2evening= app.getString("BabyUmbilicalstumpDay2evening");       
				 String    BabyJaundiceDay30min1= app.getString("BabyJaundiceDay30min1");               
				 String    BabyJaundiceDay30min2= app.getString("BabyJaundiceDay30min2");               
				String   BabyJaundiceDay30min3= app.getString("BabyJaundiceDay30min3");               
				String   BabyJaundiceDay30min4= app.getString("BabyJaundiceDay30min4");               
				String   BabyJaundiceDay6hrs5= app.getString("BabyJaundiceDay6hrs5");                
				String   BabyJaundiceDay6hrs6= app.getString("BabyJaundiceDay6hrs6");                
				String   BabyJaundiceDay6hrs7= app.getString("BabyJaundiceDay6hrs7");                
				String   BabyJaundiceDay2morning= app.getString("BabyJaundiceDay2morning");               
				String   BabyJaundiceDay2evening= app.getString("BabyJaundiceDay2evening");               
				String   BabyPassedurineDay30min1= app.getString("BabyPassedurineDay30min1");              
				String   BabyPassedurineDay30min2= app.getString("BabyPassedurineDay30min2");              
				String   BabyPassedurineDay30min3= app.getString("BabyPassedurineDay30min3");              
				String   BabyPassedurineDay30min4= app.getString("BabyPassedurineDay30min4");              
				 String  BabyPassedurineDay6hrs5= app.getString("BabyPassedurineDay6hrs5");               
				 String  BabyPassedurineDay6hrs6= app.getString("BabyPassedurineDay6hrs6");               
				 String  BabyPassedurineDay6hrs7= app.getString("BabyPassedurineDay6hrs7");               
				 String  BabyPassedurineDay2morning= app.getString("BabyPassedurineDay2morning");            
				 String  BabyPassedurineDay2evening= app.getString("BabyPassedurineDay2evening");            
				String   BabyPassedstoolDay30min1= app.getString("BabyPassedstoolDay30min1");              
				String   BabyPassedstoolDay30min2= app.getString("BabyPassedstoolDay30min2");              
				String   BabyPassedstoolDay30min3= app.getString("BabyPassedstoolDay30min3");              
				String   BabyPassedstoolDay30min4= app.getString("BabyPassedstoolDay30min4");            
				String   BabyPassedstoolDay6hrs5= app.getString("BabyPassedstoolDay6hrs5");             
				String   BabyPassedstoolDay6hrs6= app.getString("BabyPassedstoolDay6hrs6");             
				 String  BabyPassedstoolDay6hrs7= app.getString("BabyPassedstoolDay6hrs7");             
				 String  BabyPassedstoolDay2morning= app.getString("BabyPassedstoolDay2morning");          
				 String  BabyPassedstoolDay2evening= app.getString("BabyPassedstoolDay2evening");          
				                                                     
				
				 
				 
					org.json.JSONObject others1 = openJson.getJSONObject("others");
					 String field1   =  others1.getString("field1");;
					 String field2   =  others1.getString("field2");
					 String field3   =  others1.getString("field3");
					 String field4   =  others1.getString("field4");
					 String field5   =  others1.getString("field5");
					 String field6   =  others1.getString("field6");
					 String field7   =  others1.getString("field7");
					 String field8   =  others1.getString("field8");
					 String field9   =  others1.getString("field9");
					 String field10  =  others1.getString("field10");
				 
				 
				 
				 Admission setAdmission1 = admissionRepository.findById(Long.valueOf(admissionId))
							.orElseThrow(() -> new ResourceNotFoundException("Admission", "NOT FOUND", admissionId));
					Delivery obsteric = deliveryRepository.findById(Long.valueOf(setAdmission1.getDelivery().getId()))
							.orElseThrow(() -> new ResourceNotFoundException("delivery", "NOT FOUND", id));
			
				AssessmentOfPostPartumCondition assessmentOfPostPartumCondition = 
						assessmentOfPostPartumConditionRepository.findById(Long.valueOf(setAdmission1.getAssessmentOfPostPartumCondition().getId()))
						.orElseThrow(() -> new ResourceNotFoundException("delivery", "NOT FOUND", id));
				
				assessmentOfPostPartumCondition.setMotherBPDay30min1(MotherBPDay30min1);
				assessmentOfPostPartumCondition.setMotherBPDay30min2(MotherBPDay30min2);
				assessmentOfPostPartumCondition.setMotherBPDay30min3(MotherBPDay30min3);
				assessmentOfPostPartumCondition.setMotherBPDay30min4(MotherBPDay30min4);
				assessmentOfPostPartumCondition.setMotherBPDay6hrs5(MotherBPDay6hrs5);
				assessmentOfPostPartumCondition.setMotherBPDay6hrs6(MotherBPDay6hrs6);
				assessmentOfPostPartumCondition.setMotherBPDay6hrs7(MotherBPDay6hrs7);
				assessmentOfPostPartumCondition.setMotherBPDay2morning(MotherBPDay2morning);
				assessmentOfPostPartumCondition.setMotherBPDay2evening(MotherBPDay2evening);
				
				
				assessmentOfPostPartumCondition.setMothertempDay30min1(MothertempDay30min1);
				assessmentOfPostPartumCondition.setMothertempDay30min2(MothertempDay30min2);
				assessmentOfPostPartumCondition.setMothertempDay30min3(MothertempDay30min3);
				assessmentOfPostPartumCondition.setMothertempDay30min4(MothertempDay30min4);
				assessmentOfPostPartumCondition.setMothertempDay6hrs5(MothertempDay6hrs5);
				assessmentOfPostPartumCondition.setMothertempDay6hrs6(MothertempDay6hrs6);
				assessmentOfPostPartumCondition.setMothertempDay6hrs7(MothertempDay6hrs7);
				assessmentOfPostPartumCondition.setMothertempDay2morning(MothertempDay2morning);
				assessmentOfPostPartumCondition.setMothertempDay2evening(MothertempDay2evening);
				
				
				
				
				assessmentOfPostPartumCondition.setMotherpulseDay30min1(MotherpulseDay30min1);
				assessmentOfPostPartumCondition.setMotherpulseDay30min2(MotherpulseDay30min2);
				assessmentOfPostPartumCondition.setMotherpulseDay30min3(MotherpulseDay30min3);
				assessmentOfPostPartumCondition.setMotherpulseDay30min4(MotherpulseDay30min4);
				assessmentOfPostPartumCondition.setMotherpulseDay6hrs5(MotherpulseDay6hrs5);
				assessmentOfPostPartumCondition.setMotherpulseDay6hrs6(MotherpulseDay6hrs6);
				assessmentOfPostPartumCondition.setMotherpulseDay6hrs7(MotherpulseDay6hrs7);
				assessmentOfPostPartumCondition.setMotherpulseDay2morning(MotherpulseDay2morning);
				assessmentOfPostPartumCondition.setMotherpulseDay2evening(MotherpulseDay2evening);
				
				
				
				
				assessmentOfPostPartumCondition.setMotherBreastconditionDay30min1    (MotherBreastconditionDay30min1  );
				assessmentOfPostPartumCondition.setMotherBreastconditionDay30min2    (MotherBreastconditionDay30min2  );
				assessmentOfPostPartumCondition.setMotherBreastconditionDay30min3    (MotherBreastconditionDay30min3  );
				assessmentOfPostPartumCondition.setMotherBreastconditionDay30min4    (MotherBreastconditionDay30min4  );
				assessmentOfPostPartumCondition.setMotherBreastconditionDay6hrs5     (MotherBreastconditionDay6hrs5   );
				assessmentOfPostPartumCondition.setMotherBreastconditionDay6hrs6     (MotherBreastconditionDay6hrs6   );
				assessmentOfPostPartumCondition.setMotherBreastconditionDay6hrs7     (MotherBreastconditionDay6hrs7   );
				assessmentOfPostPartumCondition.setMotherBreastconditionDay2morning  (MotherBreastconditionDay2morning);
				assessmentOfPostPartumCondition.setMotherBreastconditionDay2evening  (MotherBreastconditionDay2evening);
				
				
				
				
				
				
				assessmentOfPostPartumCondition.setMotherBleedingPVDay30min1  (MotherBleedingPVDay30min1   );
				assessmentOfPostPartumCondition.setMotherBleedingPVDay30min2  (MotherBleedingPVDay30min2   );
				assessmentOfPostPartumCondition.setMotherBleedingPVDay30min3  (MotherBleedingPVDay30min3   );
				assessmentOfPostPartumCondition.setMotherBleedingPVDay30min4  (MotherBleedingPVDay30min4   );
				assessmentOfPostPartumCondition.setMotherBleedingPVDay6hrs5   (MotherBleedingPVDay6hrs5    );
				assessmentOfPostPartumCondition.setMotherBleedingPVDay6hrs6   (MotherBleedingPVDay6hrs6    );
				assessmentOfPostPartumCondition.setMotherBleedingPVDay6hrs7   (MotherBleedingPVDay6hrs7    );
				assessmentOfPostPartumCondition.setMotherBleedingPVDay2morning(MotherBleedingPVDay2morning );
				assessmentOfPostPartumCondition.setMotherBleedingPVDay2evening(MotherBleedingPVDay2evening );
				
				
				
				
				
				
				assessmentOfPostPartumCondition.setMotherUterineToneDay30min1       ( MotherUterineToneDay30min1   );
				assessmentOfPostPartumCondition.setMotherUterineToneDay30min2       ( MotherUterineToneDay30min2   );
				assessmentOfPostPartumCondition.setMotherUterineToneDay30min3       ( MotherUterineToneDay30min3   );
				assessmentOfPostPartumCondition.setMotherUterineToneDay30min4       ( MotherUterineToneDay30min4   );
				assessmentOfPostPartumCondition.setMotherUterineToneDay6hrs5        ( MotherUterineToneDay6hrs5    );
				assessmentOfPostPartumCondition.setMotherUterineToneDay6hrs6        ( MotherUterineToneDay6hrs6    );
				assessmentOfPostPartumCondition.setMotherUterineToneDay6hrs7        ( MotherUterineToneDay6hrs7    );
				assessmentOfPostPartumCondition.setMotherUterineToneDay2morning     ( MotherUterineToneDay2morning );
				assessmentOfPostPartumCondition.setMotherUterineToneDay2evening     ( MotherUterineToneDay2evening );
				
		        
				assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay30min1       ( MotherEpisiotomyTearDay30min1   );       
				assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay30min2       ( MotherEpisiotomyTearDay30min2   );       
				assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay30min3       ( MotherEpisiotomyTearDay30min3   );       
				assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay30min4       ( MotherEpisiotomyTearDay30min4   );       
				assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay6hrs5        ( MotherEpisiotomyTearDay6hrs5    );       
				assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay6hrs6        ( MotherEpisiotomyTearDay6hrs6    );       
				assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay6hrs7        ( MotherEpisiotomyTearDay6hrs7    );       
				assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay2morning     ( MotherEpisiotomyTearDay2morning );       
				assessmentOfPostPartumCondition.setMotherEpisiotomyTearDay2evening     ( MotherEpisiotomyTearDay2evening );       
		        
		        
				assessmentOfPostPartumCondition.setBabyResprateDay30min1       ( BabyResprateDay30min1   );  
				assessmentOfPostPartumCondition.setBabyResprateDay30min2       ( BabyResprateDay30min2   );  
				assessmentOfPostPartumCondition.setBabyResprateDay30min3       ( BabyResprateDay30min3   );  
				assessmentOfPostPartumCondition.setBabyResprateDay30min4       ( BabyResprateDay30min4   );  
				assessmentOfPostPartumCondition.setBabyResprateDay6hrs5        ( BabyResprateDay6hrs5    );  
				assessmentOfPostPartumCondition.setBabyResprateDay6hrs6        ( BabyResprateDay6hrs6    );  
				assessmentOfPostPartumCondition.setBabyResprateDay6hrs7        ( BabyResprateDay6hrs7    );  
				assessmentOfPostPartumCondition.setBabyResprateDay2morning     ( BabyResprateDay2morning );  
				assessmentOfPostPartumCondition.setBabyResprateDay2evening     ( BabyResprateDay2evening );  
				
				
		        
				assessmentOfPostPartumCondition.setBabytempDay30min1       ( BabytempDay30min1   );  
				assessmentOfPostPartumCondition.setBabytempDay30min2       ( BabytempDay30min2   );  
				assessmentOfPostPartumCondition.setBabytempDay30min3       ( BabytempDay30min3   );  
				assessmentOfPostPartumCondition.setBabytempDay30min4       ( BabytempDay30min4   );  
				assessmentOfPostPartumCondition.setBabytempDay6hrs5        ( BabytempDay6hrs5    );  
				assessmentOfPostPartumCondition.setBabytempDay6hrs6        ( BabytempDay6hrs6    );  
				assessmentOfPostPartumCondition.setBabytempDay6hrs7        ( BabytempDay6hrs7    );  
				assessmentOfPostPartumCondition.setBabytempDay2morning     ( BabytempDay2morning );  
				assessmentOfPostPartumCondition.setBabytempDay2evening     ( BabytempDay2evening );  
						
				
				assessmentOfPostPartumCondition.setBabyBreastfeedingDay30min1       ( BabyBreastfeedingDay30min1  );   
				assessmentOfPostPartumCondition.setBabyBreastfeedingDay30min2       ( BabyBreastfeedingDay30min2  );   
				assessmentOfPostPartumCondition.setBabyBreastfeedingDay30min3       ( BabyBreastfeedingDay30min3  );   
				assessmentOfPostPartumCondition.setBabyBreastfeeding30min4      	( BabyBreastfeeding30min4  );   
				assessmentOfPostPartumCondition.setBabyBreastfeedingDay6hrs5        ( BabyBreastfeedingDay6hrs5   );   
				assessmentOfPostPartumCondition.setBabyBreastfeedingDay6hrs6        ( BabyBreastfeedingDay6hrs6   );   
				assessmentOfPostPartumCondition.setBabyBreastfeedingDay6hrs7        ( BabyBreastfeedingDay6hrs7   );   
				assessmentOfPostPartumCondition.setBabyBreastfeedingDay2morning     ( BabyBreastfeedingDay2morning);   
				assessmentOfPostPartumCondition.setBabyBreastfeedingDay2evening     ( BabyBreastfeedingDay2evening);   
						                                                                                  
				assessmentOfPostPartumCondition.setBabyActivityDay30min1       ( BabyActivityDay30min1  ); 
				assessmentOfPostPartumCondition.setBabyActivityDay30min2       ( BabyActivityDay30min2  ); 
				assessmentOfPostPartumCondition.setBabyActivityDay30min3       ( BabyActivityDay30min3  ); 
				assessmentOfPostPartumCondition.setBabyActivityDay30min4       ( BabyActivityDay30min4  );    
				assessmentOfPostPartumCondition.setBabyActivityDay6hrs5        ( BabyActivityDay6hrs5   ); 
				assessmentOfPostPartumCondition.setBabyActivityDay6hrs6        ( BabyActivityDay6hrs6   ); 
				assessmentOfPostPartumCondition.setBabyActivityDay6hrs7        ( BabyActivityDay6hrs7   ); 
				assessmentOfPostPartumCondition.setBabyActivityDay2morning     ( BabyActivityDay2morning); 
				assessmentOfPostPartumCondition.setBabyActivityDay2evening     ( BabyActivityDay2evening); 
						                                                                                       
				
		        
					assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay30min1       ( BabyUmbilicalstumpDay30min1    );     
					assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay30min2       ( BabyUmbilicalstumpDay30min2    );     
					assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay30min3       ( BabyUmbilicalstumpDay30min3    );     
					assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay30min4       ( BabyUmbilicalstumpDay30min4    );     
					assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay6hrs5        ( BabyUmbilicalstumpDay6hrs5     );     
					assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay6hrs6        ( BabyUmbilicalstumpDay6hrs6     );     
					assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay6hrs7        ( BabyUmbilicalstumpDay6hrs7     );     
					assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay2morning     ( BabyUmbilicalstumpDay2morning  );     
					assessmentOfPostPartumCondition.setBabyUmbilicalstumpDay2evening     ( BabyUmbilicalstumpDay2evening  ); 
					
					
		            
					assessmentOfPostPartumCondition.setBabyJaundiceDay30min1       ( BabyJaundiceDay30min1   ); 
					assessmentOfPostPartumCondition.setBabyJaundiceDay30min2       ( BabyJaundiceDay30min2   ); 
					assessmentOfPostPartumCondition.setBabyJaundiceDay30min3       ( BabyJaundiceDay30min3   ); 
					assessmentOfPostPartumCondition.setBabyJaundiceDay30min4       ( BabyJaundiceDay30min4   ); 
					assessmentOfPostPartumCondition.setBabyJaundiceDay6hrs5        ( BabyJaundiceDay6hrs5    ); 
					assessmentOfPostPartumCondition.setBabyJaundiceDay6hrs6        ( BabyJaundiceDay6hrs6    ); 
					assessmentOfPostPartumCondition.setBabyJaundiceDay6hrs7        ( BabyJaundiceDay6hrs7    ); 
					assessmentOfPostPartumCondition.setBabyJaundiceDay2morning     ( BabyJaundiceDay2morning ); 
					assessmentOfPostPartumCondition.setBabyJaundiceDay2evening     ( BabyJaundiceDay2evening ); 
					
					
					assessmentOfPostPartumCondition.setBabyPassedurineDay30min1       ( BabyPassedurineDay30min1   );         
					assessmentOfPostPartumCondition.setBabyPassedurineDay30min2       ( BabyPassedurineDay30min2   );         
					assessmentOfPostPartumCondition.setBabyPassedurineDay30min3       ( BabyPassedurineDay30min3   );         
					assessmentOfPostPartumCondition.setBabyPassedurineDay30min4       ( BabyPassedurineDay30min4   );         
					assessmentOfPostPartumCondition.setBabyPassedurineDay6hrs5        ( BabyPassedurineDay6hrs5    );         
					assessmentOfPostPartumCondition.setBabyPassedurineDay6hrs6        ( BabyPassedurineDay6hrs6    );         
					assessmentOfPostPartumCondition.setBabyPassedurineDay6hrs7        ( BabyPassedurineDay6hrs7    );         
					assessmentOfPostPartumCondition.setBabyPassedurineDay2morning     ( BabyPassedurineDay2morning );         
					assessmentOfPostPartumCondition.setBabyPassedurineDay2evening     ( BabyPassedurineDay2evening ); 
					
					
					assessmentOfPostPartumCondition.setBabyPassedstoolDay30min1       (BabyPassedstoolDay30min1  ); 
					assessmentOfPostPartumCondition.setBabyPassedstoolDay30min2       (BabyPassedstoolDay30min2  ); 
					assessmentOfPostPartumCondition.setBabyPassedstoolDay30min3       (BabyPassedstoolDay30min3  ); 
					assessmentOfPostPartumCondition.setBabyPassedstoolDay30min4       (BabyPassedstoolDay30min4  ); 
					assessmentOfPostPartumCondition.setBabyPassedstoolDay6hrs5        (BabyPassedstoolDay6hrs5   ); 
					assessmentOfPostPartumCondition.setBabyPassedstoolDay6hrs6        (BabyPassedstoolDay6hrs6   ); 
					assessmentOfPostPartumCondition.setBabyPassedstoolDay6hrs7        (BabyPassedstoolDay6hrs7   ); 
					assessmentOfPostPartumCondition.setBabyPassedstoolDay2morning     (BabyPassedstoolDay2morning); 
					assessmentOfPostPartumCondition.setBabyPassedstoolDay2evening     (BabyPassedstoolDay2evening); 
		        
				
			 	assessmentOfPostPartumCondition.setAdmission(setAdmission1);
			 	assessmentOfPostPartumConditionRepository.save(assessmentOfPostPartumCondition);
			 	 assessmentOfPostPartumCondition.getId();
			 	 
			 	 
			 	
			 	obsteric.      setDeliveryOutcome1(deliveryOutcome1)	;	
				obsteric.      setDeliveryOutcome2(deliveryOutcome2) ;	
				obsteric.      setDeliveryNotesTime(deliveryNotesTime) ;	
				obsteric.      setSexOfBaby(sexOfBaby)         ;	
				obsteric.      setPreterm (preterm);         	
				obsteric.      setBirthweight(birthweight);    	
				obsteric.      setCriedImmediatelyAfterBirth(criedImmediatelyAfterBirth);
				obsteric.      setResuscitated (resuscitated);
				obsteric.      setBreastFeedingInitiated(breastFeedingInitiated);
				obsteric.      setIfYesTimeOfInitiation(ifYesTimeOfInitiation);
				obsteric.      setCongenitalAnomaly(congenitalAnomaly);
				obsteric.      setSpecify (specify);
				obsteric.setField1(field1);
				obsteric.setField2(field2);
				obsteric.setField3(field3);
				obsteric.setField4(field4);
				obsteric.setField5(field5);
				obsteric.setField6(field6);
				obsteric.setField7(field7);
				obsteric.setField8(field8);
				obsteric.setField9(field9);
				obsteric.setField10(field10);
				obsteric.      setAdmission(setAdmission1);
				setAdmission1. setId(Long.valueOf(admissionId));
				obsteric.setAdmission(setAdmission1);
				deliveryRepository.save(obsteric);
					
					
					
					obsteric.setAdmission(setAdmission1);
					setAdmission1.setId(Long.valueOf(admissionId));
			
					JSONArray arr = (JSONArray) openJson.get("babyNotes");
					Long[] babyNotesarr = new Long[arr.length()];
				for (int i = 0; i < arr.length(); i++) {
					 String babyId =arr.getJSONObject(i).getString("id"); 
					  String injVitaminKAdministered=arr.getJSONObject(i).getString("injVitaminKAdministered");        
					  String ifYesDose               =arr.getJSONObject(i).getString("ifYesDose");                       
					  String vaccinationDone        =arr.getJSONObject(i).getString("vaccinationDone");         
					  String conditionOfBaby         =arr.getJSONObject(i).getString("conditionOfBaby");      
					  String detailsOf3rd4thStageOfLabor=arr.getJSONObject(i).getString("detailsOf3rd4thStageOfLabor");     
					  String anyOtherDrugSpecify=arr.getJSONObject(i).getString("anyOtherDrugSpecify");            
					  String cctWard		=arr.getJSONObject(i).getString("cctWard");                      
					  String utMassageWard=arr.getJSONObject(i).getString("utMassageWard");                  
					  String placentaCompleteWard=arr.getJSONObject(i).getString("placentaCompleteWard");           
					  String episiotomyGivenWard=arr.getJSONObject(i).getString("episiotomyGivenWard");             
					  String ppiucdInsertedWard=arr.getJSONObject(i).getString("ppiucdInsertedWard");             
				String complicationsDuringDeliveryWard =arr.getJSONObject(i).getString("complicationsDuringDeliveryWard");
					  String dateOfTransferToPncWard =arr.getJSONObject(i).getString("dateOfTransferToPncWard");        
				      String timeOfTransferToPncWard =arr.getJSONObject(i).getString("timeOfTransferToPncWard");      
				      String babyReferredWard =arr.getJSONObject(i).getString("babyReferredWard");             
				      String ifYesgiveReasonWard =arr.getJSONObject(i).getString("ifYesgiveReasonWard");            
				      String child_id =setAdmission1.getUniqueNo()+"GOI"+i;
				BabyNotes babynotes = new BabyNotes();
				if(!babyId.equalsIgnoreCase("0"))
					babynotes.setId(Long.valueOf(babyId));
					babynotes.setBabyId(setAdmission1.uniqueNo+"GOIL2"+String.valueOf(arr.length()+i));
			babynotes.setInjVitaminKAdministered(injVitaminKAdministered);
			babynotes.setIfYesDose(ifYesDose);
			babynotes.setVaccinationDone(vaccinationDone);
			babynotes.setConditionOfBaby(conditionOfBaby);
			babynotes.setDetailsOf3rd4thStageOfLabor(detailsOf3rd4thStageOfLabor);
			babynotes.setAnyOtherDrugSpecify(anyOtherDrugSpecify);
			babynotes.setCctWard(cctWard);
			babynotes.setUtMassageWard(utMassageWard);
			babynotes.setPlacentaCompleteWard(placentaCompleteWard);
			babynotes.setEpisiotomyGivenWard(episiotomyGivenWard);
			babynotes.setPpiucdInsertedWard(ppiucdInsertedWard);
			babynotes.setComplicationsDuringDeliveryWard(complicationsDuringDeliveryWard);
			babynotes.setDateOfTransferToPncWard(dateOfTransferToPncWard);
			babynotes.setTimeOfTransferToPncWard(timeOfTransferToPncWard);
			babynotes.setBabyReferredWard(babyReferredWard);
			babynotes.setIfYesgiveReasonWard(ifYesgiveReasonWard);
			babynotes.setBabyId(child_id);
			babynotes.setAdmission(setAdmission1);
			babynotes.setUniqueId(setAdmission1.getUniqueNo());
				babynotes.setBabyId(child_id);
				babynotes.setUniqueId(setAdmission1.getUniqueNo());
				babynotes.setAdmission(setAdmission1);
				babyNotesRepository.save(babynotes);
				babyNotesarr[i] = babynotes.getId();
				}
				
				
				deliveryRepository.save(obsteric);
				userMap.put("status", HttpStatus.OK);
				userMap.put("data", "Delivery Updated successfull");
				userMap.put("id", obsteric.getId());
				userMap.put("babyNotesarr", babyNotesarr);
				userMap.put("postPartumArr", assessmentOfPostPartumCondition.getId());
			/*}
				catch(Exception e)
				{
					userMap.put("status",  HttpStatus.INTERNAL_SERVER_ERROR);
					userMap.put("data", "Delivery Failure");
					userMap.put("id", "fail");
					userMap.put("babyNotesarr",  "fail");
					userMap.put("postPartumArr",  "fail");
					userMap.put("error",  e.getMessage());
				}*/
				return userMap;
		}
}
