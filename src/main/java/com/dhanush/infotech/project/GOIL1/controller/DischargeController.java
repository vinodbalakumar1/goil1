package com.dhanush.infotech.project.GOIL1.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL1.model.Admission;
import com.dhanush.infotech.project.GOIL1.model.DischargeDetails;
import com.dhanush.infotech.project.GOIL1.repository.AdmissionRepository;
import com.dhanush.infotech.project.GOIL1.repository.DischargeDetailsRepository;
import com.dhanush.infotech.project.GOIL1.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api")
public class DischargeController {

	/**
	 * Created by vinod on 13/06/18 12:23 // For insert discharge data.
	 * 
	 */

	@Autowired
	private DischargeDetailsRepository dischargeDetailsRepository;

	public DischargeDetailsRepository getDischargeDetailsRepository() {
		return dischargeDetailsRepository;
	}

	public void setDischargeDetailsRepository(DischargeDetailsRepository dischargeDetailsRepository) {
		this.dischargeDetailsRepository = dischargeDetailsRepository;
	}

	@Autowired
	AdmissionRepository admissionRepository;

	@CrossOrigin
	@RequestMapping(value = "/discharge/l1", method = RequestMethod.POST, produces = { "application/json" })
	public HashMap<String, Object> discharge(@RequestBody String userJsonReq) throws NumberFormatException, Exception {
		HashMap<String, Object> userMap = new HashMap<>();
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String id = openJson.getString("id");
		HashMap<String, Object> status = null;
		if (id.equalsIgnoreCase("")) {
			status = insertDischarge(userJsonReq);
			String statu = status.get("data").equals("Discharge Added successfull") ? "OK"
					: "ERROR";
			userMap.put("status", statu);
			userMap.put("data", status.get("data"));
			userMap.put("id", status.get("id"));
		} else {
			status = updateDischarge(userJsonReq, id);
			String statu = status.get("data").equals("Discharge Updated successfull") ? "OK"
					: "ERROR";
			userMap.put("status", statu);
			userMap.put("data", status.get("data"));
			userMap.put("id", status.get("id"));
		}

		return userMap;
	}

	public HashMap<String, Object> insertDischarge(String userJsonReq) {
		HashMap<String, Object> userMap = new HashMap<>();
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		try {
			// getting values from request

			String conditionOfMother = openJson.getString("conditionOfMother");
			String conditionOfBaby = openJson.getString("conditionOfBaby");
			String adviceOfMother = openJson.getString("adviceOfMother");
			String adviceOfBaby = openJson.getString("adviceOfBaby");
			String admissionId = openJson.getString("admissionId");
			String otherNotes = openJson.getString("othernotes");
			String date_of_Discharge_Referral = openJson.getString("date_of_Discharge_Referral");
			String time_of_Discharge_Referral =openJson.getString("time_of_Discharge_Referral");
			org.json.JSONObject others1 = openJson.getJSONObject("others");
			 String field1   =  others1.getString("field1");;
			 String field2   =  others1.getString("field2");
			 String field3   =  others1.getString("field3");
			 String field4   =  others1.getString("field4");
			 String field5   =  others1.getString("field5");
			 String field6   =  others1.getString("field6");
			 String field7   =  others1.getString("field7");
			 String field8   =  others1.getString("field8");
			 String field9   =  others1.getString("field9");
			 String field10  =  others1.getString("field10");
			
			
			
			

			// setters
			Admission setAdmission1 = admissionRepository.findById(Long.valueOf(admissionId))
					.orElseThrow(() -> new ResourceNotFoundException("Admission", "NOT FOUND", admissionId));
			
			DischargeDetails dischargeDetails = new DischargeDetails();
			dischargeDetails.setConditionOfMother(conditionOfMother);
			dischargeDetails.setConditionOfBaby(conditionOfBaby);
			dischargeDetails.setAdviceOfBaby(adviceOfBaby);
			dischargeDetails.setAdviceOfMother(adviceOfMother);
			dischargeDetails.setAdmission(setAdmission1);
			dischargeDetails.setOthers(otherNotes);
			dischargeDetails.setDate_of_Discharge_Referral(date_of_Discharge_Referral);
			dischargeDetails.setTime_of_Discharge_Referral(time_of_Discharge_Referral);
			dischargeDetails.setField1(field1);
			dischargeDetails.setField2(field2);
			dischargeDetails.setField3(field3);
			dischargeDetails.setField4(field4);
			dischargeDetails.setField5(field5);
			dischargeDetails.setField6(field6);
			dischargeDetails.setField7(field7);
			dischargeDetails.setField8(field8);
			dischargeDetails.setField9(field9);
			dischargeDetails.setField10(field10);
			
			setAdmission1.setId(Long.valueOf(admissionId));
			setAdmission1.setCompleteness("100%");
			List<DischargeDetails> admission = dischargeDetailsRepository.findByCaseId(setAdmission1.getId());
			if(admission.size() == 0) {
				dischargeDetailsRepository.save(dischargeDetails);
				userMap.put("status", HttpStatus.OK);
				userMap.put("data", "Discharge Added successfull");
				userMap.put("id", dischargeDetails.getId());
				return userMap;
			}
			
			userMap.put("status", HttpStatus.OK);
			userMap.put("data", "Discharge Already Exist");
			userMap.put("id", dischargeDetails.getId());
		
			} catch (Exception e) {
			userMap.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			userMap.put("data", "Discharge failure");
			userMap.put("id", "failed");
			userMap.put("error", e.getMessage());
		}
		return userMap;
	}

	public HashMap<String, Object> updateDischarge(String userJsonReq, String id) {
		HashMap<String, Object> userMap = new HashMap<>();
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		try {
			// getting values from request
		String conditionOfMother = openJson.getString("conditionOfMother");
		String conditionOfBaby = openJson.getString("conditionOfBaby");
		String adviceOfMother = openJson.getString("adviceOfMother");
		String adviceOfBaby = openJson.getString("adviceOfBaby");
		String admissionId = openJson.getString("admissionId");
		String otherNotes = openJson.getString("othernotes");
		String date_of_Discharge_Referral = openJson.getString("date_of_Discharge_Referral");
		String time_of_Discharge_Referral =openJson.getString("time_of_Discharge_Referral");
		org.json.JSONObject others1 = openJson.getJSONObject("others");
		 String field1   =  others1.getString("field1");;
		 String field2   =  others1.getString("field2");
		 String field3   =  others1.getString("field3");
		 String field4   =  others1.getString("field4");
		 String field5   =  others1.getString("field5");
		 String field6   =  others1.getString("field6");
		 String field7   =  others1.getString("field7");
		 String field8   =  others1.getString("field8");
		 String field9   =  others1.getString("field9");
		 String field10  =  others1.getString("field10");
		
		// setters
			Admission setAdmission1 = admissionRepository.findById(Long.valueOf(admissionId))
					.orElseThrow(() -> new ResourceNotFoundException("Admission", "NOT FOUND", admissionId));
			DischargeDetails dischargeDetails = dischargeDetailsRepository.findById(Long.valueOf(setAdmission1.getDischargeDetails().getId()))
					.orElseThrow(() -> new ResourceNotFoundException("Discharge", "NOT FOUND", id));
			dischargeDetails.setConditionOfMother(conditionOfMother);
			dischargeDetails.setConditionOfBaby(conditionOfBaby);
			dischargeDetails.setAdviceOfBaby(adviceOfBaby);
			dischargeDetails.setAdviceOfMother(adviceOfMother);
			dischargeDetails.setOthers(otherNotes);
			dischargeDetails.setDate_of_Discharge_Referral(date_of_Discharge_Referral);
			dischargeDetails.setTime_of_Discharge_Referral(time_of_Discharge_Referral);
			dischargeDetails.setField1(field1);
			dischargeDetails.setField2(field2);
			dischargeDetails.setField3(field3);
			dischargeDetails.setField4(field4);
			dischargeDetails.setField5(field5);
			dischargeDetails.setField6(field6);
			dischargeDetails.setField7(field7);
			dischargeDetails.setField8(field8);
			dischargeDetails.setField9(field9);
			dischargeDetails.setField10(field10);
			dischargeDetails.setAdmission(setAdmission1);
			dischargeDetails.setUniqueId(setAdmission1.getUniqueNo());
			dischargeDetailsRepository.save(dischargeDetails);

			userMap.put("status", HttpStatus.OK);
			userMap.put("data", "Discharge Updated successfull");
			userMap.put("id", dischargeDetails.getId());
				} catch (Exception e) {
			userMap.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			userMap.put("data", "Discharge Updated failure");
			userMap.put("id", "failed");
			userMap.put("error", e.getMessage());
		}
		return userMap;
	}
}
