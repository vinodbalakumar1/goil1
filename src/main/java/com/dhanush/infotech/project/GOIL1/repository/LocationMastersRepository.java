package com.dhanush.infotech.project.GOIL1.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dhanush.infotech.project.GOIL1.model.LocationMasters;


@Repository
public interface LocationMastersRepository extends JpaRepository<LocationMasters, Long>{

	
	@Query(value = "SELECT distinct (a.stateCode),a.state FROM LocationMasters a")
	List<LocationMasters> findByCaseId();
	
	
	@Query(value = "SELECT distinct (a.district),a.districtCode FROM LocationMasters a where a.stateCode = :stateId")
	List<LocationMasters> findByCaseId1(@Param("stateId") int stateId); 
	
	@Query(value = "SELECT distinct (a.blockCode),a.block FROM LocationMasters a where a.stateCode = :stateId and a.district =:districtId ")
	List<LocationMasters> findByCaseId2(@Param("stateId") int stateId,@Param("districtId") int districtId); 
	
	
	@Query(value = "SELECT distinct (a.facilityTypeId),a.facilityType FROM LocationMasters a where a.stateCode = :stateId and a.district =:districtId and a.blockCode = :blockCode")
	List<LocationMasters> findByCaseId3(@Param("stateId") int stateId,@Param("districtId") int districtId,@Param("blockCode") int blockCode); 
	
	
	@Query(value = "SELECT distinct (a.facilityCode),a.facility FROM LocationMasters a where a.stateCode = :stateId and a.district =:districtId and a.blockCode = :blockCode and facilityTypeId = :facilityTypeId")
	List<LocationMasters> findByCaseId4(@Param("stateId") int stateId,@Param("districtId") int districtId,@Param("blockCode") int blockCode,@Param("facilityTypeId") String facilityTypeId); 
	
	@Query(value = "SELECT distinct (a.villageCode),a.village FROM LocationMasters a where a.stateCode = :stateId and a.district =:districtId and a.blockCode = :blockCode and facilityTypeId = :facilityTypeId and a.facilityCode = :facilityCode")
	List<LocationMasters> findByCaseId5(@Param("stateId") int stateId,@Param("districtId") int districtId,@Param("blockCode") int blockCode,@Param("facilityTypeId") String facilityTypeId, @Param("facilityCode") int facilityCode); 
	
	
	@Query(value = "SELECT distinct(a.villageCode),a.village FROM LocationMasters a where a.stateCode = :stateId and a.district =:districtId and a.blockCode = :blockCode and facilityCode = :facilityCode and facilityTypeId = :facilityType and subFacilityCode = :subFacilityCode")
	List<LocationMasters> findByCaseId6(@Param("stateId") int stateId,@Param("districtId") int districtId,@Param("blockCode") int blockCode,@Param("facilityCode") int facilityCode,@Param("facilityType") int i,@Param("subFacilityCode") int subFacilityCode);
	
	@Query(value = "SELECT distinct (a.facilityType),a.facility,a.block,a.districtCode,a.state FROM LocationMasters a where a.stateCode = :stateId and a.district =:districtId and a.blockCode = :blockCode and facilityCode = :facilityCode and facilityTypeId = :facilityType")
	List<LocationMasters> findByLogin(@Param("stateId") int stateId,@Param("districtId") int districtId,@Param("blockCode") int blockCode,@Param("facilityCode") int facilityCode,@Param("facilityType") String i);

	@Query(value = "SELECT a FROM LocationMasters a where a.stateCode = :state and a.district =:district and a.blockCode = :block  and a.facilityCode = :facility_code and a.facilityTypeId = :facilityTypeId")
	 List<LocationMasters> findByFacility(@Param("state")int state,@Param("district") int district,@Param("block") int block,@Param("facility_code") int facility_code,@Param("facilityTypeId") String facilityTypeId);


	
	
	//for createuser 
	
	@Query(value = "SELECT a FROM LocationMasters a where a.stateCode = :state")
	List<LocationMasters> findByState(@Param("state") String state);

	@Query(value = "SELECT a FROM LocationMasters a where a.stateCode = :state and a.district =:district")
	List<LocationMasters> findByDistrictMethod(@Param("state")int state,@Param("district") int district);

	@Query(value = "SELECT a FROM LocationMasters a where a.stateCode = :state and a.district =:district and a.blockCode = :block ")
	List<LocationMasters> findByBlockMethod(@Param("state")int state,@Param("district") int district,@Param("block") int block);

	@Query(value = "SELECT a FROM LocationMasters a where a.stateCode = :state and a.district =:district and a.blockCode = :block and  a.facilityTypeId = :facilityType")
	List<LocationMasters> findByFacilityTypeIdMethod(@Param("state")int state,@Param("district") int district, @Param("block")int block,@Param("facilityType") String facilityType);

	@Query(value = "SELECT a FROM LocationMasters a where a.stateCode = :state and a.district =:district and a.blockCode = :block and a.facilityTypeId = :facilityType and a.facilityCode = :facility_code ")
	List<LocationMasters> findByFacilityCodeMethod(@Param("state")int state,@Param("district") int district,@Param("block")int block, @Param("facilityType")String facilityType,@Param("facility_code") int facility_code);

	@Query(value = "SELECT a FROM LocationMasters a where a.stateCode = :state")
	List<LocationMasters> findByStateCodeMethod(@Param("state")int state);




	
}
