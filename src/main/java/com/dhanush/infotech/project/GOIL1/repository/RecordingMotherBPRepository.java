package com.dhanush.infotech.project.GOIL1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dhanush.infotech.project.GOIL1.model.RecordingMotherBP;

public interface RecordingMotherBPRepository extends JpaRepository<RecordingMotherBP, Long> {

	@Query(value = "select count(a.bpdiastolic) as count FROM RecordingMotherBP a where a.state = :state and a.createdat > :start and a.createdat < :end and a.bpdiastolic > 0")
	Long findByState(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(a.bpdiastolic) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.createdat > :start and a.createdat < :end and a.bpdiastolic > 0 ")
	Long findByDistrict(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(a.bpdiastolic) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.block = :block and a.createdat > :start and a.createdat < :end and a.bpdiastolic > 0 ")
	Long findByBlock(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(a.bpdiastolic) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat > :start and a.createdat < :end and a.bpdiastolic > 0 ")
	Long findByFacilitytype(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(a.bpdiastolic) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat > :start and a.createdat < :end and a.bpdiastolic > 0")
	Long findByFacility(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM RecordingMotherBP a where a.state = :state and a.createdat >= :start and a.createdat <= :end")
	Long findByState1(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.createdat >= :start and a.createdat <= :end")
	Long findByDistrict1(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(1) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.block = :block and a.createdat >= :start and a.createdat <= :end ")
	Long findByBlock1(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat >= :start and a.createdat <= :end ")
	Long findByFacilitytype1(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(1) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat >= :start and a.createdat <= :end ")
	Long findByFacility1(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select  count(1) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat like :date%  ")
	Long FacilityDashBoard(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("date") String date);
	
	
	
	
	@Query(value = "select count(1) as count FROM RecordingMotherBP a where a.state = :state and a.createdat like :date%  ")
	Long FacilityDashBoardstate(@Param("state") String state,@Param("date") String date);
	
	

	@Query(value = "select  count(1) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat like :date%  ")
	Long FacilityDashBoardFacility(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("date") String date);


	
	@Query(value = "select  count(a.bpdiastolic) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat like :date% and a.bpdiastolic > 0")
	Long MotherBpFacility(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("date") String date);


	
	@Query(value = "select count(a.bpdiastolic) as count FROM RecordingMotherBP a where a.state = :state and a.createdat like :date%  and a.bpdiastolic > 0")
	Long MotherBpState(@Param("state") String state,@Param("date") String date);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Query(value = "select count(a.bpdiastolic) as count,a.districtname,a.district FROM RecordingMotherBP a where a.state = :state and a.createdat like :start%  and a.bpdiastolic > '0' group by a.district,a.districtname")
	List<RecordingMotherBP> SDByState(@Param("state") String state,@Param("start") String start);
	
	
	@Query(value = "select count(1) as count,a.districtname,a.facilityname,a.district,a.facility FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.createdat like :start%  and a.bpdiastolic > '0' group by a.district,a.districtname,a.facility,a.facilityname")
	List<RecordingMotherBP> SDByDistrict(@Param("state") String state,@Param("district") String district,@Param("start") String start);

	
	
	// sd
	
	@Query(value = "select count(a.bpdiastolic) as count,a.districtname,a.district FROM RecordingMotherBP a where a.state = :state and a.createdat like :start%  and a.bpdiastolic > '0' group by a.district,a.districtname")
	List<Object[]> objState(@Param("state") String state,@Param("start") String start);
	
	
	@Query(value = "select count(1) as count,a.facilityname,a.districtname,a.district,a.facility FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.createdat like :start%  and a.bpdiastolic > '0' group by a.district,a.districtname,a.facility,a.facilityname")
	List<Object[]> objDistrict(@Param("state") String state,@Param("district") String district,@Param("start") String start);

//	
	
	@Query(value = "select a.districtname,a.district FROM RecordingMotherBP a where a.state = :state and a.createdat like :start% group by a.districtname,a.district ")
	List<Object[]> objtate(@Param("state") String state,@Param("start") String start);
	
	@Query(value = "select a.districtname,a.facilityname,a.district,a.facility FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.createdat like :start% group by a.district,a.districtname,a.facility,a.facilityname ")
	List<Object[]> objDistrict1(@Param("state") String state,@Param("district") String district,@Param("start") String start);
	
	
	
	
	
	//
	
	@Query(value = "select count(1) as count,a.districtname,a.district FROM RecordingMotherBP a where a.state = :state and a.createdat like :start% group by a.districtname,a.district ")
	List<RecordingMotherBP> SDByState1(@Param("state") String state,@Param("start") String start);
	
	
	@Query(value = "select count(1) as count,a.districtname,a.facilityname,a.district,a.facility FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.createdat like :start% group by a.district,a.districtname,a.facility,a.facilityname ")
	List<RecordingMotherBP> SDByDistrict1(@Param("state") String state,@Param("district") String district,@Param("start") String start);
	
	
	//sd
	@Query(value = "select count(1) as count,a.districtname,a.district FROM RecordingMotherBP a where a.state = :state and a.createdat like :start% group by a.districtname,a.district ")
	List<Object[]> objByState(@Param("state") String state,@Param("start") String start);
	
	@Query(value = "select count(1) as count,a.facilityname,a.districtname,a.district,a.facility FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.createdat like :start% group by a.district,a.districtname,a.facility,a.facilityname ")
	List<Object[]> objByDistrict1(@Param("state") String state,@Param("district") String district,@Param("start") String start);

	
	//mother bp for mpreport

	
		@Query(value = "select count(a.bpdiastolic) as count FROM RecordingMotherBP a where a.state = :state and a.createdat like :start%  and a.bpdiastolic > 0")
		Long findByStateMotherBp(@Param("state") String state,@Param("start") String start);
		
		
		@Query(value = "select count(a.bpdiastolic) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.createdat like :start% and a.bpdiastolic > 0 ")
		Long findByDistrictMotherBp(@Param("state") String state,@Param("district") String district,@Param("start") String start);

		
		@Query(value = "select count(a.bpdiastolic) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.block = :block and a.createdat like :start% and a.bpdiastolic > 0 ")
		Long findByBlockMotherBp(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start);
		
		
		@Query(value = "select count(a.bpdiastolic) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat like :start% and a.bpdiastolic > 0 ")
		Long findByFacilitytypeMotherBp(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		@Query(value = "select  count(a.bpdiastolic) as count FROM RecordingMotherBP a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat like :start% and a.bpdiastolic > 0")
		Long findByFacilityCodeMotherBp(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start);


	
	
	
	
	
}
